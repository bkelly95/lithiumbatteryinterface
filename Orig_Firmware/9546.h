// ***************** User-configurable parameters ********************

#define PCA9546_addr 0xE0

// *******************************************************************
/*
// Function prototypes.
// Devices:
// 24LC128 EEPROM Address 000 = 10100000 0xC0
// SCD41-D-R1 CO2 Sensor, Humidity and Temperature Address = 0x62=
// PCA9548AD,118 8=Port Battery interface Address = 11101110 0xEE 
*/
