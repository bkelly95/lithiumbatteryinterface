#include "common.h"
#include "external_wake.h"
#include "uart.h"

int wakemask;

int wake_enable[10];

int dcl_first_state [7];
int dcl_second_state [7];
int dcl_state =0;

int stc_first_state [3];
int stc_second_state [3];
int stc_state =0;


int parse_wakemask (char * string){

long test_bitmask;
char * strptr;
char * not_used;

strptr = strchr(string, 0x20);
if (strptr == 0x00){
return 0;
}

test_bitmask=strtol(strptr, &not_used, 16);

if ((test_bitmask > 0x3FF) || (test_bitmask < 0)){

return 0;

}

wakemask = (int)test_bitmask;

int i;
for (i=0;i!=10;i++){

wake_enable[i]=(wakemask>>i)&0x1;

}




return 1;



}


void init_wake_sources (void){

if (BOARDTYPE==0){

wakemask = default_cpm_wakemask;

}

if (BOARDTYPE==2){

wakemask = default_stc_wakemask;

}

int i;
for (i=0;i!=10;i++){

wake_enable[i]=(wakemask>>i)&0x1;

}








}


void set_wakemask (int bitmask){

wakemask = bitmask;

int i;
for (i=0;i!=10;i++){

wake_enable[i]=(wakemask>>i)&0x1;

}

}





int dcl_wake(int wake_1, int wake_2, int wake_3, int wake_4, int wake_5, int wake_6, int wake_7){

if (dcl_state==0){

dcl_first_state[0] = wake_1&wake_enable[0];
dcl_first_state[1] = wake_2&wake_enable[1];
dcl_first_state[2] = wake_3&wake_enable[2];
dcl_first_state[3] = wake_4&wake_enable[3];
dcl_first_state[4] = wake_5&wake_enable[4];
dcl_first_state[5] = wake_6&wake_enable[5];
dcl_first_state[6] = wake_7&wake_enable[6];

dcl_state++;
return 0;
}

if (dcl_state==1){

dcl_state=0;

dcl_second_state[0] = wake_1&wake_enable[0];
dcl_second_state[1] = wake_2&wake_enable[1];
dcl_second_state[2] = wake_3&wake_enable[2];
dcl_second_state[3] = wake_4&wake_enable[3];
dcl_second_state[4] = wake_5&wake_enable[4];
dcl_second_state[5] = wake_6&wake_enable[5];
dcl_second_state[6] = wake_7&wake_enable[6];

// Falling edge results in difference 1 between first and second states

if ( ((dcl_first_state[0]-dcl_second_state[0])==1) || ((dcl_first_state[1]-dcl_second_state[1])==1) || ((dcl_first_state[2]-dcl_second_state[2])==1) ||
((dcl_first_state[3]-dcl_second_state[3])==1) || ((dcl_first_state[4]-dcl_second_state[4])==1) || ((dcl_first_state[5]-dcl_second_state[5])==1) ||
((dcl_first_state[6]-dcl_second_state[6])==1) ) {

#ifdef external_wake_debug
UART1_putstring("Falling edge detected!");
UART1_newline();
#endif

// Falling edge detected, wake valid...
return 1;

}

}      

// No falling edge detected, no wake...
return 0;


}


int stc_wake(int wake_1, int wake_2, int wake_3, int wake_5, int wake_7){

#ifdef wake_from_STC_serial_chan

if (stc_state==0){

stc_first_state[0] = wake_3&wake_enable[2];
stc_first_state[1] = wake_5&wake_enable[4];
stc_first_state[2] = wake_7&wake_enable[6];

stc_state++;
return 0;
}

if (stc_state==1){
stc_state=0;

stc_second_state[0] = wake_3&wake_enable[2];
stc_second_state[1] = wake_5&wake_enable[4];
stc_second_state[2] = wake_7&wake_enable[6];

// Falling edge results in difference 1 between first and second states

if ((((stc_first_state[0]-stc_second_state[0])==1) || ((stc_first_state[1]-stc_second_state[1])==1) || ((stc_first_state[2]-stc_second_state[2])==1)) || ((wake_1&0) ==1) || ((wake_2&wake_enable[9])==1))     {


#ifdef external_wake_debug
UART1_putstring("Wake detected!");
UART1_newline();
#endif
return 1;

}

else{
return 0;
}

}

#endif


// No bitmask for STC wake_1.
if ( (  (wake_1&0) ==1) || ((wake_2&wake_enable[9])==1)){

#ifdef external_wake_debug
UART1_putstring("Wake detected!");
UART1_newline();
#endif

return 1;
}


return 0;

}
