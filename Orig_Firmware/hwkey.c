#include "common.h"
#include "hwkey.h"
#include "GPIO_pin_assignments.h"
#include "timers.h"
#include "CE.h"

int test_hwkey (unsigned int key){

// init clock
LATCH_CLK = 0;


SHIFT_CLK = 0;
SHIFT_DATA = (key >> 7)&0x1;
blocking_delay(1);
SHIFT_CLK = 1;
blocking_delay(1);


SHIFT_CLK = 0;
SHIFT_DATA = (key >> 6)&0x1;
blocking_delay(1);
SHIFT_CLK = 1;
blocking_delay(1);


SHIFT_CLK = 0;
SHIFT_DATA = (key >> 5)&0x1;
blocking_delay(1);
SHIFT_CLK = 1;
blocking_delay(1);

SHIFT_CLK = 0;
SHIFT_DATA = (key >> 4)&0x1;
blocking_delay(1);
SHIFT_CLK = 1;
blocking_delay(1);

SHIFT_CLK = 0;
SHIFT_DATA = (key >> 3)&0x1;
blocking_delay(1);
SHIFT_CLK = 1;
blocking_delay(1);

SHIFT_CLK = 0;
SHIFT_DATA = (key >> 2)&0x1;
blocking_delay(1);
SHIFT_CLK = 1;
blocking_delay(1);

SHIFT_CLK = 0;
SHIFT_DATA = (key >> 1)&0x1;
blocking_delay(1);
SHIFT_CLK = 1;
blocking_delay(1);

SHIFT_CLK = 0;
SHIFT_DATA = (key)&0x1;
blocking_delay(1);
SHIFT_CLK = 1;
blocking_delay(1);

LATCH_CLK = 1;

LATCH_CLK = 0;
SHIFT_CLK = 0;

// Currently not supported, could read output of comparator here.

return 1;

}



int parse_hwkey (char * string){

char * strptr;
char * not_used;

unsigned int key;
unsigned long DCL_bitmask;

strptr = strchr(string, 0x20);
if (strptr == 0x00){
return 0;
}

key = atoi(strptr);

if (key != 185){
return 0;
}

strptr = strchr(strptr+1, 0x20);
if (strptr == 0x00){
return 0;
}

DCL_bitmask = strtol(strptr, &not_used, 16);
if (DCL_bitmask > 127){
return 0;
}

int i;
int testbit;
for (i=0; i!=7; i++){

testbit = (DCL_bitmask>>i)&0x1;

switch (i){

case 0:

Ien_DCL1 = testbit;
TE_CH1 = OFF;
DE_1 = testbit;
break;

case 1:

Ien_DCL2 = testbit;
TE_CH2 = OFF;
DE_2 = testbit;
break;

case 2:

Ien_DCL3 = testbit;
TE_CH3 = OFF;
DE_3 = testbit;
break;

case 3:

Ien_DCL4 = testbit;
TE_CH4 = OFF;
DE_4 = testbit;
break;

case 4:

Ien_DCL5 = testbit;
TE_CH5 = OFF;
DE_5 = testbit;
break;

case 5:

Ien_DCL6 = testbit;
TE_CH6 = OFF;
DE_6 = testbit;
break;

case 6:

Ien_DCL7 = testbit;
TE_CH7 = OFF;
DE_7 = testbit;
break;



}


if (testbit==1){
active_DCL=active_DCL|(1<<i);
}
else{
active_DCL=active_DCL&(~(1<<i));
}

}


test_hwkey(key);

return 1;

}

void clear_hwkey (void){

test_hwkey(0);

}


void clear_DCL_states (void){

Ien_DCL1 = OFF;
TE_CH1 = OFF;
DE_1 = OFF;

Ien_DCL2 = OFF;
TE_CH2 = OFF;
DE_2 = OFF;

Ien_DCL3 = OFF;
TE_CH3 = OFF;
DE_3 = OFF;

Ien_DCL4 = OFF;
TE_CH4 = OFF;
DE_4 = OFF;

Ien_DCL5 = OFF;
TE_CH5 = OFF;
DE_5 = OFF;

Ien_DCL6 = OFF;
TE_CH6 = OFF;
DE_6 = OFF;

Ien_DCL7 = OFF;
TE_CH7 = OFF;
DE_7 = OFF;

active_DCL = 0;


}
