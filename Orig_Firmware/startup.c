#include "common.h"
#include "GPIO_pin_assignments.h"
#include "analog_pin_assignments.h"
#include "uart.h"
#include "uart_fifo.h"
#include "i2c.h"
#include "HTU31D.h"
#include "timers.h"
#include "SCD41.h"
#include "EEPROM.h"
#include "9546.h"
#include "ADC.h"
#include "9548.h"
#include "LED.h"



/* 

********************************* EEPROM Emulation *******************

Uninitialized addresses return 0xFFFF (unsigned) with bit 1 of the EEPROM error
flag set (ignore).

We don't gain efficiency (e.g. minimize writes to a particular address) by using
multiple EEPROM "addresses", as the EEPROM emulation uses multiple pages within each bank
to write to a new program memory address each time (even though the emulated
EEPROM address may be the same, this is transparent to the user). Once a page is full it it "packed",
and the next page utilized for further writes (this requires a minimum of two pages per bank).

DataEEWrite(data, address)
DataEERead(address)

EEPROM MAP:

Addr		Val			Desc.

0			0/1			Reserved (possible cold-start flag)
1			0/1			SBD force restart

10			count		Start count...
20	 		0/1			Start count bootstrap

100			0/1/2		Boardtype_switch


**********************************************************************


BOARDTYPE:
0 = Supervisor
1 = DCL
2 = STC


Startup steps:

1.) Get BOARDTYPE (either from #define or shorting plug, EEPROM)

2.) Determine event:
	- Cold start (most likely define by shorting plug)
	- Watchdog timeout
	- Intentional reboot (rare)
	- Unknown (most likely supply voltage interrupted)

3.) If cold start, set default values to pins and EEPROM, set cold start bit in EEPROM

4.) If watchdog / intentional/ unknown reset, update wake status, set critical outputs to last known values,
	rest to cold-boot defaults.

*/

// set in common.h
const int BOARDTYPE=boardtype_override;

// default to primary
int CPM_PRIMARY_SEL = 1;

// flag for SBD-based restart
int SBD_restart = 0;

void master_pic_init (void) {

// Read EERPOM addresses, load values
SBD_restart = DataEERead(1);
Nop();

reset_bootstrap = DataEERead(20);
if (reset_bootstrap != 1){

DataEEWrite(1, 20);
Nop();
DataEEWrite(0, 10);
Nop();
reset_count = 0;
}

else{

reset_count = DataEERead(10);
Nop();
reset_count++;
DataEEWrite(reset_count, 10);
Nop();
}


// Default condition cold-start at boot
force_update_CE_wake_status(1);

// If we had a watchdog timeout...
if (RCONbits.WDTO==1){
force_update_CE_wake_status(5);
}



// SBD-based resets?
/*
if(SBD_restart ==1){
// Clear
DataEEWrite(0,1);
force_update_CE_wake_status(9);
}

*/

/* 
	Assign default values to pins. States can't be retained across a reboot.

	First group of pins assigned are version-dependent pins (if any exist depending on 
	BOARDTYPE). Next are standard pin-assignments, followed by unused pin assignments.

	Following pins must be set low prior to HOT3V3 being enabled (CPM2 only):

	LT4151_SHDN		(pin 8)
	PSC_Reset		(pin 10)
	SWFD_RESET		(pin 29)
	Ien_PSC			(pin 57)
	

	
	

	


*/


// ******************* CPM default pin assignments ****************

if (BOARDTYPE==0){ // if board is CPM




// ********** Version dependent pin assignments **********


// Initial CPM version:
if (CPM_version == 1){

// power for irid is off
_12V_ENA = OFF_AL;

// power for fwwf is off
_24V_ENA = OFF_AL;

// No leak detect enable in initial version, set low
LEAK_DETECT_ENABLE = OFF;

// Enable LT3573... later versions pin is unused (LT3573 assigned to HOT3V3_ENABLE), safe to maintain state
HOTPcon = ON_AL;

// Env. sensor power on, active-low
SensPcon = ON_AL;

// NO LT4151 shutdown, active high
LT4151_SHDN = OFF;
}

// Later versions
else{

// leak detect enabled in later versions, active-low disable
LEAK_DETECT_ENABLE =OFF_AL;

// No control of 12V, set low
_12V_ENA = OFF;

// No control of 24V, set low
_24V_ENA = OFF;

// Not used
HOTPcon = OFF;

// Primary / secondary status will set this bit, temporarily off
HOT3V3_ENA = OFF_AL;

// NO LT4151 shutdown, active low
LT4151_SHDN = OFF_AL;
}

// *******************************************************




// *************** Standard pin assignments **************

// Turn on CE PFET following delay, active low
CEcon = CE_OFF;
blocking_delay(250);
CEcon = CE_ON;


// Primary / secondary status will set this bit, temporarily off
PSC_Reset = OFF;

// Set to GPS as default PPS source
PPS_src_select = 0;

// Turn off SWFD channel (LTM2881), active high
SWFD_ENA = OFF;

// Primary / secondary status will set this bit, temporarily off
SWFD_RESET = OFF;

// Turn off SWFD_SWENA (switch), active low
SWFD_SWENA = OFF_AL;

// Turn off DCL (LTM2881) ports. Active high.

Ien_DCL1 = OFF; // DCL port 0 off
Ien_DCL2 = OFF; // DCL port 1 off
Ien_DCL3 = OFF; // DCL port 2 off
Ien_DCL4 = OFF; // DCL port 3 off
Ien_DCL5 = OFF; // DCL port 4 off
Ien_DCL6 = OFF; // DCL port 5 off
Ien_DCL7 = OFF; // DCL port 6 off

// 9522 channel off, active high
Ien_9522 = OFF;

// GPS is ON, active low (SUPERBOARD)
Ien_GPS = ON_AL;

// RFM channel off, active high.
Ien_RFM = OFF;

// 9602 channel off, active low (SUPERBOARD)
Ien_9602 = OFF_AL;

// Primary / secondary status will set this bit, temporarily off
Ien_PSC= OFF;

// Turn off 120 terminator resistor (LTM2881) enables for DCL ports. Active high.
TE_CH1 = OFF; // DCL port 1 TE off
TE_CH2 = OFF; // DCL port 2 TE off
TE_CH3 = OFF; // DCL port 3 TE off
TE_CH4 = OFF; // DCL port 4 TE off
TE_CH5 = OFF; // DCL port 5 TE off
TE_CH6 = OFF; // DCL port 6 TE off
TE_CH7 = OFF; // DCL port 7 TE off

// Turn off drivers (LTM2881) for DCL ports. Active high.
DE_1 = OFF; // DCL port 0 driver off
DE_2 = OFF; // DCL port 1 driver off
DE_3 = OFF; // DCL port 2 driver off
DE_4 = OFF; // DCL port 3 driver off
DE_5 = OFF; // DCL port 4 driver off
DE_6 = OFF; // DCL port 5 driver off
DE_7 = OFF; // DCL port 6 driver off

// Enable driver for PSC
DE_PSC = ON;

// *******************************************************




// ******************** Unused pins **********************


// Pins 33,34,40,64

// Pin 33
// HOTPcon in initial version, already OFF

// Pin 34
// Version dependent, see above

// Pin 64
// Version dependent, see above


// *******************************************************




}




// ******************* DCL default pin assignments ****************

if (BOARDTYPE==1){ // if board is DCL

// ********** Version dependent pin assignments **********

if (DCL_version ==1){
// Env. sensor power on (enables engineering sensors AND low/high power board temperature measurements, must be kept on)
SensPcon = ON_AL;

// Not used (same as DCL_SENS_PCON_TSENSE)
HOTPcon = OFF;

// No leak detect enable in initial version, set low
LEAK_DETECT_ENABLE = OFF;

// Turn on CE, active low
CEcon = CE_ON;

// LTC 4151 on, active low
LT4151_SHDN = ON_AL;

}

else{
// not used
SensPcon = OFF;

// High/low power board temperature sense
DCL_SENS_PCON_TSENSE = OFF_AL;

// leak detect enabled in later versions, active-low disable
LEAK_DETECT_ENABLE = OFF_AL;

// Turn on CE following delay, active high
CEcon = CE_OFF;
blocking_delay(250);
CEcon = CE_ON;

// LTC 4151 on, active high
LT4151_SHDN = ON;

}


// *******************************************************




// *************** Standard pin assignments **************

// Set to oscillator as default PPS source
PPS_src_select = 0;

// Turn off SWFD channel (LTM2881), active high
SWFD_ENA = OFF;

// No resetting of SWFD, active-low
SWFD_RESET = OFF_AL;

// Turn off SWFD_SWENA (switch), active low
SWFD_SWENA = OFF_AL;

// 12V OFF
_12V_ENA = OFF_AL;

// 24V OFF
_24V_ENA = OFF_AL;

// Channels initially off
CH1_IEN = OFF; // Sensor ch. 0 off
CH2_IEN = OFF; // Sensor ch. 1 off
CH3_IEN = OFF; // Sensor ch. 2 off
CH4_IEN = OFF; // Sensor ch. 3 off
CH5_IEN = OFF; // Sensor ch. 4 off
CH6_IEN = OFF; // Sensor ch. 5 off
CH7_IEN = OFF; // Sensor ch. 6 off
CH8_IEN = OFF; // Sensor ch. 7 off

// Console always ON (might change for further power reduction)
CON_IEN = ON; 

// Turn off 120 terminator resistor (LTM2881) enables for sensors.
CH3_TE = OFF; // channel 3 TE off
CH4_TE = OFF; // channel 4 TE off
CH7_TE = OFF; // channel 7 TE off
CH8_TE = OFF; // channel 8 TE off


// *******************************************************




// ******************** Unused pins **********************
// Pins 2,3,6,10,13,14,16,17,37,38,39,40,41,42,43,44,49,54,56,57,72,73,74,75

// Pin 2. Normally input, override here as output, set low.
TRISEbits.TRISE6 = 0;
LATEbits.LATE6 = OFF;

// Pin 3
TE_CH6 = OFF;

// Pin 6
SHIFT_DATA = OFF;

// Pin 10
PSC_Reset = OFF;

// Pin 13
TE_CH7 = OFF;

// Pin 14
DE_7 = OFF;

// Pin 16
MP_ACTIVE = OFF;

// Pin 17. Normally input, override here as output, set low.
TRISBbits.TRISB3 = 0;
LATBbits.LATB3 = OFF;

// Pin 37. Normally assigned to UART3, override as GPIO output, set low.
TRISDbits.TRISD14 = 0;
LATDbits.LATD14 =OFF;

// Pin 38. Normally assigned to UART3, override as GPIO output, set low.
TRISDbits.TRISD15 = 0;
LATDbits.LATD15 =OFF;

// Pin 39. Normally assigned to UART4, override as GPIO output, set low.
TRISFbits.TRISF4 = 0;
LATFbits.LATF4 =OFF;

// Pin 40. Normally assigned to UART4, override as GPIO output, set low.
TRISFbits.TRISF5 = 0;
LATFbits.LATF5 =OFF;

// Pin 41
DE_4 = OFF;

// Pin 42
DE_3 = OFF;

// Pin 43
DE_6 = OFF;

// Pin 44
DE_5 = OFF;

// Pin 49
PPS_src_select = OFF;

// Pin 54
Ien_GPS = OFF;

// Pin 56
Ien_9602 = OFF;

// Pin 57
Ien_PSC = OFF;

// Pin 72
DE_1 = OFF;

// Pin 73
DE_2 = OFF;

// Pin 74
SHIFT_CLK = OFF;

// Pin 75
LATCH_CLK = OFF;

// *******************************************************

}




// ******************* STC default pin assignments ****************




// ********** Version dependent pin assignments **********

// *******************************************************




// *************** Standard pin assignments **************

if (BOARDTYPE==2){ // if board is STC

// Turn on CE PFET following delay, active low
CEcon = CE_OFF;
blocking_delay(250);
CEcon = CE_ON;

// LTC 4151 on, active low
LT4151_SHDN = OFF_AL;

// Set to GPS as default PPS source
PPS_src_select = 0;

// Turn off SWFD channel (LTM2881), active high
SWFD_ENA = OFF;

// No resetting of SWFD, active-low
SWFD_RESET = OFF_AL;

// Turn off SWFD_SWENA (switch), active low
SWFD_SWENA = OFF_AL;

// Env. sensor power on, active-low (always on)
SensPcon = ON_AL;

// Turn off DCL (LTM2881) ports. Active high.

CH1_IEN = OFF_AL; // STC port 1 off
CH2_IEN = OFF_AL; // STC port 2 off
CH3_IEN = OFF; // STC port 3 off
CH5_IEN = OFF; // STC port 5 off
CH7_IEN = OFF; // STC port 7 off

// Sensor power off
STC_12V_SENSOR_PWR = OFF_AL;

// 9522 channel off, active high
Ien_9522 = OFF;

// GPS is ON, active low (SUPERBOARD)
Ien_GPS = ON_AL;

// RFM channel off, active high.
Ien_RFM = OFF;

// 9602 channel off, active low (SUPERBOARD)
Ien_9602 = OFF_AL;

// Turn off 120 terminator resistor (LTM2881) enables

CH3_TE = OFF; // channel 3 TE off
CH4_TE = OFF; // channel 4 TE off
CH7_TE = OFF; // channel 7 TE off
CH8_TE = OFF; // channel 8 TE off

// Leak detect off
LEAK_DETECT_ENABLE = OFF_AL;

// CH2 flag_out off
STC_CH2_FLAG_OUT = OFF;

// CH1 flag_out off
STC_CH1_FLAG_OUT = OFF;

// *******************************************************




// ******************** Unused pins **********************
// Pins 3,5,6,10,13,14,17,33,39,40,41,42,43,44,50,57,60,72,73,74,75

// Pin 3
TE_CH6 = OFF;

// Pin 5
CH4_TE = OFF;

// Pin 6
SHIFT_DATA = OFF;

// Pin 10
PSC_Reset = OFF;

// Pin 13
TE_CH7 = OFF;

// Pin 14
DE_7 = OFF;

// Pin 17
MP_PRIMARY_SEL = OFF;

// Pin 33
HOTPcon = OFF;

// Pin 39. Normally assigned to UART4, override as GPIO output, set low.
TRISFbits.TRISF4 = 0;
LATFbits.LATF4 =OFF;

// Pin 40. Normally assigned to UART4, override as GPIO output, set low.
TRISFbits.TRISF5 = 0;
LATFbits.LATF5 =OFF;

// Pin 41
DE_4 = OFF;

// Pin 42
DE_3 = OFF;

// Pin 43
DE_6 = OFF;

// Pin 44
DE_5 = OFF;

// Pin 50
TE_CH5 = OFF;

// Pin 57
Ien_PSC = OFF;

// Pin 60
CH8_TE = OFF;

// Pin 72
DE_1 = OFF;

// Pin 73
DE_2 = OFF;

// Pin 74
SHIFT_CLK = OFF;

// Pin 75
LATCH_CLK = OFF;

// *******************************************************

}



// Specific tasks for CPM go here.
if (BOARDTYPE==0){

// enable UARTs
setUART1();
setUART2();
setUART3();
setUART4();

blocking_delay(1);

// Clear CE RX FIFO
clear_UART1_fifo();

// enable LED status
init_LED();

// Determine if we're primary / backup
CPM_PRIMARY_SEL = MP_PRIMARY_SEL; 

// If we're the primary CPM...
if (CPM_PRIMARY_SEL==1){

#ifdef iridium_on_boot
power_on_irid(1);
#endif

#ifdef fwwf_on_boot
power_on_fwwf(1);
#endif

#ifdef heartbeats_enabled_on_boot


CPM_heartbeat_del_t = default_heartbeat_dtime+heartbeat_additional_time;
CPM_heartbeat_threshold = default_heartbeat_threshold;

// Enable last in sequence
CPM_heartbeats_enabled = 1;

// Enable timer
enable_heartbeat_timer();
			
#endif

// Enable HOT3V3, set PSC / SWFD reset pins to default states

// Enable HOT3v3
HOT3V3_ENA = ON_AL;

// No initial PSC reset (inverted)
PSC_Reset = OFF_AL;

// Enable PSC channel, active high
Ien_PSC= ON;

// No resetting of SWFD, active-low
SWFD_RESET = OFF_AL;


}

else if (CPM_PRIMARY_SEL==0){

// Disable polling of engineering data
CE_stream_data = 0;

// Disable heartbeats
disable_heartbeats();

// Remove power to CE
CEcon = CE_OFF;

// Turn off HOT3V3
if (CPM_version==1){
HOTPcon=OFF_AL;
}

// Turn off HOT3V3
else{

// Disable HOT3v3
HOT3V3_ENA = OFF_AL;

// Set low
Ien_PSC= OFF;

// Set low
PSC_Reset = OFF;

// Set low
SWFD_RESET = OFF;

}

// stop checking of GPS
disable_GPS();

// Clear wake status
clear_CE_wake_status();

// disable all external wake sources (PSC)
set_wakemask(0);

}




}

// Specific tasks for DCL go here.
if (BOARDTYPE==1){

// Enable UARTs
setUART1();
setUART2();

blocking_delay(1);

// Clear CE RX FIFO
clear_UART1_fifo();


// No checking of PPS if the board is a DCL.
// Disable GPS.
disable_GPS();

}




// Specific tasks for STC go here.
if (BOARDTYPE==2){

// Enable UARTs
setUART1();
setUART2();
setUART3();

blocking_delay(1);

// Clear CE RX FIFO
clear_UART1_fifo();

// enable LED status
init_LED();


#ifdef iridium_on_boot
power_on_irid(1);
#endif

#ifdef fwwf_on_boot
power_on_fwwf(1);
#endif


#ifdef heartbeats_enabled_on_boot

CPM_heartbeat_del_t = default_heartbeat_dtime+heartbeat_additional_time;
CPM_heartbeat_threshold = default_heartbeat_threshold;

// Enable last in sequence
CPM_heartbeats_enabled = 1;

// Enable timer
enable_heartbeat_timer();
			
#endif

}




}
