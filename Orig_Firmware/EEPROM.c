/*********************************************************************
 * FileName: 		LT4151.c
 * Dependencies: 	LT5151.h (include here and in main)
 *					common.h (include here and in main)
 *					I2C.h (include here and in main)
 ********************************************************************/


/*********************************************************************
 * Function:        	void get_LT4151_main_voltage (void), void get_LT4151_main_current (void)
 * PreCondition:    	None.
 * Input:           	None.
 * Output:          	None.
 * Side Effects:    	None.
 * Overview:       		Function for obtaining LT4151 voltage via I2C
 *						bus. All ACKs from LT4151 are checked.
 *
 ********************************************************************/

#include "common.h"
#include "EEPROM.h"
#include "I2C.h"
