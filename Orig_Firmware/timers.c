/*********************************************************************
 * FileName: 		timers.c
 * Dependencies: 	timers.h (include here and in main)
 *					common.h (include here and in main)
 *
 ********************************************************************/


/*********************************************************************
 * Function:        	void set_timers (int)
 * PreCondition:    	None.
 * Input:           	None.
 * Output:          	None.
 * Side Effects:    	None.
 * Overview:       		Sets up timers.
 *
 ********************************************************************/

#include "common.h"
#include "timers.h"


// Common to all versions

int CE_stream_data = 1;
int CE_stream_count = 0;
int CE_stream_rate = 2;
int CE_send_data = 0;

unsigned long elapsed_time = 0;


// Only valid for supervisor / STC config.

// Variables for wake counts.

int CPM_off_count=0;
unsigned long CPM_on_count=0;

// Alarm flags to be polled in main.

int CPM_off_alarm=0;
int CPM_on_alarm=0;

// Error flag for I2C timeout
int I2C_timeout; 

// Heartbeat variables.

int CPM_heartbeats_enabled = 0;
int CPM_heartbeat_del_t = 0;
int CPM_heartbeat_threshold = 0;

int CPM_heartbeat_sec_count = 0;
int CPM_missed_heartbeat_count = 0;
int CPM_heartbeat_alarm = 0; // flag for main indicating no heartbeat within delt
int CPM_no_hbeat_force_reboot=0; // flag for main indicating no heartbeat within delt and thresholds exceeded; force restart
int CPM_force_reboot_count=0;
int CPM_switch = 0;

// SBD variables.

int send_SBD_status = 0;
int get_SBD_msg = 0;
int SBD_sec_count = 0;
int SBD_status_alarm_trig = SBD_default_status_msg_trig;
int SBD_receive_trig = SBD_default_rec_msg_trig;

// GFLT variables.

int gflt_sec_count = 0;
int get_gflt = 0;
int gflt_trig_T = default_gflt_trig_T;

// Need test statements for DCL

int pps_count_enabled=1;

// Should be able to safely ignore for DCL.

int PSC_sec_count = 0;
int PSC_string_rec_alarm = 0;

// Should be able to safely ignore for DCL.

int pps_count = 0;

// Local flag for non-blocking I2C / general-purpose variable millisecond timer
int sel_I2C_watchdog = 0;


void set_timers(void){

// Timer 1, variable blocking / I2C watchdog / variable one-shot non-blocking

T1CONbits.TON = 0; // Disable Timer
T1CONbits.TCS = 0; // Select internal instruction cycle clock
T1CONbits.TGATE = 0; // Disable Gated Timer mode
T1CONbits.TCKPS = 0b01; // Select 1:8 Prescaler

// Timer 3 - 1 second timer

T2CONbits.TON = 0;
T2CON = 0;
T3CON = 0;
TMR3 = 0;
TMR2 = 0;;
PR3 = 0b111101;
PR2 = 0b0000100100000000;
IFS0bits.T3IF = 0;
IEC0bits.T3IE = 1;
T2CONbits.T32 = 1; // enable 32 bit mode
T2CONbits.TON = 1;

// Timer 5 - 1 second timer, dedicated for heartbeats

T4CONbits.TON = 0;
T4CON = 0;
T5CON = 0;
TMR4 = 0;
TMR5 = 0;;
PR5 = 0b111101;
PR4 = 0b0000100100000000;
IFS1bits.T5IF = 0;
IEC1bits.T5IE = 0; // disable interrupts initially
T4CONbits.T32 = 1; // enable 32 bit mode


}

void enable_heartbeat_timer(void){
IEC1bits.T5IE = 1; // enable interrupts
T4CONbits.TON = 1; // enable timer

}


void disable_heartbeat_timer(void){
IEC1bits.T5IE = 0; // disable interrupts
T4CONbits.TON = 0; // disable timer

}



/*********************************************************************
 * Function:        	void blocking_delay (unsigned int)
 * PreCondition:    	None.
 * Input:           	None.
 * Output:          	None.
 * Side Effects:    	None.
 * Overview:       		Millisecond-based blocking delay.
 *
 ********************************************************************/

void blocking_delay (unsigned int milliseconds){

unsigned int adjusted_period = 500*milliseconds;

// Timer period is (clock period / 4)*8 or 2us.

T1CONbits.TON = 0; // reset
TMR1 = 0x00; // Clear timer register
PR1 = adjusted_period;

IFS0bits.T1IF = 0; // Clear Timer1 Interrupt Flag
IEC0bits.T1IE = 0; // Disable interrupts
T1CONbits.TON = 1; // Start Timer

// Read interrupt flag, block while not set
while (IFS0bits.T1IF == 0);
T1CONbits.TON = 0;

}




/*********************************************************************
 * Function:        	void I2C_watchdog (void)
 * PreCondition:    	None.
 * Input:           	None.
 * Output:          	None.
 * Side Effects:    	None.
 * Overview:       		I2C watchdog timer (set for 5 ms). Uses interrupts.
 *
 ********************************************************************/


void I2C_watchdog (void){

// Flag "I2C_timeout" is normally tested against ACK from the I2C device. If we receive ACK before 5ms, I2C tasks proceed normally; the timer remains active
// and the I2C timeout flag is eventually set but ignored. 

// stop timer
T1CONbits.TON = 0;

// I2C watchdog
sel_I2C_watchdog =1;

IFS0bits.T1IF = 0; // reset
I2C_timeout = 0; // reset flag

unsigned int adjusted_period = 500*5; // 5 ms timeout

TMR1 = 0x00; // Clear timer register
PR1 = adjusted_period;
IFS0bits.T1IF = 0; // Clear Timer1 Interrupt Flag
IEC0bits.T1IE = 1; // Enable interrupts
T1CONbits.TON = 1; // Start Timer


}


int timer_B_alarm;

void one_shot_non_blocking_delay_B(int milliseconds){

// stop timer
T1CONbits.TON = 0;

// non-blocking one-shot
sel_I2C_watchdog =0;

IFS0bits.T1IF = 0; // reset
timer_B_alarm = 0; // reset flag

unsigned int adjusted_period = 500*milliseconds; // 5 ms timeout

TMR1 = 0x00; // Clear timer register
PR1 = adjusted_period;
IFS0bits.T1IF = 0; // Clear Timer1 Interrupt Flag
IEC0bits.T1IE = 1; // Enable interrupts
T1CONbits.TON = 1; // Start Timer


}




/*********************************************************************
 * Function:        	void non_blocking_delay_A (int)
 * PreCondition:    	None.
 * Input:           	None.
 * Output:          	None.
 * Side Effects:    	None.
 * Overview:       		Starts non-blocking delay. Uses interrupts.
 *
 ********************************************************************/

int timer_A_idle;
int timer_A_alarm;
int timer_A_val;

void non_blocking_delay_A(int delay_sec){
timer_A_idle = 0;
delay_sec++;
timer_A_val=delay_sec;
}




/*********************************************************************
 * Function:        	void reset_timer_A (void)
 * PreCondition:    	None.
 * Input:           	None.
 * Output:          	None.
 * Side Effects:    	None.
 * Overview:       		Resets timer.
 *
 ********************************************************************/

void reset_timer_A(void){
timer_A_alarm=0;
timer_A_val=0;
}




/*********************************************************************
 * Function:        	void _T1Interrupt (void)
 * PreCondition:    	None.
 * Input:           	None.
 * Output:          	None.
 * Side Effects:    	None.
 * Overview:       		Corresponding ISR for I2C watchdog. Sets flag indicating
 * 						that a timeout has occured, disables interrupts, and clears
 * 						interrupt flag.
 *
 ********************************************************************/

void __attribute__((__interrupt__, no_auto_psv)) _T1Interrupt(void){

// if we're using this for watchdog timeouts...
if (sel_I2C_watchdog==1){
// clear flag anyway
sel_I2C_watchdog=0;
I2C_timeout =1; // set flag
}

// else defaults to general-purpose non-blocking delay timer
else {
timer_B_alarm=1;
}

IEC0bits.T1IE = 0; // disable interrupts
IFS0bits.T1IF = 0; // clear interrupt flag

}




/*********************************************************************
 * Function:        	void _T3Interrupt (void)
 * PreCondition:    	None.
 * Input:           	None.
 * Output:          	None.
 * Side Effects:    	None.
 * Overview:       		Corresponding ISR for general-purpose 1 second
 *						timer.
 *
 ********************************************************************/

void __attribute__((__interrupt__, no_auto_psv)) _T3Interrupt(void){ 


gflt_sec_count++;
if (gflt_sec_count>=gflt_trig_T){
gflt_sec_count=0;
get_gflt = 1;
}

// increment run time
elapsed_time++;

if (pps_count_enabled ==1){
pps_count++;
}

if (CE_stream_data==1){
CE_stream_count++;
if(CE_stream_count >= CE_stream_rate){
	CE_stream_count = 0;
	CE_send_data = 1;
}

}

if (CPM_off_count >= 1) {
	CPM_off_count--;
	if (CPM_off_count == 1){
		CPM_off_alarm=1;
		CPM_off_count--;
	}		
}

if (CPM_on_count >= 1) {
	CPM_on_count--;
	if (CPM_on_count == 1){
		CPM_on_alarm=1;
		CPM_on_count--;
	}		
}

PSC_sec_count++;
if (PSC_sec_count >= PSC_max_string_delt){

PSC_sec_count=0;
PSC_string_rec_alarm = 1;

}


SBD_sec_count++;
if (SBD_sec_count >= SBD_status_alarm_trig){

SBD_sec_count=0;
send_SBD_status = 1;


// avoids duplicate flags for message reception / status being set at timer rollover
} else if ( (SBD_sec_count % SBD_receive_trig)==0){
get_SBD_msg=1;

}





if (timer_A_val >= 1){
		timer_A_val--;
		if (timer_A_val == 1){
			timer_A_alarm=1;
			timer_A_idle = 1;
		}

}
else{

timer_A_idle = 1;
}
		

IFS0bits.T3IF = 0;

}


void __attribute__((__interrupt__, no_auto_psv)) _T5Interrupt(void){


if (CPM_heartbeats_enabled==1) {
	CPM_heartbeat_sec_count++;

	// Missed a heartbeat, set flag, start over
	if (CPM_heartbeat_sec_count >= CPM_heartbeat_del_t){

			CPM_heartbeat_alarm=1; // flag for main
			CPM_heartbeat_sec_count = 0;
			CPM_missed_heartbeat_count++;
			if (CPM_missed_heartbeat_count >= CPM_heartbeat_threshold){
				
			// Missed too many heartbeats, set flag, recommend 
			// switchover

				CPM_heartbeat_sec_count = 0;
				CPM_missed_heartbeat_count=0;
				CPM_no_hbeat_force_reboot=1; // flag for main
				CPM_force_reboot_count++;
				
					if (CPM_force_reboot_count >= CPM_max_no_hbeat_restarts)
						{
							//CPM_heartbeats_enabled = 0;
							CPM_heartbeat_sec_count = 0;
							CPM_missed_heartbeat_count = 0;
							CPM_force_reboot_count =0;
							CPM_switch = 1; // flag for main
						}
				
				
				
				}
			}	
		 
}


IFS1bits.T5IF = 0;


}


// resets timer
void reset_CE_wake_timer(void){

CPM_off_count =0;
CPM_on_count=0;

CPM_off_alarm=0;
CPM_on_alarm=0;

}
