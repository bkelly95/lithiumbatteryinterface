/*********************************************************************
 * FileName: 		uart.h
 * Dependencies: 	None
 * Overview:		Defines function prototypes, macros for setting up
 *					baudrates, checks for errors due to combinations of
 *					certain oscillator frequencies / baudrates. 
 *
 ********************************************************************/

#define UART1_strbuf_size 400
#define UART2_strbuf_size 150
#define UART2_chksum_strbuf_size 50

//******************** Begin function prototypes *********************

void setUART1 ();

void UART1_putstring(char *);
void UART1_putchar(char);
void UART1_newline();

extern char UART1_strbuf[];

//******************** End function prototypes ***********************

// Define main oscillator frequency
#define SYSCLK 8000000

// Baudrates
#define BAUDRATE1 115200 // Port STC

// UxBRG register values
#define BAUDRATEREG1 8
//#define BAUDRATEREG1 SYSCLK/8/BAUDRATE1-1

// Check baudrate values

#if (BAUDRATEREG1) > 255)
#error Cannot set up UART. Check values.
#endif

#define BAUDRATE_MISTAKE1 1000*(BAUDRATE1-SYSCLK/8/(BAUDRATEREG1+1))/BAUDRATE1
#if (BAUDRATE_MISTAKE1 > 2)||(BAUDRATE_MISTAKE1 < -2)
    #error UART1 baudrate mistake is too big.
#endif

int verify_checksum(char *str);
unsigned int gen_checksum (char *str);
