#define default_cpm_wakemask 0x100

#define default_stc_wakemask 0x200


extern int wakemask;

extern int wake_enable[];

int dcl_wake(int wake_1, int wake_2, int wake_3, int wake_4, int wake_5, int wake_6, int wake_7);

int stc_wake(int wake_1, int wake_2, int wake_3, int wake_5, int wake_7);

int parse_wakemask (char * string);

void init_wake_sources ();

void set_wakemask (int bitmask);
