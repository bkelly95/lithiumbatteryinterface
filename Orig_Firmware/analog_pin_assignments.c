/*********************************************************************
 * FileName: 		analog_pin_assignments.c
 * Dependencies: 	analog_pin_assignments.h (include here and in main)
 *					common.h (include here and in main)
 *
 ********************************************************************/


/*********************************************************************
 * Function:        	void set_analog_pins (void)
 * PreCondition:    	None.
 * Input:           	None.
 * Output:          	None.
 * Side Effects:    	None.
 * Overview:       		Sets up digital analog pins for use with the 
 *						ADC. Setting appropriate register of AD1PCFGL
 *						to "0" sets pin to analog input.
 *
 ********************************************************************/

#include "analog_pin_assignments.h"
#include "common.h"

void set_analog_pins (void) {

// RC2 hydrogen
    
#define hydrogen.RC2 = 0;


}
