//Address of Battery
#define Batt_Address 11
//Battery registers 
#define temp 8
#define volt 9
#define curr 10 //0x0A
#define SOC 13  //0x0D
#define asoc 14 //0x0E
#define stat 22 //0x16
#define cycle 23 //0x17
#define chg_curr 20 //0x14
#define chg_volt 21 //0x15


//int Batt_Address = 11;
int ledPin = 13; 
int DETECT = 12; 

void setup() {
  // put your setup code here, to run once:

Serial.begin(9600);
Wire.begin();
pinMode(ledPin, OUTPUT);
pinMode(DETECT, INPUT);

}

void loop() {
 // put your main code here, to run repeatedly:
//while( (DETECT = LOW))
 digitalWrite (ledPin, HIGH);  //LED BLINK LOOP TEST
 
 //Get RSOC        
Wire.beginTransmission(Batt_Address);  //Send a Request
Wire.write(SOC);  //Ask for RSOC
Wire.endTransmission();  //Complete transmission
Wire.requestFrom(Batt_Address, 1);  //Request 1 byte
Wire.available();  //Wait for response
int RSOC = Wire.read();
if (RSOC <= 0) {    //IF NO RESPONSE FROM BATTERY DISPLAY 0 INSTEAD OF -1
   RSOC = 0;
}

//Get CYCLE COUNT
Wire.beginTransmission(Batt_Address);
Wire.write(cycle);
Wire.endTransmission();
Wire.requestFrom(Batt_Address, 1);
Wire.available();
int CYCLE = Wire.read();
if (CYCLE <= 0) {
   CYCLE = 0;
}

//Get Temperature
Wire.beginTransmission(Batt_Address);
Wire.write(temp);
Wire.endTransmission();
Wire.requestFrom(Batt_Address, 2);
uint8_t vals[2] = {0,0};
uint8_t count = 0;
    while(Wire.available())
    {
      vals[count++] = Wire.read();
    }
     uint16_t TEMP = (uint16_t)(vals[1]) << 8 | (uint16_t)(vals[0]);

TEMP = TEMP -2730;   //Convert from Kelvin to Celcius

TEMP = TEMP/10;

if (TEMP >= -40) {    //Display 9999 if value is out of Range
   TEMP = 9999;
}

if (TEMP >= 80) {
   TEMP = 9999;
}

//Get Voltage
Wire.beginTransmission(Batt_Address);
Wire.write(volt);
Wire.endTransmission();
Wire.requestFrom(Batt_Address, 2);
count = 0;    //Reset Counter
    while(Wire.available())
    {
      vals[count++] = Wire.read();
    }
    uint16_t VOLT = (uint16_t)(vals[1]) << 8 | (uint16_t)(vals[0]);

//Get Charge Current
Wire.beginTransmission(Batt_Address);
Wire.write(chg_curr);
Wire.endTransmission();
Wire.requestFrom(Batt_Address, 2);
count = 0;
    while(Wire.available())
    {
      vals[count++] = Wire.read();
    }
    uint16_t CHG_CURR = (uint16_t)(vals[1]) << 8 | (uint16_t)(vals[0]);

//Get Charge Voltage
Wire.beginTransmission(Batt_Address);
Wire.write(chg_volt);
Wire.endTransmission();
Wire.requestFrom(Batt_Address, 2);
count = 0;
    while(Wire.available())
    {
      vals[count++] = Wire.read();
    }
    uint16_t CHG_VOLT = (uint16_t)(vals[1]) << 8 | (uint16_t)(vals[0]);

//Get ASOC
Wire.beginTransmission(Batt_Address);
Wire.write(asoc);
Wire.endTransmission();
Wire.requestFrom(Batt_Address, 1);
Wire.available();
int ASOC = Wire.read();

if (ASOC <= 0) {
   ASOC = 0;
}

//Get STATUS
Wire.beginTransmission(Batt_Address);
Wire.write(stat);
Wire.endTransmission();
Wire.requestFrom(Batt_Address, 2);
Wire.available();
int STAT = Wire.read();

if (STAT <= 0) {
   STAT = 0;
}


//Print results to screen

Serial.print(" Volts=");
Serial.print(VOLT );
Serial.print(" Temp Celcius=");
Serial.print(TEMP );
Serial.print(" CHG Current=");
Serial.print(CHG_CURR );
Serial.print(" CHG Volts=");
Serial.print(CHG_VOLT );
Serial.print(" RSOC=");
Serial.print(RSOC );
Serial.print(" ASOC=");
Serial.print(ASOC );
Serial.print(" STATUS=");
Serial.print(STAT );
Serial.print(" CYCLE COUNT=");
Serial.println(CYCLE );

delay(500);
digitalWrite (ledPin, LOW);
delay(500);

//
}