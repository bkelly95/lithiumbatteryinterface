/*
  Generating an echo on PC using PIC18F4550 USART ISR
  http://www.electronicwings.com
*/
#include <pic18f.h>
#include "Configuration_Header_File.h"
#include "LCD_16x2_8-bit_Header_File.h"

void USART_Init(long);

#define F_CPU 8000000/64
char out;

void main()
{
    OSCCON=0x72;
    LCD_Init();
    USART_Init(115200);    
    LCD_String_xy(1,0,"Receive");
    LCD_Command(0xC0);
    while(1);
}

void USART_Init(long baud_rate)
{   
    float temp;
    TRISC6=0;		/* Make Tx pin as output */
    TRISC7=1;		/* Make Rx pin as input */
    temp=(( (float) (F_CPU) / (float) baud_rate) - 1);     
    SPBRG=(int)temp;	/* Baud rate=9600 SPBRG=(F_CPU /(64*9600))-1 */
    TXSTA=0x20;		/* TX enable; */
    RCSTA=0x90;		/* RX enable and serial port enable */
    INTCONbits.GIE=1;	/* Enable Global Interrupt */
    INTCONbits.PEIE=1;	/* Enable Peripheral Interrupt */
    PIE1bits.RCIE=1;	/* Enable Receive Interrupt */
    PIE1bits.TXIE=1;	/* Enable Transmit Interrupt */
}


void interrupt ISR()
{
      while(RCIF==0);
      out=RCREG;	/* Copy received data from RCREG register */
      LCD_Char(out);
      while(TXIF==0);
      TXREG=out;	/* Transmit received(ECHO) to TXREG register */
}