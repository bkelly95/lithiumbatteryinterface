/* 
* File: main.c
* Author: www.studentcompanion.co.za
*
* Created on 26 June 2015, 10:57 PM
*/

#include <stdio.h>
#include <stdlib.h>
#include <usart.h>

#include "config.c" //configuration bits

/*
* 
*/

void main(void) {
TRISB=0x00;
OSCCON=0x76; //Configure OSCON Register to use
//internal oscillator. Please check datasheet
char data;
unsigned char Txdata1[] = "Welcome to studentcompanion.co.za rn";
for (int x=0; x<=10; x++) __delay_ms(50); //Generate 500ms delay
unsigned char Txdata2[] = "Press 1 for Red LED, 2 for Yellow LED and 3 for Green LED rn";
// configure USART frequency is 8MHz, using high speed, asynchronous mode is used with 9600 baud and 8 data bits.
//Fosc / (16 * (spbrg + 1 ))
// spbrg = FOSC/(16 * baud) - 1
// = 8000000/(16 � 9600) - 1 = 52

OpenUSART( USART_TX_INT_OFF &
USART_RX_INT_OFF &
USART_ASYNCH_MODE &
USART_EIGHT_BIT &
USART_CONT_RX &
USART_BRGH_HIGH &
USART_ADDEN_OFF,
52);
while(BusyUSART()); //Check if Usart is busy or not
putsUSART(Txdata1); //transmit the string1
for (int x=0; x<=20; x++) __delay_ms(50); //Generate 500ms delay
while(BusyUSART()); //Check if Usart is busy or not
putsUSART(Txdata2); //transmit the string2

for (int x=0; x<=20; x++) __delay_ms(50); //Generate 500ms delay
while(1){

data = ReadUSART();
switch(data){
case '1':LATBbits.LB0 = 1;
LATBbits.LB1 = 0;
LATBbits.LB2 = 0;

break;

case '2':LATBbits.LB0 = 0;
LATBbits.LB1 = 1;
LATBbits.LB2 = 0;

break;
case '3':LATBbits.LB0 = 0;
LATBbits.LB1 = 0;
LATBbits.LB2 = 1;

break;
default:LATBbits.LB0 = 0;
LATBbits.LB1 = 0;
LATBbits.LB2 = 0;
break;

}
}
}