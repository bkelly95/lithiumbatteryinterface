/*********************************************************************
 * FileName: 		I2C.c
 * Dependencies: 	I2C.h (include here and in main)
 *					common.h (include here and in main)
 *					timers.h (include here and in main)
 *
 ********************************************************************/


/*********************************************************************
 * Function:        	void i2c(task) (void)
 * PreCondition:    	None.
 * Input:           	None.
 * Output:          	None.
 * Side Effects:    	None.
 * Overview:       		Polled functions for common I2C bus operations.
 *
 ********************************************************************/

#include "common.h"
#include "i2c.h"
#include "timers.h"


int I2C_error = 0; // I2C error flag
int I2C_error_latch = 0; // latching I2C error flag - must be cleared in main

void setI2C (void) {
SSP2CON2.I2CEN = 0; // temporarily disable
SP2BRG = 0x13; // set baud for 400KHz FSCL
SSP2CON2.A10M = 0; // 7-bit address;
SSP2CON2.I2CEN = 1; // enable

}

void i2cstart(void){

	I2C_watchdog(); // start I2C watchdog timer

	SSP2CON2.SEN = 1;

	while((SSP2CON2.SEN==1) && (I2C_timeout == 0));

	if (I2C_timeout == 1) {
	SSP2CON2.I2CEN = 0; // enable
	SSP2CON2.I2CEN = 1; // enable
			I2C_error=1;
			I2C_error_latch = 1;
	}
 
}

void i2cstop(void){

	I2C_watchdog(); // start I2C watchdog timer

	SSP2CON2.PEN = 1;

	while((SSP2CON2.PEN==1)&& (I2C_timeout == 0));

	if (I2C_timeout == 1) {
	SSP2CON2.I2CEN = 0; // enable
	SSP2CON2.I2CEN = 1; // enable
			I2C_error=1;
			I2C_error_latch = 1;
	}
}


/*********************************************************************
 *		Special function for verifying ACK from slave. A timer is 
 *		started prior to waiting for the ACK; if recieved normally
 *		the device is assumed to be operating properly.
 *
 *		If the timer exceeds 5ms delay the "I2C_timeout" flag is set which
 *		sets an error flag, and all I2C tasks are halted.
 ********************************************************************/

void i2cgetack(void){

	I2C_error = 0; // clear the flag
	I2C_watchdog(); // start I2C watchdog timer

// block while both are true
while ((SSP2STATbits.ACKSTAT==1) && (I2C_timeout == 0)); 
	if (I2C_timeout == 1) {
	SSP2CON2.I2CEN = 0; // enable
	SSP2CON2.I2CEN = 1; // enable
			I2C_error=1;
			I2C_error_latch = 1;
	}
}




void i2csendack(void){

	I2C_watchdog(); // start I2C watchdog timer

	SSP2CON2.ACKDT=0;
	SSP2CON2.ACKEN=1;
	while((SSP2CON2.ACKEN==1)&& (I2C_timeout == 0));

	if (I2C_timeout == 1) {
	SSP2CON2.I2CEN = 0; // enable
	SSP2CON2.I2CEN = 1; // enable
			I2C_error=1;
			I2C_error_latch = 1;
	}

}

void i2csendnack(void){

I2C_watchdog(); // start I2C watchdog timer

	SSP2CON2.ACKDT=1;
	SSP2CON2.ACKEN=1;
	while((SSP2CON2.ACKEN==1)&& (I2C_timeout == 0));

	if (I2C_timeout == 1) {
	SSP2CON2.I2CEN = 0; // enable
	SSP2CON2.I2CEN = 1; // enable
			I2C_error=1;
			I2C_error_latch = 1;
	}
}

/* void i2cwrite(unsigned char c){

I2C_watchdog(); // start I2C watchdog timer

	I2C1TRN=c;
	while((SSP2STATbits.TRSTAT==1)&& (I2C_timeout == 0));

	if (I2C_timeout == 1) {
	SSP2CON2.I2CEN = 0; // enable
	SSP2CON2.I2CEN = 1; // enable
			I2C_error=1;
			I2C_error_latch = 1;
	}
}
*/
unsigned char i2cread(void){

I2C_watchdog(); // start I2C watchdog timer

	unsigned char c;
	SSP2CON2.RCEN=1;

	while((SSP2CON2.RCEN==1)&& (I2C_timeout == 0));
	if (I2C_timeout == 1) {
	SSP2CON2.I2CEN = 0; // enable
	SSP2CON2.I2CEN = 1; // enable
			I2C_error=1;
			I2C_error_latch = 1;
			return 0;
	}

	//c=I2C1RCV;
	return c;
}
