/*********************************************************************
 * FileName: 		uart_fifo.h
 * Dependencies: 	None
 * Overview:		Sets up global datatypes to be used in main for obtaining
 *					data from the receive buffer. 
 *
 *
 ********************************************************************/


//******************** FIFO configuration ****************************

#define UART1_FIFO_size 350
#define UART1_FIFO_depth 4

//********************************************************************




//******************** Common buffer variables ***********************

#define UART1_data_available UART1_dat >= 1
#define UART1_rx_data (UART1_rx_fifo[UART1_rx_fifo_rd])
#define UART1_receive_empty UART1_dat==0

//********************************************************************




//******************** Error flags ***********************************

extern int UART_error_latch;



//******************** Additional buffer variables *******************

extern int UART1_rx_fifo_wr;
extern int UART1_rx_fifo_rd;
extern int UART1_local_rx_index;
extern int UART1_dat;

extern char UART1_rx_fifo [UART1_FIFO_depth][UART1_FIFO_size];

//********************************************************************


//******************** Function prototypes ***************************

void clear_UART1_fifo ();
void update_UART1_fifo ();

//********************************************************************
