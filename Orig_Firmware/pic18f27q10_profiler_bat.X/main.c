/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.7
        Device            :  PIC18F26Q10
        Driver Version    :  2.00
 */

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
 */

#include "mcc_generated_files/mcc.h"
#include <stdio.h>

/*
                         Main application
 */

void EUSART1_sendString(const char *str)
{
    while(*str)
    {
        while (!(EUSART1_is_tx_ready()));
        EUSART1_Write(*str++);

    }
}

void main(void) {
    volatile uint8_t rxData;
    // Initialize the device
    SYSTEM_Initialize();
    TRISBbits.TRISB4 = 0; /* Configure the TRISA4 pin as output */
    TRISBbits.TRISB5 = 0; /* Configure the TRISA4 pin as output */
    LATBbits.LATB4 = 0; /* Drive the output on */
    LATBbits.LATB5 = 0; /* Drive the output off */

    // If using interrupts in PIC18 High/Low Priority Mode you need to enable the Global High and Low Interrupts
    // If using interrupts in PIC Mid-Range Compatibility Mode you need to enable the Global and Peripheral Interrupts
    // Use the following macros to:

    // Enable the Global Interrupts
    INTERRUPT_GlobalInterruptEnable();

    // Disable the Global Interrupts
    //INTERRUPT_GlobalInterruptDisable();

    // Enable the Peripheral Interrupts
    INTERRUPT_PeripheralInterruptEnable();

    // Disable the Peripheral Interrupts
    //INTERRUPT_PeripheralInterruptDisable();
    
    //EUSART1_Write(48);
    //EUSART1_Write('x');
    EUSART1_sendString("Hello World!\r\n");

    while (1) {
        _delay(400000);
        if (EUSART1_is_tx_ready()) {
            LATBbits.LATB4 = 1; /* Drive the output off */
            LATBbits.LATB5 = 0; /* Drive the output on */
            //EUSART1_Write('_');
            EUSART1_sendString("Hello World!\r\n");
        }

        if (EUSART1_is_tx_done()) {
           
        }
        
        if(EUSART1_is_rx_ready())
        {
           
            rxData = EUSART1_Read();
            if(EUSART1_is_tx_ready())
            {
                EUSART1_Write(rxData);
            }
        }
         
        //_delay(100000);
        // Add your application code
        //LATBbits.LATB4 = 1; /* Drive the output off */
        //        LATBbits.LATB5 = 0; /* Drive the output on */
        //        _delay(100000);
        //        LATBbits.LATB4 = 0; /* Drive the output on */
        //        LATBbits.LATB5 = 1; /* Drive the output off */
        //        _delay(100000);
        //EUSART1_Write('w');
    }
}
/**
 End of File
 */