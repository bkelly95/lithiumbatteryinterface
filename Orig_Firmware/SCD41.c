/*********************************************************************
 * FileName: 		SCD41.c
 * Dependencies: 	SCD41.h (include here and in main)
 *					common.h (include here and in main)
 *					GPIO_pin_assignments.h (include here and in main)
 *					timers.h (include here and in main)
 ********************************************************************/

/*********************************************************************
 *		Simple routines for SCD41 serial port. Data pin is bidirectional
 *		and requires a pullup - therefore in the "GPIO_pin_assignments.h"
 *		header file the corresponding open-drain register is enabled 
 *		for PIC-based transmits. (4.7K resistor seemed to improve slew-rate vs.
 *		10K). Possible to insert assembly NOP to widen "clock" pulses.
 *
 *		Steps:
 *	
 *			- bus is initialized
 *			- command is sent to measure humidity
 *			- a blocking delay of 80ms is observed 
 *			- data bits are shifted out
 *		
 *		Seperate functions are included for debugging various steps (initialize, 
 *		send command, get data) as well as a function that performs all steps.
 *
 *		Safe to use these routines without timeout protection, as no blocking
 *		conditions exist (not receive an "ACK" when appropriate will return 0).
 *		
 ********************************************************************/

#include "common.h"
#include "SCD41.h"
#include "GPIO_pin_assignments.h"
