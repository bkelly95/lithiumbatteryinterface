/*********************************************************************
 * FileName: 		uart_fifo.c
 * Dependencies: 	uart_fifo.h (include here and in main)
 *					common.h (include here and in main)
 *
 ********************************************************************/




/*********************************************************************
 * Function:        	(void) _UxRXInterrupt (void)
 * PreCondition:    	Function for assigning UART PPS pins should have
 *						been called.
 * Input:           	None.
 * Output:          	None.
 * Side Effects:    	None.
 * Overview:       		Interrupt-driven UART receive buffer. An interrupt is
						generated upon reception of a byte; corresponding ISR
						transfers byte from receive buffer to FIFO. Upon detecting
						0X0A the write pointer and data count is incremented.
 *
 ********************************************************************/


#include "common.h"
#include "uart_fifo.h"


//******************** UART1 Receive FIFO ****************************

int UART_error_latch = 0;
char UART1_rx_fifo [UART1_FIFO_depth][UART1_FIFO_size];

int UART1_rx_fifo_wr = 0;
int UART1_rx_fifo_rd = 0;
int UART1_local_rx_index = 0;
int UART1_dat = 0;

//********************************************************************


//********************************************************************




/*********************************************************************
 * Function:        	(void) update_UARTx_fifo (void)
 * PreCondition:    	UART should be configured properly.
 * Input:           	None.
 * Output:          	None.
 * Side Effects:    	None.
 * Overview:       		Provides convienent way of updating FIFO variables
						(assumes received data of interest is only 1 string
						long)
 ********************************************************************/

void update_UART1_fifo (void){

UART1_dat--;
UART1_rx_fifo_rd=(UART1_rx_fifo_rd+1)%UART1_FIFO_depth;

}


void clear_UART1_fifo (void){

UART1_rx_fifo_wr = 0;
UART1_rx_fifo_rd = 0;
UART1_local_rx_index = 0;
UART1_dat = 0;
//U1STAbits.OERR=0;

}
