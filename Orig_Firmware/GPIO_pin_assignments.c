/*********************************************************************
 * FileName: 		GPIO_pin_assignments.c
 * Dependencies: 	GPIO_pin_assignments.h (include here and in main)
 *					common.h (include here and in main)
 *
 ********************************************************************/


/*********************************************************************
 * Function:        	void set_GPIO_pins (void)
 * PreCondition:    	None.
 * Input:           	None.
 * Output:          	None.
 * Side Effects:    	None.
 * Overview:       		Sets up digital GPIO pins. TRISx register sets
 *						direction; LATx register used for port writes 
 *						and PORTx register used for port reads. External
 *						interrupts also assigned here.
 *
 ********************************************************************/

#include "GPIO_pin_assignments.h"
#include "common.h"


void set_GPIO_pins (void) {


// RA2 J20 Spare GPIO 3
#define GPIO3.RA2 = 0;
    

// RA1 J18 Spare GPIO 2
#define GPIO2.RA1 = 0; 
    

// RA0 J17 Spare GPIO 1
#define GPIO1.RA0 = 0;
    

// RB4 GREEN LED
#define GreenLed.RB4 = 0; 


// RB5 RED LED
    
#define RedLed.RB5 = 0;
    

}
