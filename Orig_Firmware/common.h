/*********************************************************************
 * FileName: 		common.h
 * Dependencies: 	None
 * Overview:		Sets up global variables to be used in "main.c". 
 *
 *
 ********************************************************************/


#include <p18cxxx.h> // PIC-specific header file
#include <xc.h> // Compiler
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"
