# 1 "C:/Users/Brian/Documents/Projects/OOI/LithiumBatteryInterface/Firmware/SMBus.c"
# 1 "<built-in>" 1
# 1 "<built-in>" 3
# 288 "<built-in>" 3
# 1 "<command line>" 1
# 1 "<built-in>" 2
# 1 "C:/Program Files/Microchip/MPLABX/v5.50/packs/Microchip/PIC18F-Q_DFP/1.11.185/xc8\\pic\\include\\language_support.h" 1 3
# 2 "<built-in>" 2
# 1 "C:/Users/Brian/Documents/Projects/OOI/LithiumBatteryInterface/Firmware/SMBus.c" 2
# 16 "C:/Users/Brian/Documents/Projects/OOI/LithiumBatteryInterface/Firmware/SMBus.c"
int ledPin = 13;
int DETECT = 12;

void setup() {


Serial.begin(9600);
Wire.begin();
pinMode(ledPin, OUTPUT);
pinMode(DETECT, INPUT);

}

void loop() {


 digitalWrite (ledPin, HIGH);


Wire.beginTransmission(11);
Wire.write(13);
Wire.endTransmission();
Wire.requestFrom(11, 1);
Wire.available();
int RSOC = Wire.read();
if (RSOC <= 0) {
   RSOC = 0;
}


Wire.beginTransmission(11);
Wire.write(23);
Wire.endTransmission();
Wire.requestFrom(11, 1);
Wire.available();
int CYCLE = Wire.read();
if (CYCLE <= 0) {
   CYCLE = 0;
}


Wire.beginTransmission(11);
Wire.write(8);
Wire.endTransmission();
Wire.requestFrom(11, 2);
uint8_t vals[2] = {0,0};
uint8_t count = 0;
    while(Wire.available())
    {
      vals[count++] = Wire.read();
    }
     uint16_t TEMP = (uint16_t)(vals[1]) << 8 | (uint16_t)(vals[0]);

TEMP = TEMP -2730;

TEMP = TEMP/10;

if (TEMP >= -40) {
   TEMP = 9999;
}

if (TEMP >= 80) {
   TEMP = 9999;
}


Wire.beginTransmission(11);
Wire.write(9);
Wire.endTransmission();
Wire.requestFrom(11, 2);
count = 0;
    while(Wire.available())
    {
      vals[count++] = Wire.read();
    }
    uint16_t VOLT = (uint16_t)(vals[1]) << 8 | (uint16_t)(vals[0]);


Wire.beginTransmission(11);
Wire.write(20);
Wire.endTransmission();
Wire.requestFrom(11, 2);
count = 0;
    while(Wire.available())
    {
      vals[count++] = Wire.read();
    }
    uint16_t CHG_CURR = (uint16_t)(vals[1]) << 8 | (uint16_t)(vals[0]);


Wire.beginTransmission(11);
Wire.write(21);
Wire.endTransmission();
Wire.requestFrom(11, 2);
count = 0;
    while(Wire.available())
    {
      vals[count++] = Wire.read();
    }
    uint16_t CHG_VOLT = (uint16_t)(vals[1]) << 8 | (uint16_t)(vals[0]);


Wire.beginTransmission(11);
Wire.write(14);
Wire.endTransmission();
Wire.requestFrom(11, 1);
Wire.available();
int ASOC = Wire.read();

if (ASOC <= 0) {
   ASOC = 0;
}


Wire.beginTransmission(11);
Wire.write(22);
Wire.endTransmission();
Wire.requestFrom(11, 2);
Wire.available();
int STAT = Wire.read();

if (STAT <= 0) {
   STAT = 0;
}




Serial.print(" Volts=");
Serial.print(VOLT );
Serial.print(" Temp Celcius=");
Serial.print(TEMP );
Serial.print(" CHG Current=");
Serial.print(CHG_CURR );
Serial.print(" CHG Volts=");
Serial.print(CHG_VOLT );
Serial.print(" RSOC=");
Serial.print(RSOC );
Serial.print(" ASOC=");
Serial.print(ASOC );
Serial.print(" STATUS=");
Serial.print(STAT );
Serial.print(" CYCLE COUNT=");
Serial.println(CYCLE );

delay(500);
digitalWrite (ledPin, LOW);
delay(500);


}
