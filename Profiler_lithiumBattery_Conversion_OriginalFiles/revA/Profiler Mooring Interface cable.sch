EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr B 17000 11000
encoding utf-8
Sheet 1 1
Title "PCB, Profiler Lithium Battery"
Date "2021/07/08"
Rev "A"
Comp "Woods Hole Oceanographic Inst"
Comment1 "AOPE - OOI"
Comment2 "LOSOS Profiler Lab"
Comment3 "266 Woods Hole Road"
Comment4 "Woods Hole, MA 02543"
$EndDescr
$Comp
L Connector_Generic:Conn_01x05 P3
U 1 1 615153BB
P 5950 7600
F 0 "P3" H 5950 7175 50  0000 C CNN
F 1 "Charge_Ctrl" H 5950 7275 50  0000 C CNN
F 2 "" H 5950 7600 50  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/436450500_sd.pdf" H 5950 7600 50  0001 C CNN
F 4 "Molex" H 5950 7600 50  0001 C CNN "MFR"
F 5 "0436450500" H 5950 7600 50  0001 C CNN "MPN"
	1    5950 7600
	-1   0    0    1   
$EndComp
Text Notes 5875 7800 2    60   ~ 0
GND\nSTC TX\nSTC RX\n5V to PCB\n5V from PCB
Text Label 6175 7800 0    60   ~ 0
ISO_5V_OUT
Text Label 6175 7700 0    60   ~ 0
5V
Text Label 6175 7500 0    60   ~ 0
BATT_TXD
Text Label 6175 7600 0    60   ~ 0
BATT_RXD
Text Label 6175 7400 0    60   ~ 0
GND
Text Notes 5700 8350 0    60   ~ 0
When charging:\nDisconnect Profiler endcap from J1A\nConnect J1B and join (J1B-1 and J1B-2)
$Comp
L Connector_Generic:Conn_01x02 J15
U 1 1 61EEA346
P 6025 6175
F 0 "J15" H 5943 6308 60  0000 C CNN
F 1 "460150209" H 5943 6308 60  0001 C CNN
F 2 "Footprint:460150209" H 6425 6115 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/460150209_sd.pdf" H 6025 6175 60  0001 C CNN
F 4 "Molex" H 6025 9200 50  0001 C CNN "MFR"
F 5 "460150209" H 6025 9200 50  0001 C CNN "MPN"
	1    6025 6175
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 P5
U 1 1 61EE9CE6
P 6025 2275
F 0 "P5" H 5943 2408 60  0000 C CNN
F 1 "Charge Cable (PCB End)" H 5943 2058 60  0001 C CNN
F 2 "" H 6425 1865 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/039012025_sd.pdf" H 6025 1925 60  0001 C CNN
F 4 "Molex" H 6025 1925 50  0001 C CNN "MFR"
F 5 "0039012025" H 6025 1925 50  0001 C CNN "MPN"
	1    6025 2275
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 P7
U 1 1 61EE9681
P 6025 3025
F 0 "P7" H 5943 3158 60  0000 C CNN
F 1 "Charge Cable (PCB End)" H 5943 2058 60  0001 C CNN
F 2 "" H 6425 1865 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/039012025_sd.pdf" H 6025 1925 60  0001 C CNN
F 4 "Molex" H 6025 1925 50  0001 C CNN "MFR"
F 5 "0039012025" H 6025 1925 50  0001 C CNN "MPN"
	1    6025 3025
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 P9
U 1 1 61EE9206
P 6025 3775
F 0 "P9" H 5943 3908 60  0000 C CNN
F 1 "Charge Cable (PCB End)" H 5943 2058 60  0001 C CNN
F 2 "" H 6425 1865 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/039012025_sd.pdf" H 6025 1925 60  0001 C CNN
F 4 "Molex" H 6025 1925 50  0001 C CNN "MFR"
F 5 "0039012025" H 6025 1925 50  0001 C CNN "MPN"
	1    6025 3775
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 P14
U 1 1 61EE83B1
P 6025 5775
F 0 "P14" H 5943 5908 60  0000 C CNN
F 1 "Charge Cable (PCB End)" H 5943 2058 60  0001 C CNN
F 2 "" H 6425 1865 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/039012025_sd.pdf" H 6025 1925 60  0001 C CNN
F 4 "Molex" H 6025 1925 50  0001 C CNN "MFR"
F 5 "0039012025" H 6025 1925 50  0001 C CNN "MPN"
	1    6025 5775
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 P13
U 1 1 61EE7C25
P 6025 5375
F 0 "P13" H 5943 5508 60  0000 C CNN
F 1 "460150209" H 5943 5508 60  0001 C CNN
F 2 "Footprint:460150209" H 6425 5315 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/460150209_sd.pdf" H 6025 5375 60  0001 C CNN
F 4 "Molex" H 6025 7200 50  0001 C CNN "MFR"
F 5 "460150209" H 6025 7200 50  0001 C CNN "MPN"
	1    6025 5375
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 P11
U 1 1 61EE768D
P 6025 4525
F 0 "P11" H 5943 4658 60  0000 C CNN
F 1 "Charge Cable (PCB End)" H 5943 2058 60  0001 C CNN
F 2 "" H 6425 1865 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/039012025_sd.pdf" H 6025 1925 60  0001 C CNN
F 4 "Molex" H 6025 1925 50  0001 C CNN "MFR"
F 5 "0039012025" H 6025 1925 50  0001 C CNN "MPN"
	1    6025 4525
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 P10
U 1 1 61EE6A60
P 6025 4150
F 0 "P10" H 5943 4283 60  0000 C CNN
F 1 "Charge Cable (PCB End)" H 5943 2058 60  0001 C CNN
F 2 "" H 6425 1865 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/039012025_sd.pdf" H 6025 1925 60  0001 C CNN
F 4 "Molex" H 6025 1925 50  0001 C CNN "MFR"
F 5 "0039012025" H 6025 1925 50  0001 C CNN "MPN"
	1    6025 4150
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 P8
U 1 1 61EE647D
P 6025 3400
F 0 "P8" H 5943 3533 60  0000 C CNN
F 1 "Charge Cable (PCB End)" H 5943 2058 60  0001 C CNN
F 2 "" H 6425 1865 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/039012025_sd.pdf" H 6025 1925 60  0001 C CNN
F 4 "Molex" H 6025 1925 50  0001 C CNN "MFR"
F 5 "0039012025" H 6025 1925 50  0001 C CNN "MPN"
	1    6025 3400
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 P6
U 1 1 61EE5E18
P 6025 2650
F 0 "P6" H 5943 2783 60  0000 C CNN
F 1 "Charge Cable (PCB End)" H 5943 2058 60  0001 C CNN
F 2 "" H 6425 1865 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/039012025_sd.pdf" H 6025 1925 60  0001 C CNN
F 4 "Molex" H 6025 1925 50  0001 C CNN "MFR"
F 5 "0039012025" H 6025 1925 50  0001 C CNN "MPN"
	1    6025 2650
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 P4
U 1 1 614DBEC7
P 6025 1925
F 0 "P4" H 5943 2058 60  0000 C CNN
F 1 "Charge Cable (PCB End)" H 5943 2058 60  0001 C CNN
F 2 "" H 6425 1865 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/039012025_sd.pdf" H 6025 1925 60  0001 C CNN
F 4 "Molex" H 6025 1925 50  0001 C CNN "MFR"
F 5 "0039012025" H 6025 1925 50  0001 C CNN "MPN"
	1    6025 1925
	-1   0    0    -1  
$EndComp
Text Label 6225 4525 0    60   ~ 0
#4_CHG+
Text Label 6225 4250 0    60   ~ 0
#5_CHG-
Text Label 6225 4150 0    60   ~ 0
#5_CHG+
Text Label 6250 3500 0    60   ~ 0
#6_CHG-
Text Label 6250 3400 0    60   ~ 0
#6_CHG+
Text Label 6225 5050 0    60   ~ 0
#12_CHG-
Text Label 6225 4950 0    60   ~ 0
#12_CHG+
Text Label 6225 3875 0    60   ~ 0
#11_CHG-
Text Label 6225 3775 0    60   ~ 0
#11_CHG+
Text Label 6250 3125 0    60   ~ 0
#10_CHG-
Text Label 6250 3025 0    60   ~ 0
#10_CHG+
Text Label 6300 2375 0    60   ~ 0
#9_CHG-
Text Label 6300 2275 0    60   ~ 0
#9_CHG+
Text Label 6225 2025 0    60   ~ 0
#7_CHG-
Text Label 6225 1925 0    60   ~ 0
#7_CHG+
Text Label 6250 2650 0    60   ~ 0
#8_CHG+
Text Label 6250 2750 0    60   ~ 0
#8_CHG-
Text Label 6275 6275 0    60   ~ 0
#1_CHG-
Text Label 6275 6175 0    60   ~ 0
#1_CHG+
Text Label 6250 5875 0    60   ~ 0
#2_CHG-
Text Label 6250 5775 0    60   ~ 0
#2_CHG+
Text Label 6225 5475 0    60   ~ 0
#3_CHG-
Text Label 6225 5375 0    60   ~ 0
#3_CHG+
Text Label 6225 4625 0    60   ~ 0
#4_CHG-
Wire Wire Line
	6625 4525 6225 4525
$Comp
L Connector_Generic:Conn_01x02 P12
U 1 1 6126B9D2
P 6025 4950
F 0 "P12" H 5943 5083 60  0000 C CNN
F 1 "Charge Cable (PCB End)" H 5943 2058 60  0001 C CNN
F 2 "" H 6425 1865 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/039012025_sd.pdf" H 6025 1925 60  0001 C CNN
F 4 "Molex" H 6025 1925 50  0001 C CNN "MFR"
F 5 "0039012025" H 6025 1925 50  0001 C CNN "MPN"
	1    6025 4950
	-1   0    0    -1  
$EndComp
$Comp
L Switch:SW_SPST SW1
U 1 1 6128F4FE
P 10450 6300
F 0 "SW1" H 11075 6425 50  0000 C CNN
F 1 "LBI Power Switch" H 11225 6300 50  0000 C CNN
F 2 "" H 10450 6300 50  0001 C CNN
F 3 "https://www.nkkswitches.com/pdf/MtogglesBushing.pdf" H 10450 6300 50  0001 C CNN
F 4 "NKK Switched" H 10450 6300 50  0001 C CNN "MFR"
F 5 "M2011SD3W01-CA" H 10450 6300 50  0001 C CNN "MPN"
	1    10450 6300
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Connectors:DB9FEMALE J3
U 1 1 61290012
P 10425 5775
F 0 "J3" H 10900 5625 45  0000 C CNN
F 1 "LBI Serial communication" H 11100 5725 45  0000 C CNN
F 2 "DE09S064TLF" H 10425 6225 20  0001 C CNN
F 3 "https://www.amphenol-icc.com/media/wysiwyg/files/drawing/c-dsub-0064.pdf" H 10425 5775 50  0001 C CNN
F 4 "Ampheonol ICC (FCI)" H 10425 6178 60  0001 C CNN "MFN"
F 5 "" H 10425 5775 50  0001 C CNN "Field5"
	1    10425 5775
	1    0    0    1   
$EndComp
Wire Wire Line
	9075 5500 7925 5500
Wire Wire Line
	7925 5500 7925 7400
Wire Wire Line
	6150 7400 7925 7400
Wire Wire Line
	6150 7500 8000 7500
Wire Wire Line
	6150 7600 8075 7600
Text Notes 5725 8550 0    50   ~ 0
5 connector pins - CONN SOCKET 20-24AWG CRIMP GOLD
Text Notes 5375 3925 0    50   ~ 0
24 Pins\n0039000089
$Comp
L Connector_Generic:Conn_01x32 J1
U 1 1 612A5653
P 9275 4600
F 0 "J1" H 9100 6325 50  0000 L CNN
F 1 "Charge Connect (mooring end)" V 9250 6200 50  0000 L CNN
F 2 "" H 9275 4600 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=T2050322101000&DocType=Customer+Drawing&DocLang=English" H 9275 4600 50  0001 C CNN
F 4 "TE Connectivity" H 9275 4600 50  0001 C CNN "MFR"
F 5 "HEE-032-M" H 9275 4600 50  0001 C CNN "MPN"
	1    9275 4600
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x32 J2
U 1 1 612A9E58
P 9475 4600
F 0 "J2" H 9375 6325 50  0000 C CNN
F 1 "Charge Connector (User end)" V 9475 6750 50  0000 C CNN
F 2 "" H 9475 4600 50  0001 C CNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=srchrtrv&DocNm=T2050322201000&DocType=Customer+Drawing&DocLang=English" H 9475 4600 50  0001 C CNN
F 4 "TE Connectivity" H 9475 4600 50  0001 C CNN "MFR"
F 5 "Hee-032-F" H 9475 4600 50  0001 C CNN "MPN"
	1    9475 4600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9075 3100 8575 3100
Wire Wire Line
	8575 3100 8575 1925
Wire Wire Line
	6225 1925 8575 1925
Wire Wire Line
	9075 3200 8500 3200
Wire Wire Line
	8500 3200 8500 2025
Wire Wire Line
	6225 2025 8500 2025
Wire Wire Line
	9075 3300 8375 3300
Wire Wire Line
	8375 3300 8375 2275
Wire Wire Line
	6225 2275 8375 2275
Wire Wire Line
	9075 3400 8250 3400
Wire Wire Line
	8250 3400 8250 2375
Wire Wire Line
	6225 2375 8250 2375
Wire Wire Line
	8075 3500 8075 2650
Wire Wire Line
	6225 2650 8075 2650
Wire Wire Line
	8075 3500 9075 3500
Wire Wire Line
	9075 3600 7975 3600
Wire Wire Line
	7975 3600 7975 2750
Wire Wire Line
	6225 2750 7975 2750
Wire Wire Line
	9075 3700 7775 3700
Wire Wire Line
	7775 3700 7775 3025
Wire Wire Line
	6225 3025 7775 3025
Wire Wire Line
	9075 3800 7675 3800
Wire Wire Line
	7675 3800 7675 3125
Wire Wire Line
	6225 3125 7675 3125
Wire Wire Line
	9075 3900 7500 3900
Wire Wire Line
	7500 3900 7500 3400
Wire Wire Line
	6225 3400 7500 3400
Wire Wire Line
	9075 4000 7375 4000
Wire Wire Line
	7375 4000 7375 3500
Wire Wire Line
	6225 3500 7375 3500
Wire Wire Line
	9075 4100 7175 4100
Wire Wire Line
	7175 4100 7175 3775
Wire Wire Line
	6225 3775 7175 3775
Wire Wire Line
	9075 4200 7075 4200
Wire Wire Line
	7075 4200 7075 3875
Wire Wire Line
	6225 3875 7075 3875
Wire Wire Line
	9075 4300 6900 4300
Wire Wire Line
	6900 4300 6900 4150
Wire Wire Line
	6225 4150 6900 4150
Wire Wire Line
	9075 4400 6825 4400
Wire Wire Line
	6825 4400 6825 4250
Wire Wire Line
	6225 4250 6825 4250
Wire Wire Line
	9075 4500 6625 4500
Wire Wire Line
	6625 4500 6625 4525
Wire Wire Line
	9075 4600 6750 4600
Wire Wire Line
	6750 4600 6750 4625
Wire Wire Line
	6225 4625 6750 4625
Wire Wire Line
	9075 4700 6800 4700
Wire Wire Line
	6800 4700 6800 4950
Wire Wire Line
	6225 4950 6800 4950
Wire Wire Line
	9075 4800 6875 4800
Wire Wire Line
	6875 4800 6875 5050
Wire Wire Line
	6225 5050 6875 5050
Wire Wire Line
	9075 4900 7075 4900
Wire Wire Line
	7075 4900 7075 5375
Wire Wire Line
	6225 5375 7075 5375
Wire Wire Line
	9075 5000 7175 5000
Wire Wire Line
	7175 5000 7175 5475
Wire Wire Line
	6225 5475 7175 5475
Wire Wire Line
	9075 5100 7375 5100
Wire Wire Line
	7375 5100 7375 5775
Wire Wire Line
	6225 5775 7375 5775
Wire Wire Line
	9075 5200 7475 5200
Wire Wire Line
	7475 5200 7475 5875
Wire Wire Line
	6225 5875 7475 5875
Wire Wire Line
	9075 5300 7675 5300
Wire Wire Line
	7675 5300 7675 6175
Wire Wire Line
	6225 6175 7675 6175
Wire Wire Line
	9075 5400 7775 5400
Wire Wire Line
	7775 5400 7775 6275
Wire Wire Line
	6225 6275 7775 6275
Wire Wire Line
	9075 5600 8000 5600
Wire Wire Line
	8000 5600 8000 7500
Wire Wire Line
	9075 5700 8075 5700
Wire Wire Line
	8075 5700 8075 7600
Wire Wire Line
	9075 5800 8250 5800
Wire Wire Line
	8250 5800 8250 7700
Wire Wire Line
	6150 7700 8250 7700
Wire Wire Line
	9075 5900 8350 5900
Wire Wire Line
	8350 5900 8350 7800
Wire Wire Line
	6150 7800 8350 7800
Wire Wire Line
	9950 5600 9675 5600
Wire Wire Line
	9675 5800 9875 5800
Wire Wire Line
	9875 5800 9875 6125
Wire Wire Line
	9875 6125 10800 6125
Wire Wire Line
	10800 6125 10800 6300
Wire Wire Line
	10800 6300 10650 6300
Wire Wire Line
	9675 5900 9800 5900
Wire Wire Line
	9800 5900 9800 6300
Wire Wire Line
	9800 6300 10250 6300
$Comp
L Connector:Conn_01x02_Male J16
U 1 1 6119A92C
P 10850 5300
F 0 "J16" H 10822 5274 50  0000 R CNN
F 1 "Battery #1" H 10822 5183 50  0000 R CNN
F 2 "" H 10850 5300 50  0001 C CNN
F 3 "https://www.keyelco.com/userAssets/file/M65p112.pdf" H 10850 5300 50  0001 C CNN
F 4 "Keystone" H 10850 5300 50  0001 C CNN "MFR"
F 5 "4109" H 10850 5300 50  0001 C CNN "MPN"
	1    10850 5300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10175 5875 9950 5875
Wire Wire Line
	9675 5700 10125 5700
Wire Wire Line
	10125 5700 10125 5775
Wire Wire Line
	10125 5775 10175 5775
Wire Wire Line
	10175 5575 10175 5500
Wire Wire Line
	10175 5500 9675 5500
Wire Wire Line
	9950 5600 9950 5875
Wire Wire Line
	9675 5400 10650 5400
Wire Wire Line
	9675 5300 10650 5300
$Comp
L Connector:Conn_01x02_Male J14
U 1 1 611D543F
P 10850 5100
F 0 "J14" H 10822 5074 50  0000 R CNN
F 1 "Battery #2" H 10822 4983 50  0000 R CNN
F 2 "" H 10850 5100 50  0001 C CNN
F 3 "https://www.keyelco.com/userAssets/file/M65p112.pdf" H 10850 5100 50  0001 C CNN
F 4 "Keystone" H 10850 5100 50  0001 C CNN "MFR"
F 5 "4109" H 10850 5100 50  0001 C CNN "MPN"
	1    10850 5100
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J13
U 1 1 611D597F
P 10850 4900
F 0 "J13" H 10822 4874 50  0000 R CNN
F 1 "Battery #3" H 10822 4783 50  0000 R CNN
F 2 "" H 10850 4900 50  0001 C CNN
F 3 "https://www.keyelco.com/userAssets/file/M65p112.pdf" H 10850 4900 50  0001 C CNN
F 4 "Keystone" H 10850 4900 50  0001 C CNN "MFR"
F 5 "4109" H 10850 4900 50  0001 C CNN "MPN"
	1    10850 4900
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10650 5200 9675 5200
Wire Wire Line
	10650 5100 9675 5100
Wire Wire Line
	10650 5000 9675 5000
Wire Wire Line
	10650 4900 9675 4900
$Comp
L Connector:Conn_01x02_Male J12
U 1 1 611E97D5
P 10850 4700
F 0 "J12" H 10822 4674 50  0000 R CNN
F 1 "Battery #12" H 10822 4583 50  0000 R CNN
F 2 "" H 10850 4700 50  0001 C CNN
F 3 "https://www.keyelco.com/userAssets/file/M65p112.pdf" H 10850 4700 50  0001 C CNN
F 4 "Keystone" H 10850 4700 50  0001 C CNN "MFR"
F 5 "4109" H 10850 4700 50  0001 C CNN "MPN"
	1    10850 4700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9675 4800 10650 4800
Wire Wire Line
	9675 4700 10650 4700
$Comp
L Connector:Conn_01x02_Male J11
U 1 1 611E97DF
P 10850 4500
F 0 "J11" H 10822 4474 50  0000 R CNN
F 1 "Battery #4" H 10822 4383 50  0000 R CNN
F 2 "" H 10850 4500 50  0001 C CNN
F 3 "https://www.keyelco.com/userAssets/file/M65p112.pdf" H 10850 4500 50  0001 C CNN
F 4 "Keystone" H 10850 4500 50  0001 C CNN "MFR"
F 5 "4109" H 10850 4500 50  0001 C CNN "MPN"
	1    10850 4500
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J10
U 1 1 611E97E7
P 10850 4300
F 0 "J10" H 10822 4274 50  0000 R CNN
F 1 "Battery #5" H 10822 4183 50  0000 R CNN
F 2 "" H 10850 4300 50  0001 C CNN
F 3 "https://www.keyelco.com/userAssets/file/M65p112.pdf" H 10850 4300 50  0001 C CNN
F 4 "Keystone" H 10850 4300 50  0001 C CNN "MFR"
F 5 "4109" H 10850 4300 50  0001 C CNN "MPN"
	1    10850 4300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10650 4600 9675 4600
Wire Wire Line
	10650 4500 9675 4500
Wire Wire Line
	10650 4400 9675 4400
Wire Wire Line
	10650 4300 9675 4300
$Comp
L Connector:Conn_01x02_Male J9
U 1 1 611EEA7C
P 10850 4100
F 0 "J9" H 10822 4074 50  0000 R CNN
F 1 "Battery #11" H 10822 3983 50  0000 R CNN
F 2 "" H 10850 4100 50  0001 C CNN
F 3 "https://www.keyelco.com/userAssets/file/M65p112.pdf" H 10850 4100 50  0001 C CNN
F 4 "Keystone" H 10850 4100 50  0001 C CNN "MFR"
F 5 "4109" H 10850 4100 50  0001 C CNN "MPN"
	1    10850 4100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9675 4200 10650 4200
Wire Wire Line
	9675 4100 10650 4100
$Comp
L Connector:Conn_01x02_Male J8
U 1 1 611EEA86
P 10850 3900
F 0 "J8" H 10822 3874 50  0000 R CNN
F 1 "Battery #6" H 10822 3783 50  0000 R CNN
F 2 "" H 10850 3900 50  0001 C CNN
F 3 "https://www.keyelco.com/userAssets/file/M65p112.pdf" H 10850 3900 50  0001 C CNN
F 4 "Keystone" H 10850 3900 50  0001 C CNN "MFR"
F 5 "4109" H 10850 3900 50  0001 C CNN "MPN"
	1    10850 3900
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J7
U 1 1 611EEA8E
P 10850 3700
F 0 "J7" H 10822 3674 50  0000 R CNN
F 1 "Battery #10" H 10822 3583 50  0000 R CNN
F 2 "" H 10850 3700 50  0001 C CNN
F 3 "https://www.keyelco.com/userAssets/file/M65p112.pdf" H 10850 3700 50  0001 C CNN
F 4 "Keystone" H 10850 3700 50  0001 C CNN "MFR"
F 5 "4109" H 10850 3700 50  0001 C CNN "MPN"
	1    10850 3700
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10650 4000 9675 4000
Wire Wire Line
	10650 3900 9675 3900
Wire Wire Line
	10650 3800 9675 3800
Wire Wire Line
	10650 3700 9675 3700
$Comp
L Connector:Conn_01x02_Male J6
U 1 1 611F47A5
P 10850 3500
F 0 "J6" H 10822 3474 50  0000 R CNN
F 1 "Battery #8" H 10822 3383 50  0000 R CNN
F 2 "" H 10850 3500 50  0001 C CNN
F 3 "https://www.keyelco.com/userAssets/file/M65p112.pdf" H 10850 3500 50  0001 C CNN
F 4 "Keystone" H 10850 3500 50  0001 C CNN "MFR"
F 5 "4109" H 10850 3500 50  0001 C CNN "MPN"
	1    10850 3500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9675 3600 10650 3600
Wire Wire Line
	9675 3500 10650 3500
$Comp
L Connector:Conn_01x02_Male J5
U 1 1 611F47AF
P 10850 3300
F 0 "J5" H 10822 3274 50  0000 R CNN
F 1 "Battery #9" H 10822 3183 50  0000 R CNN
F 2 "" H 10850 3300 50  0001 C CNN
F 3 "https://www.keyelco.com/userAssets/file/M65p112.pdf" H 10850 3300 50  0001 C CNN
F 4 "Keystone" H 10850 3300 50  0001 C CNN "MFR"
F 5 "4109" H 10850 3300 50  0001 C CNN "MPN"
	1    10850 3300
	-1   0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J4
U 1 1 611F47B7
P 10850 3100
F 0 "J4" H 10822 3074 50  0000 R CNN
F 1 "Battery #7" H 10822 2983 50  0000 R CNN
F 2 "" H 10850 3100 50  0001 C CNN
F 3 "https://www.keyelco.com/userAssets/file/M65p112.pdf" H 10850 3100 50  0001 C CNN
F 4 "Keystone" H 10850 3100 50  0001 C CNN "MFR"
F 5 "4109" H 10850 3100 50  0001 C CNN "MPN"
	1    10850 3100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10650 3400 9675 3400
Wire Wire Line
	10650 3300 9675 3300
Wire Wire Line
	10650 3200 9675 3200
Wire Wire Line
	10650 3100 9675 3100
$EndSCHEMATC
