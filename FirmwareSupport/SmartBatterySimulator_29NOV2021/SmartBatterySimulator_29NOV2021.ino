
#include <Wire.h>


// ManufactureDate data type is a bitfield
//
struct Date_t
{
  unsigned day:5;
  unsigned month:4;
  unsigned year:7;
};


// Standard Smart Battery command
//
      unsigned int ManufactureAccess      = 0x0100;                       // cmd 00   r/w   - bitfield
      unsigned int RemainingCapacityAlarm = 0;                            // cmd 01   r/w   - mAh
      unsigned int RemainingTimeAlarm     = 0;                            // cmd 02   r/w   - min
      unsigned int BatteryMode            = 0x0080;                       // cmd 03   r/w   - bitfield
        signed int AtRate                 = 0;                            // cmd 04   r/w   - mA
      unsigned int AtRateTimeToFull       = 0xFFFF;                       // cmd 05   r     - min
      unsigned int AtRateTimeToEmpty      = 0xFFFF;                       // cmd 06   r     - min
      unsigned int AtRateOK               = 0x0001;                       // cmd 07   r     - boolean
const unsigned int Temperature            = (25+273.5)*10;                // cmd 08   r     - 10*°K
      unsigned int Voltage                = 0x1388;                       // cmd 09   r     - mV 5000mA
        signed int Current                = 0x04D2;                       // cmd 0A   r     - mA 1234mA
        signed int AverageCurrent         = 0;                            // cmd 0B   r     - 1 minute average current
const unsigned int MaxError               = 10;                           // cmd 0C   r     - %
      unsigned int RelativeStateOfCharge  = 0;                            // cmd 0D   r     - %
      unsigned int AbsoluteStateOfCharge  = 0;                            // cmd 0E   r     - %
      unsigned int RemainingCapacity      = 0;                            // cmd 0F   r     - mAh
      unsigned int FullChargeCapacity     = 0;                            // cmd 10   r     - mAh
      unsigned int RunTimeToEmpty         = 0;                            // cmd 11   r     - min
      unsigned int AverageTimeToEmpty     = 0;                            // cmd 12   r     - min
      unsigned int AverageTimeToFull      = 0;                            // cmd 13   r     - min
const unsigned int ChargingCurrent        = 2000;                         // cmd 14   r     - mA
const unsigned int ChargingVoltage        = 12600;                        // cmd 15   r     - nCell * 4200mV
      unsigned int BatteryStatus          = 0;                            // cmd 16   r     - bitfielf
      unsigned int CycleCount             = 10;                           // cmd 17   r     - #
const unsigned int DesignCapacity         = 4400;                         // cmd 18   r     - mAh
const unsigned int DesignVoltage          = 11100;                        // cmd 19   r     - nCell * 3700mV
const unsigned int SpecificationInfo      = 0x0021;                       // cmd 1A   r     - bitfield
const Date_t       ManufactureDate        = { 29,3,2019-1980 };           // cmd 1B   r     - bitfield
const unsigned int SerialNumber           = 12345;                        // cmd 1C   r     - #
const char         ManufacturerName[]     = { "\x07""SAM-HSW" };          // cmd 20   r     - first byte is the text length
const char         DeviceName[]           = { "\x0D""DELL RM6618AR" };    // cmd 21   r     - first byte is the text length
const char         DeviceChemistry[]      = { "\x04""LION" };             // cmd 22   r     - first byte is the text length
const char         ManufacturerData[]     = { "\x0C""123456789012" };     // cmd 23   r     - first byte is the data length

void  Setup ( void ){
}

byte  command=0;

// Answers to the read command
//
void sendAnswer ( byte command )
{
  static unsigned int zero=0;
  switch( command )
  {
    case 0x00: Wire.write((byte*)&ManufactureAccess, 2); break;
    case 0x01: Wire.write((byte*)&RemainingCapacityAlarm, 2); break;
    case 0x02: Wire.write((byte*)&RemainingTimeAlarm, 2);break;
    case 0x03: Wire.write((byte*)&BatteryMode, 2);break;
    case 0x04: Wire.write((byte*)&AtRate, 2);break;
    case 0x05: Wire.write((byte*)&AtRateTimeToFull, 2);break;
    case 0x06: Wire.write((byte*)&AtRateTimeToEmpty, 2);break;
    case 0x07: Wire.write((byte*)&AtRateOK, 2);break;
    case 0x08: Wire.write((byte*)&Temperature, 2);break;
    case 0x09: Wire.write((byte*)&Voltage, 2);break;
    case 0x0a: Wire.write((byte*)&Current, 2);break;
    case 0x0b: Wire.write((byte*)&AverageCurrent, 2);break;
    case 0x0c: Wire.write((byte*)&MaxError, 2);break;
    case 0x0d: Wire.write((byte*)&RelativeStateOfCharge, 2);break;
    case 0x0e: Wire.write((byte*)&AbsoluteStateOfCharge, 2);break;
    case 0x0f: Wire.write((byte*)&RemainingCapacity, 2);break;
    case 0x10: Wire.write((byte*)&FullChargeCapacity, 2);break;
    case 0x11: Wire.write((byte*)&RunTimeToEmpty, 2);break;
    case 0x12: Wire.write((byte*)&AverageTimeToEmpty, 2);break;
    case 0x13: Wire.write((byte*)&AverageTimeToFull, 2);break;
    case 0x16: Wire.write((byte*)&BatteryStatus, 2);break;
    case 0x17: Wire.write((byte*)&CycleCount, 2);break;
    case 0x18: Wire.write((byte*)&DesignCapacity, 2);break;
    case 0x19: Wire.write((byte*)&DesignVoltage, 2);break;
    case 0x1a: Wire.write((byte*)&SpecificationInfo, 2);break;
    case 0x1b: Wire.write((byte*)&ManufactureDate, 2);break;
    case 0x1c: Wire.write((byte*)&SerialNumber, 2);break;
  }
}
// called after received a read command from host
//
void requestEvent ( void )
{
  digitalWrite( 13, !digitalRead( 13 ) );   // for debug
  sendAnswer( command );
}

void setup() {
  pinMode( 13, OUTPUT ); /**/
  Wire.begin(11);
  Wire.onRequest(requestEvent);

  Serial.begin(38400);
  Serial.println( "Ready" );

//  displayStatus = true;
}

void loop() {
  Serial.print( "Current                =" ); Serial.println( Current );
  Serial.print( "Voltage                =" ); Serial.println( Voltage );
  Serial.println();
}
