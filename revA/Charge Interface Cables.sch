EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr B 17000 11000
encoding utf-8
Sheet 1 1
Title "PCB, Profiler Lithium Battery"
Date "2021/07/08"
Rev "A"
Comp "Woods Hole Oceanographic Inst"
Comment1 "AOPE - OOI"
Comment2 "LOSOS Profiler Lab"
Comment3 "266 Woods Hole Road"
Comment4 "Woods Hole, MA 02543"
$EndDescr
$Comp
L Connector_Generic:Conn_01x06 J2
U 1 1 5B3A61E0
P 7775 9900
F 0 "J2" H 7375 9550 50  0000 L CNN
F 1 "PROGRAM" H 7375 9450 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Horizontal" H 7775 9900 50  0001 C CNN
F 3 "https://cdn.amphenol-icc.com/media/wysiwyg/files/drawing/68015.pdf" H 7775 9900 50  0001 C CNN
F 4 "Amphenol ICC (FCI)" H -6525 5050 50  0001 C CNN "MFR"
F 5 "68016-206HLF" H -6525 5050 50  0001 C CNN "MPN"
F 6 "-" H -6525 5050 50  0001 C CNN "SPR"
F 7 "-" H -6525 5050 50  0001 C CNN "SPN"
F 8 "-" H -6525 5050 50  0001 C CNN "SPURL"
	1    7775 9900
	1    0    0    -1  
$EndComp
$Comp
L 109633-rescue:LTST-C155-dgi2 D1
U 1 1 5B3CE686
P 1775 9200
F 0 "D1" H 1675 9350 60  0000 C CNN
F 1 "LTST-C155" H 1850 8900 60  0000 C CNN
F 2 "Footprint:LED_LTST-C155GEKT" H 1775 9200 60  0001 C CNN
F 3 "https://optoelectronics.liteon.com/upload/download/DS22-2001-038/LTST-C155KGJRKT.pdf" H 1775 9200 60  0001 C CNN
F 4 "Lite-On Inc." H -6975 3450 50  0001 C CNN "MFR"
F 5 "LTST-C155KGJRKT" H -6975 3450 50  0001 C CNN "MPN"
F 6 "-" H -6975 3450 50  0001 C CNN "SPR"
F 7 "-" H -6975 3450 50  0001 C CNN "SPN"
F 8 "-" H -6975 3450 50  0001 C CNN "SPURL"
	1    1775 9200
	1    0    0    -1  
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R3
U 1 1 5DFF744E
P 1450 7050
F 0 "R3" H 1550 6975 50  0000 C CNN
F 1 "220" H 1325 6975 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1450 7050 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 1450 7050 60  0001 C CNN
F 4 "Susumu" H -6775 1300 50  0001 C CNN "MFR"
F 5 "RR0816P-221-D" H -6775 1300 50  0001 C CNN "MPN"
F 6 "-" H -6775 1300 50  0001 C CNN "SPR"
F 7 "-" H -6775 1300 50  0001 C CNN "SPN"
F 8 "-" H -6775 1300 50  0001 C CNN "SPURL"
	1    1450 7050
	0    -1   -1   0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R4
U 1 1 5B434687
P 1275 7050
F 0 "R4" H 1375 6975 50  0000 C CNN
F 1 "220" H 1150 6975 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1275 7050 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 1275 7050 60  0001 C CNN
F 4 "Susumu" H -6950 1150 50  0001 C CNN "MFR"
F 5 "RR0816P-221-D" H -6950 1150 50  0001 C CNN "MPN"
F 6 "-" H -6950 1150 50  0001 C CNN "SPR"
F 7 "-" H -6950 1150 50  0001 C CNN "SPN"
F 8 "-" H -6950 1150 50  0001 C CNN "SPURL"
	1    1275 7050
	0    -1   -1   0   
$EndComp
NoConn ~ 7575 10200
NoConn ~ 3175 8200
NoConn ~ 3175 8300
Wire Wire Line
	4075 10350 4175 10350
Wire Wire Line
	3025 10100 3025 9500
Wire Wire Line
	3025 9500 3175 9500
Wire Wire Line
	3175 9400 2925 9400
Wire Wire Line
	2925 9400 2925 10000
Wire Wire Line
	5175 8100 5775 8100
Wire Wire Line
	2025 9350 2875 9350
Wire Wire Line
	2875 9350 2875 9300
Wire Wire Line
	2875 9300 3175 9300
Wire Wire Line
	1525 9200 1450 9200
Wire Wire Line
	1525 9350 1275 9350
Wire Wire Line
	5425 7250 5425 7075
Wire Wire Line
	5125 7250 5125 7075
Connection ~ 4175 10350
Text Notes 7275 9600 0    60   ~ 0
ICD Programmer
Wire Wire Line
	5425 6725 5425 6875
Wire Wire Line
	5125 6725 5425 6725
Connection ~ 5125 6725
Wire Wire Line
	5125 6725 5125 6875
$Comp
L Device:C_Small C11
U 1 1 5C29B5DE
P 5425 6975
F 0 "C11" H 5435 7045 50  0000 L CNN
F 1 "0.01u" H 5435 6895 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5425 6975 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL10B103KB8NCNC.jsp" H 5425 6975 50  0001 C CNN
F 4 "Samsung Electro-Mechanics" H -4375 5550 50  0001 C CNN "MFR"
F 5 "CL10B103KB8NCNC" H -4375 5550 50  0001 C CNN "MPN"
F 6 "-" H -4375 5550 50  0001 C CNN "SPR"
F 7 "-" H -4375 5550 50  0001 C CNN "SPN"
F 8 "-" H -4375 5550 50  0001 C CNN "SPURL"
	1    5425 6975
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C10
U 1 1 5C299B3A
P 5125 6975
F 0 "C10" H 5135 7045 50  0000 L CNN
F 1 "0.1u" H 5135 6895 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5125 6975 50  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL10B103KB8NCNC.jsp" H 5125 6975 50  0001 C CNN
F 4 "Kemet" H -4675 5550 50  0001 C CNN "MFR"
F 5 "C0603X104K5RAC7411" H -4675 5550 50  0001 C CNN "MPN"
F 6 "-" H -4675 5550 50  0001 C CNN "SPR"
F 7 "-" H -4675 5550 50  0001 C CNN "SPN"
F 8 "-" H -4675 5550 50  0001 C CNN "SPURL"
	1    5125 6975
	1    0    0    -1  
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R9
U 1 1 5F1DB8EB
P 6025 7000
F 0 "R9" V 5550 7000 50  0000 C CNN
F 1 "4.7K" V 5650 7000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6025 7000 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H -3600 4425 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H -3600 4425 50  0001 C CNN "MPN"
F 6 "-" H -3600 4425 50  0001 C CNN "SPR"
F 7 "-" H -3600 4425 50  0001 C CNN "SPN"
F 8 "-" H -3600 4425 50  0001 C CNN "SPURL"
	1    6025 7000
	0    -1   1    0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R10
U 1 1 5F1DB8FA
P 5775 7000
F 0 "R10" V 5300 7000 50  0000 C CNN
F 1 "4.7K" V 5400 7000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5775 7000 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H -4275 4425 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H -4275 4425 50  0001 C CNN "MPN"
F 6 "-" H -4275 4425 50  0001 C CNN "SPR"
F 7 "-" H -4275 4425 50  0001 C CNN "SPN"
F 8 "-" H -4275 4425 50  0001 C CNN "SPURL"
	1    5775 7000
	0    1    1    0   
$EndComp
Wire Wire Line
	4175 10350 5425 10350
$Comp
L Device:Crystal Y1
U 1 1 5E7F5E30
P 2775 8400
F 0 "Y1" V 3025 8300 50  0000 L CNN
F 1 "3.686 MHz" V 3025 8450 50  0000 L CNN
F 2 "Crystal:Crystal_SMD_HC49-SD" H 2775 8400 50  0001 C CNN
F 3 "https://ecsxtal.com/store/pdf/csm-7x.pdf" H 2775 8400 50  0001 C CNN
F 4 "ECS-36-18-5PLX-AGN-TR" V 2775 8400 50  0001 C CNN "MPN"
F 5 "ECS Inc." V 2775 8400 50  0001 C CNN "MFR"
	1    2775 8400
	0    1    1    0   
$EndComp
Wire Wire Line
	3175 8400 2925 8400
Wire Wire Line
	2925 8400 2925 8200
Wire Wire Line
	2925 8200 2775 8200
Wire Wire Line
	2775 8200 2775 8250
Wire Wire Line
	3175 8500 2925 8500
Wire Wire Line
	2925 8500 2925 8600
Wire Wire Line
	2925 8600 2775 8600
Wire Wire Line
	2775 8600 2775 8550
$Comp
L Device:C_Small C4
U 1 1 5E883D9B
P 2525 8200
F 0 "C4" V 2275 8175 50  0000 L CNN
F 1 "27pF" V 2375 8150 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2525 8200 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0603C270J1GACAUTO.pdf" H 2525 8200 50  0001 C CNN
F 4 "Kemet" H -7275 6775 50  0001 C CNN "MFR"
F 5 "C0603C270J1GACAUTO" H -7275 6775 50  0001 C CNN "MPN"
F 6 "-" H -7275 6775 50  0001 C CNN "SPR"
F 7 "-" H -7275 6775 50  0001 C CNN "SPN"
F 8 "-" H -7275 6775 50  0001 C CNN "SPURL"
	1    2525 8200
	0    1    1    0   
$EndComp
$Comp
L Device:C_Small C5
U 1 1 5E8863ED
P 2525 8600
F 0 "C5" V 2650 8550 50  0000 L CNN
F 1 "27 pF" V 2750 8525 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 2525 8600 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0603C270J1GACAUTO.pdf" H 2525 8600 50  0001 C CNN
F 4 "Kemet" H -7275 7175 50  0001 C CNN "MFR"
F 5 "C0603C270J1GACAUTO" H -7275 7175 50  0001 C CNN "MPN"
F 6 "-" H -7275 7175 50  0001 C CNN "SPR"
F 7 "-" H -7275 7175 50  0001 C CNN "SPN"
F 8 "-" H -7275 7175 50  0001 C CNN "SPURL"
	1    2525 8600
	0    1    1    0   
$EndComp
Wire Wire Line
	2625 8200 2775 8200
Connection ~ 2775 8200
Wire Wire Line
	2775 8600 2625 8600
Connection ~ 2775 8600
Text Notes 1173 4865 2    60   ~ 0
5V_FRM_STC\nSTC RX\nSTC TX\nGND
Wire Wire Line
	5775 7100 5775 8100
$Comp
L Graphic:SYM_Arrow_Small #SYM1
U 1 1 5F20D644
P 698 4640
F 0 "#SYM1" H 698 4700 50  0001 C CNN
F 1 "SYM_Arrow_Small" H 708 4590 50  0001 C CNN
F 2 "" H 698 4640 50  0001 C CNN
F 3 "~" H 698 4640 50  0001 C CNN
	1    698  4640
	-1   0    0    1   
$EndComp
$Comp
L Graphic:SYM_Arrow_Small #SYM2
U 1 1 5F20E056
P 698 4715
F 0 "#SYM2" H 698 4775 50  0001 C CNN
F 1 "SYM_Arrow_Small" H 708 4665 50  0001 C CNN
F 2 "" H 698 4715 50  0001 C CNN
F 3 "~" H 698 4715 50  0001 C CNN
	1    698  4715
	1    0    0    -1  
$EndComp
NoConn ~ 5175 7900
Wire Wire Line
	5175 8200 6025 8200
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F3459D
P 8225 1725
AR Path="/5B11D17D/60F3459D" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F3459D" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F3459D" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F3459D" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F3459D" Ref="R?"  Part="1" 
AR Path="/5F745286/60F3459D" Ref="R?"  Part="1" 
AR Path="/60F3459D" Ref="R5"  Part="1" 
F 0 "R5" V 7725 1725 50  0000 C CNN
F 1 "4.7K" V 7800 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8225 1725 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H 4075 300 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H 4075 300 50  0001 C CNN "MPN"
F 6 "-" H 4075 300 50  0001 C CNN "SPR"
F 7 "-" H 4075 300 50  0001 C CNN "SPN"
F 8 "-" H 4075 300 50  0001 C CNN "SPURL"
	1    8225 1725
	0    1    1    0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F345A8
P 8300 1725
AR Path="/5B11D17D/60F345A8" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F345A8" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F345A8" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F345A8" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F345A8" Ref="R?"  Part="1" 
AR Path="/5F745286/60F345A8" Ref="R?"  Part="1" 
AR Path="/60F345A8" Ref="R6"  Part="1" 
F 0 "R6" V 7575 1725 50  0000 C CNN
F 1 "4.7K" V 7650 1725 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8300 1725 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H 3875 225 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H 3875 225 50  0001 C CNN "MPN"
F 6 "-" H 3875 225 50  0001 C CNN "SPR"
F 7 "-" H 3875 225 50  0001 C CNN "SPN"
F 8 "-" H 3875 225 50  0001 C CNN "SPURL"
	1    8300 1725
	0    1    1    0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F345B3
P 8500 1725
AR Path="/5B11D17D/60F345B3" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F345B3" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F345B3" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F345B3" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F345B3" Ref="R?"  Part="1" 
AR Path="/5F745286/60F345B3" Ref="R?"  Part="1" 
AR Path="/60F345B3" Ref="R7"  Part="1" 
F 0 "R7" V 8000 1725 50  0000 C CNN
F 1 "4.7K" V 8075 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8500 1725 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H 3850 400 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H 3850 400 50  0001 C CNN "MPN"
F 6 "-" H 3850 400 50  0001 C CNN "SPR"
F 7 "-" H 3850 400 50  0001 C CNN "SPN"
F 8 "-" H 3850 400 50  0001 C CNN "SPURL"
	1    8500 1725
	0    1    1    0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F345BE
P 8575 1725
AR Path="/5B11D17D/60F345BE" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F345BE" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F345BE" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F345BE" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F345BE" Ref="R?"  Part="1" 
AR Path="/5F745286/60F345BE" Ref="R?"  Part="1" 
AR Path="/60F345BE" Ref="R12"  Part="1" 
F 0 "R12" V 7875 1725 50  0000 C CNN
F 1 "4.7K" V 7950 1725 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8575 1725 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H 3700 200 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H 3700 200 50  0001 C CNN "MPN"
F 6 "-" H 3700 200 50  0001 C CNN "SPR"
F 7 "-" H 3700 200 50  0001 C CNN "SPN"
F 8 "-" H 3700 200 50  0001 C CNN "SPURL"
	1    8575 1725
	0    1    1    0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F345C9
P 11050 1700
AR Path="/5B11D17D/60F345C9" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F345C9" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F345C9" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F345C9" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F345C9" Ref="R?"  Part="1" 
AR Path="/5F745286/60F345C9" Ref="R?"  Part="1" 
AR Path="/60F345C9" Ref="R28"  Part="1" 
F 0 "R28" V 10575 1700 50  0000 C CNN
F 1 "4.7K" V 10650 1700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 11050 1700 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H 3175 375 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H 3175 375 50  0001 C CNN "MPN"
F 6 "-" H 3175 375 50  0001 C CNN "SPR"
F 7 "-" H 3175 375 50  0001 C CNN "SPN"
F 8 "-" H 3175 375 50  0001 C CNN "SPURL"
	1    11050 1700
	0    -1   1    0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F345D4
P 11150 1700
AR Path="/5B11D17D/60F345D4" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F345D4" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F345D4" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F345D4" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F345D4" Ref="R?"  Part="1" 
AR Path="/5F745286/60F345D4" Ref="R?"  Part="1" 
AR Path="/60F345D4" Ref="R26"  Part="1" 
F 0 "R26" V 10520 1693 50  0000 C CNN
F 1 "4.7K" V 10595 1693 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 11150 1700 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H 3525 200 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H 3525 200 50  0001 C CNN "MPN"
F 6 "-" H 3525 200 50  0001 C CNN "SPR"
F 7 "-" H 3525 200 50  0001 C CNN "SPN"
F 8 "-" H 3525 200 50  0001 C CNN "SPURL"
	1    11150 1700
	0    -1   1    0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F345DF
P 10775 1650
AR Path="/5B11D17D/60F345DF" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F345DF" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F345DF" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F345DF" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F345DF" Ref="R?"  Part="1" 
AR Path="/5F745286/60F345DF" Ref="R?"  Part="1" 
AR Path="/60F345DF" Ref="R30"  Part="1" 
F 0 "R30" V 11250 1650 50  0000 C CNN
F 1 "4.7K" V 11175 1650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10775 1650 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H 2425 300 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H 2425 300 50  0001 C CNN "MPN"
F 6 "-" H 2425 300 50  0001 C CNN "SPR"
F 7 "-" H 2425 300 50  0001 C CNN "SPN"
F 8 "-" H 2425 300 50  0001 C CNN "SPURL"
	1    10775 1650
	0    -1   -1   0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F345EA
P 10875 1650
AR Path="/5B11D17D/60F345EA" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F345EA" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F345EA" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F345EA" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F345EA" Ref="R?"  Part="1" 
AR Path="/5F745286/60F345EA" Ref="R?"  Part="1" 
AR Path="/60F345EA" Ref="R29"  Part="1" 
F 0 "R29" V 10250 1650 50  0000 C CNN
F 1 "4.7K" V 10325 1650 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10875 1650 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H 2775 125 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H 2775 125 50  0001 C CNN "MPN"
F 6 "-" H 2775 125 50  0001 C CNN "SPR"
F 7 "-" H 2775 125 50  0001 C CNN "SPN"
F 8 "-" H 2775 125 50  0001 C CNN "SPURL"
	1    10875 1650
	0    -1   1    0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 60F345F5
P 12350 2175
AR Path="/5B11D17D/60F345F5" Ref="C?"  Part="1" 
AR Path="/5B13E060/60F345F5" Ref="C?"  Part="1" 
AR Path="/5E57930B/60F345F5" Ref="C?"  Part="1" 
AR Path="/5EFBE18D/60F345F5" Ref="C?"  Part="1" 
AR Path="/5F6C1276/60F345F5" Ref="C?"  Part="1" 
AR Path="/5F745286/60F345F5" Ref="C?"  Part="1" 
AR Path="/60F345F5" Ref="C13"  Part="1" 
F 0 "C13" H 12275 3250 50  0000 L CNN
F 1 "0.1u" H 12275 3150 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 12350 2175 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0603X104K5RAC7411.pdf" H 12350 2175 50  0001 C CNN
F 4 "Kemet" H 2550 750 50  0001 C CNN "MFR"
F 5 "C0603X104K5RAC7411" H 2550 750 50  0001 C CNN "MPN"
F 6 "-" H 2550 750 50  0001 C CNN "SPR"
F 7 "-" H 2550 750 50  0001 C CNN "SPN"
F 8 "-" H 2550 750 50  0001 C CNN "SPURL"
	1    12350 2175
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 60F34600
P 12600 2175
AR Path="/5B11D17D/60F34600" Ref="C?"  Part="1" 
AR Path="/5B13E060/60F34600" Ref="C?"  Part="1" 
AR Path="/5E57930B/60F34600" Ref="C?"  Part="1" 
AR Path="/5EFBE18D/60F34600" Ref="C?"  Part="1" 
AR Path="/5F6C1276/60F34600" Ref="C?"  Part="1" 
AR Path="/5F745286/60F34600" Ref="C?"  Part="1" 
AR Path="/60F34600" Ref="C15"  Part="1" 
F 0 "C15" H 12525 3250 50  0000 L CNN
F 1 "1u" H 12550 3150 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 12638 2025 30  0001 C CNN
F 3 "http://www.samsungsem.com/kr/support/product-search/mlcc/CL10B105KO8NFNC.jsp" H 12600 2175 60  0001 C CNN
F 4 "Samsung Electro-Mechanics" H 1525 775 50  0001 C CNN "MFR"
F 5 "CL10B105KO8NFNC" H 1525 775 50  0001 C CNN "MPN"
F 6 "-" H 1525 775 50  0001 C CNN "SPR"
F 7 "-" H 1525 775 50  0001 C CNN "SPN"
F 8 "-" H 1525 775 50  0001 C CNN "SPURL"
	1    12600 2175
	-1   0    0    -1  
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F34616
P 8775 1725
AR Path="/5B11D17D/60F34616" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F34616" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F34616" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F34616" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F34616" Ref="R?"  Part="1" 
AR Path="/5F745286/60F34616" Ref="R?"  Part="1" 
AR Path="/60F34616" Ref="R13"  Part="1" 
F 0 "R13" V 8275 1725 50  0000 C CNN
F 1 "4.7K" V 8350 1725 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8775 1725 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H 3600 175 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H 3600 175 50  0001 C CNN "MPN"
F 6 "-" H 3600 175 50  0001 C CNN "SPR"
F 7 "-" H 3600 175 50  0001 C CNN "SPN"
F 8 "-" H 3600 175 50  0001 C CNN "SPURL"
	1    8775 1725
	0    1    1    0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F34621
P 8850 1725
AR Path="/5B11D17D/60F34621" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F34621" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F34621" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F34621" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F34621" Ref="R?"  Part="1" 
AR Path="/5F745286/60F34621" Ref="R?"  Part="1" 
AR Path="/60F34621" Ref="R14"  Part="1" 
F 0 "R14" V 8150 1725 50  0000 C CNN
F 1 "4.7K" V 8225 1725 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8850 1725 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H 3400 100 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H 3400 100 50  0001 C CNN "MPN"
F 6 "-" H 3400 100 50  0001 C CNN "SPR"
F 7 "-" H 3400 100 50  0001 C CNN "SPN"
F 8 "-" H 3400 100 50  0001 C CNN "SPURL"
	1    8850 1725
	0    1    1    0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F3462C
P 9050 1725
AR Path="/5B11D17D/60F3462C" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F3462C" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F3462C" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F3462C" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F3462C" Ref="R?"  Part="1" 
AR Path="/5F745286/60F3462C" Ref="R?"  Part="1" 
AR Path="/60F3462C" Ref="R15"  Part="1" 
F 0 "R15" V 8550 1725 50  0000 C CNN
F 1 "4.7K" V 8625 1725 50  0000 C CNN
F 2 "" H 9050 1725 50  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H 3375 275 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H 3375 275 50  0001 C CNN "MPN"
F 6 "-" H 3375 275 50  0001 C CNN "SPR"
F 7 "-" H 3375 275 50  0001 C CNN "SPN"
F 8 "-" H 3375 275 50  0001 C CNN "SPURL"
	1    9050 1725
	0    1    1    0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F34637
P 9125 1725
AR Path="/5B11D17D/60F34637" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F34637" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F34637" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F34637" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F34637" Ref="R?"  Part="1" 
AR Path="/5F745286/60F34637" Ref="R?"  Part="1" 
AR Path="/60F34637" Ref="R18"  Part="1" 
F 0 "R18" V 8425 1725 50  0000 C CNN
F 1 "4.7K" V 8500 1725 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 9125 1725 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H 3225 75  50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H 3225 75  50  0001 C CNN "MPN"
F 6 "-" H 3225 75  50  0001 C CNN "SPR"
F 7 "-" H 3225 75  50  0001 C CNN "SPN"
F 8 "-" H 3225 75  50  0001 C CNN "SPURL"
	1    9125 1725
	0    1    1    0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F34642
P 11700 1700
AR Path="/5B11D17D/60F34642" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F34642" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F34642" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F34642" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F34642" Ref="R?"  Part="1" 
AR Path="/5F745286/60F34642" Ref="R?"  Part="1" 
AR Path="/60F34642" Ref="R19"  Part="1" 
F 0 "R19" V 11075 1700 50  0000 C CNN
F 1 "4.7K" V 11150 1700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 11700 1700 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H 4075 200 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H 4075 200 50  0001 C CNN "MPN"
F 6 "-" H 4075 200 50  0001 C CNN "SPR"
F 7 "-" H 4075 200 50  0001 C CNN "SPN"
F 8 "-" H 4075 200 50  0001 C CNN "SPURL"
	1    11700 1700
	0    -1   1    0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F3464D
P 11600 1700
AR Path="/5B11D17D/60F3464D" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F3464D" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F3464D" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F3464D" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F3464D" Ref="R?"  Part="1" 
AR Path="/5F745286/60F3464D" Ref="R?"  Part="1" 
AR Path="/60F3464D" Ref="R22"  Part="1" 
F 0 "R22" V 11125 1700 50  0000 C CNN
F 1 "4.7K" V 11200 1700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 11600 1700 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H 3975 200 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H 3975 200 50  0001 C CNN "MPN"
F 6 "-" H 3975 200 50  0001 C CNN "SPR"
F 7 "-" H 3975 200 50  0001 C CNN "SPN"
F 8 "-" H 3975 200 50  0001 C CNN "SPURL"
	1    11600 1700
	0    -1   1    0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F34658
P 11325 1700
AR Path="/5B11D17D/60F34658" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F34658" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F34658" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F34658" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F34658" Ref="R?"  Part="1" 
AR Path="/5F745286/60F34658" Ref="R?"  Part="1" 
AR Path="/60F34658" Ref="R24"  Part="1" 
F 0 "R24" V 10850 1700 50  0000 C CNN
F 1 "4.7K" V 10925 1700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 11325 1700 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H 3700 200 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H 3700 200 50  0001 C CNN "MPN"
F 6 "-" H 3700 200 50  0001 C CNN "SPR"
F 7 "-" H 3700 200 50  0001 C CNN "SPN"
F 8 "-" H 3700 200 50  0001 C CNN "SPURL"
	1    11325 1700
	0    -1   1    0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F34663
P 11425 1700
AR Path="/5B11D17D/60F34663" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F34663" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F34663" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F34663" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F34663" Ref="R?"  Part="1" 
AR Path="/5F745286/60F34663" Ref="R?"  Part="1" 
AR Path="/60F34663" Ref="R23"  Part="1" 
F 0 "R23" V 10800 1700 50  0000 C CNN
F 1 "4.7K" V 10875 1700 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 11425 1700 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H 3800 200 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H 3800 200 50  0001 C CNN "MPN"
F 6 "-" H 3800 200 50  0001 C CNN "SPR"
F 7 "-" H 3800 200 50  0001 C CNN "SPN"
F 8 "-" H 3800 200 50  0001 C CNN "SPURL"
	1    11425 1700
	0    -1   1    0   
$EndComp
Wire Wire Line
	8225 1825 8225 2075
Wire Wire Line
	8500 1375 8500 1625
Wire Wire Line
	8225 1375 8225 1625
Wire Wire Line
	8300 1375 8300 1625
Wire Wire Line
	8575 1375 8575 1625
Wire Wire Line
	12350 1375 12350 2075
Wire Wire Line
	12350 2275 12350 4375
Wire Wire Line
	8300 2175 8300 1825
Wire Wire Line
	8500 2525 8500 1825
Wire Wire Line
	12600 4375 12600 2275
Wire Wire Line
	12600 1375 12600 2075
Wire Wire Line
	8775 1825 8775 3325
Wire Wire Line
	8775 1375 8775 1625
Wire Wire Line
	8850 1375 8850 1625
Wire Wire Line
	9125 1375 9125 1625
Wire Wire Line
	8850 1825 8850 3425
Wire Wire Line
	9050 1825 9050 3675
Wire Wire Line
	9125 1825 9125 3775
Wire Wire Line
	9050 1625 9050 1375
Wire Wire Line
	11700 1600 11700 1375
Connection ~ 8500 1375
Connection ~ 8300 1375
Connection ~ 8575 1375
Connection ~ 12600 1375
Connection ~ 9125 1375
Connection ~ 9050 1375
Connection ~ 8850 1375
Connection ~ 8775 1375
Wire Wire Line
	8500 1375 8575 1375
Wire Wire Line
	8300 1375 8500 1375
Wire Wire Line
	8575 1375 8775 1375
Wire Wire Line
	12350 1375 12600 1375
Wire Wire Line
	8225 1375 8300 1375
Wire Wire Line
	12600 1375 12850 1375
Wire Wire Line
	9050 1375 9125 1375
Wire Wire Line
	8850 1375 9050 1375
Wire Wire Line
	8775 1375 8850 1375
Connection ~ 12600 4375
Wire Wire Line
	12600 4375 12850 4375
Wire Wire Line
	9125 1375 9250 1375
Wire Wire Line
	9250 1375 9250 1875
Connection ~ 9250 1375
$Comp
L 109633-rescue:Rsmall-dgi2 R31
U 1 1 60F3471B
P 1875 1375
AR Path="/60F3471B" Ref="R31"  Part="1" 
AR Path="/5F6C1276/60F3471B" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F3471B" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F3471B" Ref="R?"  Part="1" 
AR Path="/5F745286/60F3471B" Ref="R?"  Part="1" 
F 0 "R31" H 1975 1300 50  0000 C CNN
F 1 "220" H 1750 1300 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 1875 1375 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 1875 1375 60  0001 C CNN
F 4 "Susumu" H -6350 -4375 50  0001 C CNN "MFR"
F 5 "RR0816P-221-D" H -6350 -4375 50  0001 C CNN "MPN"
F 6 "-" H -6350 -4375 50  0001 C CNN "SPR"
F 7 "-" H -6350 -4375 50  0001 C CNN "SPN"
F 8 "-" H -6350 -4375 50  0001 C CNN "SPURL"
	1    1875 1375
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 60F34732
P 12850 2175
AR Path="/5E57930B/60F34732" Ref="C?"  Part="1" 
AR Path="/5EFBE18D/60F34732" Ref="C?"  Part="1" 
AR Path="/5F6C1276/60F34732" Ref="C?"  Part="1" 
AR Path="/5F745286/60F34732" Ref="C?"  Part="1" 
AR Path="/60F34732" Ref="C17"  Part="1" 
F 0 "C17" H 12775 3275 50  0000 L CNN
F 1 "10uF" H 12775 3175 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 12888 2025 30  0001 C CNN
F 3 "https://datasheets.avx.com/X7RDielectric.pdf" H 12850 2175 60  0001 C CNN
F 4 "AVX Corporation" H 1775 775 50  0001 C CNN "MFR"
F 5 "08056C106KAT4A" H 1775 775 50  0001 C CNN "MPN"
F 6 "-" H 1775 775 50  0001 C CNN "SPR"
F 7 "-" H 1775 775 50  0001 C CNN "SPN"
F 8 "-" H 1775 775 50  0001 C CNN "SPURL"
	1    12850 2175
	1    0    0    -1  
$EndComp
Wire Wire Line
	12850 1375 12850 2075
Connection ~ 12850 1375
Wire Wire Line
	12850 2275 12850 4375
Connection ~ 12850 4375
Wire Wire Line
	14025 7175 14425 7175
$Comp
L Connector_Generic:Conn_02x04_Top_Bottom J?
U 1 1 60F34783
P 15675 5975
AR Path="/5F745286/60F34783" Ref="J?"  Part="1" 
AR Path="/5E57930B/60F34783" Ref="J?"  Part="1" 
AR Path="/60F34783" Ref="BT8"  Part="1" 
F 0 "BT8" H 15725 6200 50  0000 C CNN
F 1 "Conn_02x04_Top_Bottom" H 15725 6201 50  0001 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5569-08A2_2x04_P4.20mm_Horizontal" H 15675 5975 50  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/039301082_sd.pdf" H 13825 5375 50  0001 C CNN
F 4 "Molex" H 15675 5975 50  0001 C CNN "MFR"
F 5 "0039301082" H 15675 5975 50  0001 C CNN "MPN"
	1    15675 5975
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Top_Bottom J?
U 1 1 60F34791
P 13825 5975
AR Path="/5F745286/60F34791" Ref="J?"  Part="1" 
AR Path="/5E57930B/60F34791" Ref="J?"  Part="1" 
AR Path="/60F34791" Ref="BT2"  Part="1" 
F 0 "BT2" H 13875 6200 50  0000 C CNN
F 1 "Conn_02x04_Top_Bottom" H 13875 6201 50  0001 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5569-08A2_2x04_P4.20mm_Horizontal" H 13825 5375 50  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/039301082_sd.pdf" H 13825 5375 50  0001 C CNN
F 4 "Molex" H 13825 5375 50  0001 C CNN "MFR"
F 5 "0039301082" H 13825 5375 50  0001 C CNN "MPN"
	1    13825 5975
	-1   0    0    -1  
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F7E09C
P 3250 1850
AR Path="/5B11D17D/60F7E09C" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F7E09C" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F7E09C" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F7E09C" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F7E09C" Ref="R?"  Part="1" 
AR Path="/5F745286/60F7E09C" Ref="R?"  Part="1" 
AR Path="/60F7E09C" Ref="R11"  Part="1" 
F 0 "R11" V 2650 1850 50  0000 C CNN
F 1 "4.7K" V 2725 1850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3250 1850 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H -900 425 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H -900 425 50  0001 C CNN "MPN"
F 6 "-" H -900 425 50  0001 C CNN "SPR"
F 7 "-" H -900 425 50  0001 C CNN "SPN"
F 8 "-" H -900 425 50  0001 C CNN "SPURL"
	1    3250 1850
	0    1    1    0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F7E0A7
P 3175 1850
AR Path="/5B11D17D/60F7E0A7" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F7E0A7" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F7E0A7" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F7E0A7" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F7E0A7" Ref="R?"  Part="1" 
AR Path="/5F745286/60F7E0A7" Ref="R?"  Part="1" 
AR Path="/60F7E0A7" Ref="R8"  Part="1" 
F 0 "R8" V 2350 1850 50  0000 C CNN
F 1 "4.7K" V 2425 1850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3175 1850 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H -1250 350 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H -1250 350 50  0001 C CNN "MPN"
F 6 "-" H -1250 350 50  0001 C CNN "SPR"
F 7 "-" H -1250 350 50  0001 C CNN "SPN"
F 8 "-" H -1250 350 50  0001 C CNN "SPURL"
	1    3175 1850
	0    1    1    0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F7E0B2
P 6250 1800
AR Path="/5B11D17D/60F7E0B2" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F7E0B2" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F7E0B2" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F7E0B2" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F7E0B2" Ref="R?"  Part="1" 
AR Path="/5F745286/60F7E0B2" Ref="R?"  Part="1" 
AR Path="/60F7E0B2" Ref="R21"  Part="1" 
F 0 "R21" V 5525 1800 50  0000 C CNN
F 1 "4.7K" V 5600 1800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6250 1800 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H -1625 475 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H -1625 475 50  0001 C CNN "MPN"
F 6 "-" H -1625 475 50  0001 C CNN "SPR"
F 7 "-" H -1625 475 50  0001 C CNN "SPN"
F 8 "-" H -1625 475 50  0001 C CNN "SPURL"
	1    6250 1800
	0    -1   1    0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F7E0BD
P 6375 1800
AR Path="/5B11D17D/60F7E0BD" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F7E0BD" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F7E0BD" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F7E0BD" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F7E0BD" Ref="R?"  Part="1" 
AR Path="/5F745286/60F7E0BD" Ref="R?"  Part="1" 
AR Path="/60F7E0BD" Ref="R20"  Part="1" 
F 0 "R20" V 5800 1800 50  0000 C CNN
F 1 "4.7K" V 5875 1800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6375 1800 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H -1250 300 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H -1250 300 50  0001 C CNN "MPN"
F 6 "-" H -1250 300 50  0001 C CNN "SPR"
F 7 "-" H -1250 300 50  0001 C CNN "SPN"
F 8 "-" H -1250 300 50  0001 C CNN "SPURL"
	1    6375 1800
	0    -1   1    0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F7E0C8
P 6000 1800
AR Path="/5B11D17D/60F7E0C8" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F7E0C8" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F7E0C8" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F7E0C8" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F7E0C8" Ref="R?"  Part="1" 
AR Path="/5F745286/60F7E0C8" Ref="R?"  Part="1" 
AR Path="/60F7E0C8" Ref="R27"  Part="1" 
F 0 "R27" V 5275 1800 50  0000 C CNN
F 1 "4.7K" V 5350 1800 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6000 1800 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H -2350 450 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H -2350 450 50  0001 C CNN "MPN"
F 6 "-" H -2350 450 50  0001 C CNN "SPR"
F 7 "-" H -2350 450 50  0001 C CNN "SPN"
F 8 "-" H -2350 450 50  0001 C CNN "SPURL"
	1    6000 1800
	0    -1   1    0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F7E0D3
P 6100 1825
AR Path="/5B11D17D/60F7E0D3" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F7E0D3" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F7E0D3" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F7E0D3" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F7E0D3" Ref="R?"  Part="1" 
AR Path="/5F745286/60F7E0D3" Ref="R?"  Part="1" 
AR Path="/60F7E0D3" Ref="R25"  Part="1" 
F 0 "R25" V 5500 1825 50  0000 C CNN
F 1 "4.7K" V 5575 1825 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6100 1825 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H -2000 300 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H -2000 300 50  0001 C CNN "MPN"
F 6 "-" H -2000 300 50  0001 C CNN "SPR"
F 7 "-" H -2000 300 50  0001 C CNN "SPN"
F 8 "-" H -2000 300 50  0001 C CNN "SPURL"
	1    6100 1825
	0    -1   1    0   
$EndComp
Wire Wire Line
	5000 1375 5000 1725
Wire Wire Line
	6250 2375 6250 1900
Wire Wire Line
	6000 2075 6000 1900
Wire Wire Line
	6100 1925 6100 2175
Wire Wire Line
	6375 1375 6375 1700
Wire Wire Line
	6250 1375 6250 1700
Wire Wire Line
	6100 1375 6100 1725
Wire Wire Line
	6000 1375 6000 1700
Wire Wire Line
	4200 2025 4300 2025
$Comp
L 9546:PCA9546AD U?
U 1 1 60F7E147
P 5000 2925
AR Path="/5F745286/60F7E147" Ref="U?"  Part="1" 
AR Path="/60F7E147" Ref="U5"  Part="1" 
F 0 "U5" H 4575 4200 50  0000 C CNN
F 1 "PCA9546AD" H 4650 4075 50  0000 C CNN
F 2 "SOIC127P600X175-16N" H 5000 2925 50  0001 L BNN
F 3 "https://www.nxp.com/docs/en/data-sheet/PCA9546A.pdf" H 5000 2925 50  0001 L BNN
F 4 "NXP" H 5000 2925 50  0001 C CNN "MFR"
F 5 "PCA9546AD,112" H 5000 2925 50  0001 C CNN "MPN"
	1    5000 2925
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 2925 3475 2925
Wire Wire Line
	3400 3025 3400 1950
Wire Wire Line
	3475 2925 3475 1950
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F7E160
P 3400 1850
AR Path="/5B11D17D/60F7E160" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F7E160" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F7E160" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F7E160" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F7E160" Ref="R?"  Part="1" 
AR Path="/5F745286/60F7E160" Ref="R?"  Part="1" 
AR Path="/60F7E160" Ref="R16"  Part="1" 
F 0 "R16" V 2650 1850 50  0000 C CNN
F 1 "4.7K" V 2725 1850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3400 1850 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H -1475 325 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H -1475 325 50  0001 C CNN "MPN"
F 6 "-" H -1475 325 50  0001 C CNN "SPR"
F 7 "-" H -1475 325 50  0001 C CNN "SPN"
F 8 "-" H -1475 325 50  0001 C CNN "SPURL"
	1    3400 1850
	0    1    1    0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 60F7E16B
P 3475 1850
AR Path="/5B11D17D/60F7E16B" Ref="R?"  Part="1" 
AR Path="/5B13E060/60F7E16B" Ref="R?"  Part="1" 
AR Path="/5E57930B/60F7E16B" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/60F7E16B" Ref="R?"  Part="1" 
AR Path="/5F6C1276/60F7E16B" Ref="R?"  Part="1" 
AR Path="/5F745286/60F7E16B" Ref="R?"  Part="1" 
AR Path="/60F7E16B" Ref="R17"  Part="1" 
F 0 "R17" V 2875 1850 50  0000 C CNN
F 1 "4.7K" V 2950 1850 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 3475 1850 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 3175 1850 60  0001 C CNN
F 4 "Susumu" H -1175 525 50  0001 C CNN "MFR"
F 5 "RR0816P-472-D" H -1175 525 50  0001 C CNN "MPN"
F 6 "-" H -1175 525 50  0001 C CNN "SPR"
F 7 "-" H -1175 525 50  0001 C CNN "SPN"
F 8 "-" H -1175 525 50  0001 C CNN "SPURL"
	1    3475 1850
	0    1    1    0   
$EndComp
Wire Wire Line
	4300 3025 3400 3025
$Comp
L Connector_Generic:Conn_02x04_Top_Bottom J?
U 1 1 60F7E192
P 15675 5375
AR Path="/5F745286/60F7E192" Ref="J?"  Part="1" 
AR Path="/5E57930B/60F7E192" Ref="J?"  Part="1" 
AR Path="/60F7E192" Ref="BT7"  Part="1" 
F 0 "BT7" H 15725 5600 50  0000 C CNN
F 1 "Conn_02x04_Top_Bottom" H 15725 5601 50  0001 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5569-08A2_2x04_P4.20mm_Horizontal" H 15675 5375 50  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/039301082_sd.pdf" H 13825 5375 50  0001 C CNN
F 4 "Molex" H 15675 5375 50  0001 C CNN "MFR"
F 5 "0039301082" H 15675 5375 50  0001 C CNN "MPN"
	1    15675 5375
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Top_Bottom J?
U 1 1 60F7E199
P 15700 7775
AR Path="/5F745286/60F7E199" Ref="J?"  Part="1" 
AR Path="/5E57930B/60F7E199" Ref="J?"  Part="1" 
AR Path="/60F7E199" Ref="BT11"  Part="1" 
F 0 "BT11" H 15750 8000 50  0000 C CNN
F 1 "Conn_02x04_Top_Bottom" H 15750 8001 50  0001 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5569-08A2_2x04_P4.20mm_Horizontal" H 15700 7775 50  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/039301082_sd.pdf" H 13825 5375 50  0001 C CNN
F 4 "Molex" H 15700 7775 50  0001 C CNN "MFR"
F 5 "0039301082" H 15700 7775 50  0001 C CNN "MPN"
	1    15700 7775
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Top_Bottom J?
U 1 1 60F7E1A1
P 15700 8375
AR Path="/5F745286/60F7E1A1" Ref="J?"  Part="1" 
AR Path="/5E57930B/60F7E1A1" Ref="J?"  Part="1" 
AR Path="/60F7E1A1" Ref="BT12"  Part="1" 
F 0 "BT12" H 15750 8600 50  0000 C CNN
F 1 "Conn_02x04_Top_Bottom" H 15750 8601 50  0001 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5569-08A2_2x04_P4.20mm_Horizontal" H 15700 8375 50  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/039301082_sd.pdf" H 13825 5375 50  0001 C CNN
F 4 "Molex" H 15700 8375 50  0001 C CNN "MFR"
F 5 "0039301082" H 15700 8375 50  0001 C CNN "MPN"
	1    15700 8375
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Top_Bottom J?
U 1 1 60F7E1B9
P 15700 7175
AR Path="/5F745286/60F7E1B9" Ref="J?"  Part="1" 
AR Path="/5E57930B/60F7E1B9" Ref="J?"  Part="1" 
AR Path="/60F7E1B9" Ref="BT10"  Part="1" 
F 0 "BT10" H 15750 7400 50  0000 C CNN
F 1 "Conn_02x04_Top_Bottom" H 15750 7401 50  0001 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5569-08A2_2x04_P4.20mm_Horizontal" H 15700 7175 50  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/039301082_sd.pdf" H 13825 5375 50  0001 C CNN
F 4 "Molex" H 15700 7175 50  0001 C CNN "MFR"
F 5 "0039301082" H 15700 7175 50  0001 C CNN "MPN"
	1    15700 7175
	-1   0    0    -1  
$EndComp
Wire Wire Line
	4300 2225 3250 2225
Wire Wire Line
	3250 1950 3250 2225
Wire Wire Line
	4300 2325 3175 2325
Wire Wire Line
	3175 2325 3175 1950
$Comp
L Device:C_Small C?
U 1 1 61054046
P 10425 6650
AR Path="/5B34FACD/61054046" Ref="C?"  Part="1" 
AR Path="/61054046" Ref="C8"  Part="1" 
F 0 "C8" H 10435 6720 50  0000 L CNN
F 1 "0.1u" H 10435 6570 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10425 6650 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0603X104K5RAC7411.pdf" H 10425 6650 50  0001 C CNN
F 4 "C0603X104K5RAC7411" H 625 5225 50  0001 C CNN "Kemet"
F 5 "C0603X104K5RAC7411" H 625 5225 50  0001 C CNN "MPN"
F 6 "-" H 625 5225 50  0001 C CNN "SPR"
F 7 "-" H 625 5225 50  0001 C CNN "SPN"
F 8 "-" H 625 5225 50  0001 C CNN "SPURL"
F 9 "Kemet" H 10425 6650 50  0001 C CNN "MFR"
	1    10425 6650
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 61054051
P 8050 7375
AR Path="/5B34FACD/61054051" Ref="C?"  Part="1" 
AR Path="/61054051" Ref="C6"  Part="1" 
F 0 "C6" H 8060 7445 50  0000 L CNN
F 1 "0.1u" H 8060 7295 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8050 7375 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0603X104K5RAC7411.pdf" H 8050 7375 50  0001 C CNN
F 4 "Kemet" H -1750 5950 50  0001 C CNN "MFR"
F 5 "C0603X104K5RAC7411" H -1750 5950 50  0001 C CNN "MPN"
F 6 "-" H -1750 5950 50  0001 C CNN "SPR"
F 7 "-" H -1750 5950 50  0001 C CNN "SPN"
F 8 "-" H -1750 5950 50  0001 C CNN "SPURL"
	1    8050 7375
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 6105405C
P 8400 6025
AR Path="/5B34FACD/6105405C" Ref="C?"  Part="1" 
AR Path="/6105405C" Ref="C7"  Part="1" 
F 0 "C7" H 8410 6095 50  0000 L CNN
F 1 "0.1u" H 8410 5945 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8400 6025 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0603X104K5RAC7411.pdf" H 8400 6025 50  0001 C CNN
F 4 "Kemet" H -1400 4600 50  0001 C CNN "MFR"
F 5 "C0603X104K5RAC7411" H -1400 4600 50  0001 C CNN "MPN"
F 6 "-" H -1400 4600 50  0001 C CNN "SPR"
F 7 "-" H -1400 4600 50  0001 C CNN "SPN"
F 8 "-" H -1400 4600 50  0001 C CNN "SPURL"
	1    8400 6025
	-1   0    0    -1  
$EndComp
$Comp
L 109633-rescue:MAX3323E-dgi2 U?
U 1 1 610540A5
P 9525 7125
AR Path="/5B34FACD/610540A5" Ref="U?"  Part="1" 
AR Path="/610540A5" Ref="U3"  Part="1" 
F 0 "U3" H 9125 8175 70  0000 C CNN
F 1 "MAX3323E" H 9275 8025 70  0000 C CNN
F 2 "Footprint:SOP65P640X110-16N" H 9525 7125 60  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX3322E-MAX3323E.pdf" H 9525 7125 60  0001 C CNN
F 4 "Maxim Integrated" H 0   400 50  0001 C CNN "MFR"
F 5 "MAX3323EEUE+" H 0   400 50  0001 C CNN "MPN"
F 6 "-" H 0   400 50  0001 C CNN "SPR"
F 7 "-" H 0   400 50  0001 C CNN "SPN"
F 8 "-" H 0   400 50  0001 C CNN "SPURL"
	1    9525 7125
	-1   0    0    -1  
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 610540B3
P 11200 6825
AR Path="/5B34FACD/610540B3" Ref="R?"  Part="1" 
AR Path="/610540B3" Ref="R1"  Part="1" 
F 0 "R1" V 11100 6725 40  0000 C CNN
F 1 "100K" V 11200 6725 30  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 11200 6825 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 11200 6825 60  0001 C CNN
F 4 "Susumu" H 7325 5275 50  0001 C CNN "MFR"
F 5 "RR1220P-104-D" H 7325 5275 50  0001 C CNN "MPN"
F 6 "-" H 7325 5275 50  0001 C CNN "SPR"
F 7 "-" H 7325 5275 50  0001 C CNN "SPN"
F 8 "-" H 7325 5275 50  0001 C CNN "SPURL"
	1    11200 6825
	0    1    1    0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 610540BE
P 11350 6825
AR Path="/5B34FACD/610540BE" Ref="R?"  Part="1" 
AR Path="/610540BE" Ref="R2"  Part="1" 
F 0 "R2" V 11250 6725 40  0000 C CNN
F 1 "100K" V 11350 6725 30  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" H 11350 6825 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 11350 6825 60  0001 C CNN
F 4 "Susumu" H 7475 5275 50  0001 C CNN "MFR"
F 5 "RR1220P-104-D" H 7475 5275 50  0001 C CNN "MPN"
F 6 "-" H 7475 5275 50  0001 C CNN "SPR"
F 7 "-" H 7475 5275 50  0001 C CNN "SPN"
F 8 "-" H 7475 5275 50  0001 C CNN "SPURL"
	1    11350 6825
	0    -1   1    0   
$EndComp
$Comp
L Device:C_Small C?
U 1 1 610540C9
P 10425 7125
AR Path="/5B34FACD/610540C9" Ref="C?"  Part="1" 
AR Path="/610540C9" Ref="C9"  Part="1" 
F 0 "C9" H 10435 7195 50  0000 L CNN
F 1 "0.1u" H 10435 7045 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 10425 7125 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0603X104K5RAC7411.pdf" H 10425 7125 50  0001 C CNN
F 4 "Kemet" H 625 5700 50  0001 C CNN "MFR"
F 5 "C0603X104K5RAC7411" H 625 5700 50  0001 C CNN "MPN"
F 6 "-" H 625 5700 50  0001 C CNN "SPR"
F 7 "-" H 625 5700 50  0001 C CNN "SPN"
F 8 "-" H 625 5700 50  0001 C CNN "SPURL"
	1    10425 7125
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C?
U 1 1 610540D4
P 8050 6850
AR Path="/5B34FACD/610540D4" Ref="C?"  Part="1" 
AR Path="/610540D4" Ref="C1"  Part="1" 
F 0 "C1" H 7775 6950 50  0000 L CNN
F 1 "0.1u" H 7775 6875 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8050 6850 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0603X104K5RAC7411.pdf" H 8050 6850 50  0001 C CNN
F 4 "Kemet" H -1750 5425 50  0001 C CNN "MFR"
F 5 "C0603X104K5RAC7411" H -1750 5425 50  0001 C CNN "MPN"
F 6 "-" H -1750 5425 50  0001 C CNN "SPR"
F 7 "-" H -1750 5425 50  0001 C CNN "SPN"
F 8 "-" H -1750 5425 50  0001 C CNN "SPURL"
	1    8050 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	10325 6425 10425 6425
Wire Wire Line
	10425 6425 10425 6550
Wire Wire Line
	10425 6750 10425 6825
Wire Wire Line
	10425 6825 10325 6825
Wire Wire Line
	8400 5675 8400 5925
Wire Wire Line
	8050 7875 8050 7475
Wire Wire Line
	10325 7625 11275 7625
Wire Wire Line
	10325 7525 10950 7525
Wire Wire Line
	11200 6925 11200 7525
Wire Wire Line
	11350 6925 11350 7625
Wire Wire Line
	11350 6725 11350 5675
Wire Wire Line
	11200 6725 11200 5675
Connection ~ 11200 5675
Wire Wire Line
	10325 7825 10850 7825
Wire Wire Line
	10425 6925 10425 7025
Wire Wire Line
	10425 7225 10425 7325
Wire Wire Line
	10425 6925 10325 6925
Wire Wire Line
	10425 7325 10325 7325
Wire Wire Line
	8725 6425 8675 6425
Wire Wire Line
	8675 5675 8675 6425
Connection ~ 8675 5675
Wire Wire Line
	8675 6625 8725 6625
Connection ~ 8675 6425
Wire Wire Line
	8400 7025 8725 7025
Wire Wire Line
	8050 6950 8050 7025
Wire Wire Line
	8050 7025 8400 7025
Connection ~ 8400 7025
Wire Wire Line
	8725 7875 8400 7875
Connection ~ 8400 7875
Wire Wire Line
	8050 7875 8400 7875
Wire Wire Line
	11200 5675 11350 5675
Wire Wire Line
	8675 6425 8675 6625
Wire Wire Line
	8400 7875 8400 7775
Wire Wire Line
	8400 7025 8400 6375
Text Notes 8875 8125 0    60   ~ 0
SHDN MIN_H_THRESHOLD IS  2/3 x VCC
Connection ~ 8400 7775
Wire Wire Line
	8400 7775 8400 7025
Wire Wire Line
	8400 7775 8725 7775
Wire Wire Line
	10850 7825 10850 5675
Wire Wire Line
	10850 5675 11200 5675
Connection ~ 5425 7250
Wire Wire Line
	5125 7250 5425 7250
Wire Wire Line
	5775 9900 5775 10350
Connection ~ 5425 6725
Wire Wire Line
	5775 6725 5775 6900
Wire Wire Line
	4175 6725 4175 7500
Wire Wire Line
	6025 6725 6025 6900
Connection ~ 4175 6725
Wire Wire Line
	8400 5675 8675 5675
Connection ~ 10850 5675
$Comp
L Connector_Generic:Conn_01x04 J19
U 1 1 6103D383
P 1250 3650
F 0 "J19" H 1675 3725 50  0000 C CNN
F 1 "LEAK DET" H 1650 3650 50  0000 C CNN
F 2 "Connector_Molex:Molex_Micro-Fit_3.0_43045-0400_2x02_P3.00mm_Horizontal" H 1250 3650 50  0001 C CNN
F 3 "https://media.digikey.com/pdf/Data%20Sheets/Molex%20PDFs/PK-70873-05xx_1_N3.pdf" H 1250 3650 50  0001 C CNN
F 4 "Molex" H 1250 3650 50  0001 C CNN "MFR"
F 5 "0430450400" H 1250 3650 50  0001 C CNN "MPN"
	1    1250 3650
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Top_Bottom BT9
U 1 1 61B066BC
P 15700 6575
F 0 "BT9" H 15750 6775 50  0000 C CNN
F 1 "Conn_02x04_Top_Bottom" H 15600 6050 50  0001 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5569-08A2_2x04_P4.20mm_Horizontal" H 15700 6575 50  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/039301082_sd.pdf" H 13825 5375 50  0001 C CNN
F 4 "Molex" H 15700 6575 50  0001 C CNN "MFR"
F 5 "0039301082" H 15700 6575 50  0001 C CNN "MPN"
	1    15700 6575
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J16
U 1 1 60F76AD0
P 1250 5525
F 0 "J16" H 1400 5525 50  0000 C CNN
F 1 "RTE" H 1400 5425 50  0000 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5569-02A2_2x01_P4.20mm_Horizontal" H 1250 5525 50  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/ps/PS-5556-001.pdf" H 1250 5525 50  0001 C CNN
F 4 "Molex" H 1250 5525 50  0001 C CNN "MFR"
F 5 "0039301020" H 1250 5525 50  0001 C CNN "MPN"
	1    1250 5525
	-1   0    0    -1  
$EndComp
Wire Wire Line
	14400 5975 14025 5975
Wire Wire Line
	14425 7075 14025 7075
Wire Wire Line
	13100 5875 13525 5875
Wire Wire Line
	13525 7175 13125 7175
Wire Wire Line
	13525 7075 13125 7075
Wire Wire Line
	14400 6075 14025 6075
Wire Wire Line
	14425 7375 14025 7375
Wire Wire Line
	13525 6075 13125 6075
Wire Wire Line
	14425 7275 14025 7275
Wire Wire Line
	13525 7275 13125 7275
Wire Wire Line
	9250 1875 9350 1875
Wire Wire Line
	9050 3675 9350 3675
Wire Wire Line
	9125 3775 9350 3775
Wire Wire Line
	8775 3325 9350 3325
Wire Wire Line
	8850 3425 9350 3425
Wire Wire Line
	8225 2075 9350 2075
Wire Wire Line
	8300 2175 9350 2175
Wire Wire Line
	15875 6075 15925 6075
Wire Wire Line
	15900 7375 15925 7375
Wire Wire Line
	15900 7275 15925 7275
Wire Wire Line
	15875 5275 16275 5275
Wire Wire Line
	15875 5375 16275 5375
Wire Wire Line
	13525 7375 13125 7375
Text Label 13900 1375 0    60   ~ 0
ALL_CH_5V
Wire Wire Line
	12350 4375 12600 4375
Wire Wire Line
	13525 6175 13125 6175
Wire Wire Line
	13525 6675 13125 6675
Wire Wire Line
	14375 6775 14025 6775
Wire Wire Line
	14375 6675 14025 6675
Wire Wire Line
	14375 6575 14025 6575
$Comp
L Connector_Generic:Conn_02x04_Top_Bottom J?
U 1 1 60F3479F
P 13825 6575
AR Path="/5F745286/60F3479F" Ref="J?"  Part="1" 
AR Path="/5E57930B/60F3479F" Ref="J?"  Part="1" 
AR Path="/60F3479F" Ref="BT3"  Part="1" 
F 0 "BT3" H 13875 6800 50  0000 C CNN
F 1 "Conn_02x04_Top_Bottom" H 13875 6801 50  0001 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5569-08A2_2x04_P4.20mm_Horizontal" H 13825 6575 50  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/039301082_sd.pdf" H 13825 5375 50  0001 C CNN
F 4 "Molex" H 13825 6575 50  0001 C CNN "MFR"
F 5 "0039301082" H 13825 6575 50  0001 C CNN "MPN"
	1    13825 6575
	-1   0    0    -1  
$EndComp
Wire Wire Line
	13525 6775 13125 6775
Wire Wire Line
	13550 7900 13125 7900
Wire Wire Line
	14050 8000 14425 8000
Wire Wire Line
	14425 7900 14050 7900
Wire Wire Line
	14425 7800 14050 7800
$Comp
L Connector_Generic:Conn_02x04_Top_Bottom J?
U 1 1 60F347B0
P 13850 7800
AR Path="/5F745286/60F347B0" Ref="J?"  Part="1" 
AR Path="/5E57930B/60F347B0" Ref="J?"  Part="1" 
AR Path="/60F347B0" Ref="BT5"  Part="1" 
F 0 "BT5" H 13900 8025 50  0000 C CNN
F 1 "Conn_02x04_Top_Bottom" H 13900 8026 50  0001 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5569-08A2_2x04_P4.20mm_Horizontal" H 13850 7800 50  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/039301082_sd.pdf" H 13825 5375 50  0001 C CNN
F 4 "Molex" H 13850 7800 50  0001 C CNN "MFR"
F 5 "0039301082" H 13850 7800 50  0001 C CNN "MPN"
	1    13850 7800
	-1   0    0    -1  
$EndComp
Wire Wire Line
	13550 8000 13125 8000
Wire Wire Line
	15375 5475 14925 5475
Wire Wire Line
	8500 2525 9350 2525
$Comp
L Device:C_Small C?
U 1 1 6138ADCF
P 14450 2225
AR Path="/5B11D17D/6138ADCF" Ref="C?"  Part="1" 
AR Path="/5B13E060/6138ADCF" Ref="C?"  Part="1" 
AR Path="/5E57930B/6138ADCF" Ref="C?"  Part="1" 
AR Path="/5EFBE18D/6138ADCF" Ref="C?"  Part="1" 
AR Path="/5F6C1276/6138ADCF" Ref="C?"  Part="1" 
AR Path="/5F745286/6138ADCF" Ref="C?"  Part="1" 
AR Path="/6138ADCF" Ref="C20"  Part="1" 
F 0 "C20" H 14500 2325 50  0000 L CNN
F 1 "0.1u" H 14500 2125 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 14450 2225 50  0001 C CNN
F 3 "https://api.kemet.com/component-edge/download/datasheet/C0603X104K5RAC7411.pdf" H 14450 2225 50  0001 C CNN
F 4 "Kemet" H 4650 800 50  0001 C CNN "MFR"
F 5 "C0603X104K5RAC7411" H 4650 800 50  0001 C CNN "MPN"
F 6 "-" H 4650 800 50  0001 C CNN "SPR"
F 7 "-" H 4650 800 50  0001 C CNN "SPN"
F 8 "-" H 4650 800 50  0001 C CNN "SPURL"
	1    14450 2225
	1    0    0    -1  
$EndComp
$Comp
L SparkFun-Jumpers:JUMPER-SMT_3_NO JP2
U 1 1 61937FA2
P 3175 5850
F 0 "JP2" H 2625 5850 45  0000 L CNN
F 1 "JUMPER-SMT_3_NO" H 2200 5925 45  0000 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm_NumberLabels" H 3175 6100 20  0001 C CNN
F 3 "" H 3175 5850 60  0001 C CNN
F 4 "XXX-00000" H 3279 5934 60  0001 L CNN "PROD_ID"
	1    3175 5850
	-1   0    0    1   
$EndComp
$Comp
L NCS3S1205SC:NCS3S1205SC U6
U 1 1 610760C4
P 11450 9900
F 0 "U6" H 12450 9313 60  0000 C CNN
F 1 "NCS3S1205SC" H 12450 9419 60  0000 C CNN
F 2 "Footprint:NCS3S1205SC_MUR" H 12450 10140 60  0001 C CNN
F 3 "https://www.murata.com/products/productdata/8807030620190/kdc-ncs3.pdf?1583754812000" H 11450 9900 60  0001 C CNN
F 4 "Murata" H 11450 9900 50  0001 C CNN "MFR"
F 5 "NCS3S1205SC" H 11450 9900 50  0001 C CNN "MPN"
	1    11450 9900
	-1   0    0    1   
$EndComp
Wire Wire Line
	3400 5850 3325 5850
$Comp
L MLP2520S:MLP2520S100MT0S1 L1
U 1 1 6170558F
P 9400 9625
F 0 "L1" V 9753 9581 60  0000 R CNN
F 1 "100uH" V 9647 9581 60  0000 R CNN
F 2 "Footprint:IND_5022_TDK" H 9675 9340 60  0001 C CNN
F 3 "https://product.tdk.com/info/en/catalog/datasheets/inductor_automotive_power_ltf5022-d_en.pdf?ref_disty=digikey" H 9400 9625 60  0001 C CNN
F 4 "LTF5022T-101MR45-D" V 9400 9625 50  0001 C CNN "MPN"
F 5 "TDK Corporation" V 9400 9625 50  0001 C CNN "MFR"
	1    9400 9625
	0    -1   -1   0   
$EndComp
Wire Wire Line
	9400 9000 9400 9025
Wire Wire Line
	2025 9200 3175 9200
Text Label 5550 8100 0    60   ~ 0
SCL
Text Label 5550 8200 0    60   ~ 0
SDA
Text Label 4625 10350 0    60   ~ 0
GND
Text Label 8175 7875 0    60   ~ 0
GND
Text Label 1650 4550 0    60   ~ 0
STC_5V
Text Notes 925  1700 0    60   ~ 0
SOFT START TO PREVENT PIC RESET\nWHEN I2C POWER SWITCHED ON
Wire Wire Line
	14450 1375 14450 2125
Text Label 6000 2675 0    60   ~ 0
SCL
Text Label 6000 2775 0    60   ~ 0
SDA
Text Label 13100 2750 0    60   ~ 0
SCL
Text Label 13100 2850 0    60   ~ 0
SDA
$Comp
L SparkFun-Jumpers:JUMPER-SMT_2_NO JP1
U 1 1 610B034E
P 2725 5475
F 0 "JP1" V 2630 5579 45  0000 L CNN
F 1 "JUMPER-SMT_2_NO" V 2714 5579 45  0001 L CNN
F 2 "Jumper:SolderJumper-2_P1.3mm_Open_RoundedPad1.0x1.5mm" H 2725 5675 20  0001 C CNN
F 3 "" H 2725 5475 60  0001 C CNN
F 4 "XXX-00000" V 2809 5579 60  0001 L CNN "PROD_ID"
	1    2725 5475
	0    -1   1    0   
$EndComp
$Comp
L Connector_Generic:Conn_02x04_Top_Bottom J?
U 1 1 60F3478A
P 13850 8400
AR Path="/5F745286/60F3478A" Ref="J?"  Part="1" 
AR Path="/5E57930B/60F3478A" Ref="J?"  Part="1" 
AR Path="/60F3478A" Ref="BT6"  Part="1" 
F 0 "BT6" H 13900 8625 50  0000 C CNN
F 1 "Conn_02x04_Top_Bottom" H 13900 8626 50  0001 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5569-08A2_2x04_P4.20mm_Horizontal" H 13850 8400 50  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/039301082_sd.pdf" H 13825 5375 50  0001 C CNN
F 4 "Molex" H 13850 8400 50  0001 C CNN "MFR"
F 5 "0039301082" H 13850 8400 50  0001 C CNN "MPN"
	1    13850 8400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	14050 8600 14450 8600
Wire Wire Line
	10775 1550 10775 1375
Connection ~ 10775 1375
Wire Wire Line
	10775 1375 10875 1375
Wire Wire Line
	10775 1750 10775 1825
Wire Wire Line
	11425 1800 11425 3425
Wire Wire Line
	11325 1800 11325 3325
Wire Wire Line
	11050 1800 11050 2225
Wire Wire Line
	11150 1800 11150 2325
Wire Wire Line
	11600 1600 11600 1375
Connection ~ 11600 1375
Wire Wire Line
	11600 1375 11700 1375
Wire Wire Line
	11425 1600 11425 1375
Connection ~ 11425 1375
Wire Wire Line
	11425 1375 11325 1375
Wire Wire Line
	11325 1600 11325 1375
Connection ~ 11325 1375
Wire Wire Line
	11150 1600 11150 1375
Connection ~ 11150 1375
Wire Wire Line
	11050 1600 11050 1375
Connection ~ 11050 1375
Wire Wire Line
	10875 1375 10875 1550
Connection ~ 10875 1375
Wire Wire Line
	10875 1750 10875 1925
Wire Wire Line
	15875 5475 16275 5475
Wire Wire Line
	15875 5575 16275 5575
Wire Wire Line
	10875 1375 11050 1375
Wire Wire Line
	11050 1375 11150 1375
Wire Wire Line
	11150 1375 11325 1375
Wire Wire Line
	11425 1375 11600 1375
Wire Wire Line
	11600 1800 11600 3675
Wire Wire Line
	11700 1800 11700 3775
Wire Wire Line
	4075 10350 4075 9900
Wire Wire Line
	4175 10350 4175 9900
$Comp
L 109633-rescue:PIC18(L)F2520-I_SO-dgi2 U2
U 1 1 5DFF744B
P 4175 8700
F 0 "U2" H 3375 9800 50  0000 C CNN
F 1 "PIC18F27Q10" H 4625 7600 50  0000 C CNN
F 2 "Package_SO:SSOP-28_5.3x10.2mm_P0.65mm" H 4175 8700 50  0001 C CIN
F 3 "https://ww1.microchip.com/downloads/en/DeviceDoc/PIC18F27-47Q10-Data-Sheet-40002043E.pdf" H 4175 8400 60  0001 C CNN
F 4 "Microchip Technology" H -7025 4200 50  0001 C CNN "MFR"
F 5 "PIC18F27Q10T-I/SS" H -7025 4200 50  0001 C CNN "MPN"
F 6 "-" H -7025 4200 50  0001 C CNN "SPR"
F 7 "-" H -7025 4200 50  0001 C CNN "SPN"
F 8 "-" H -7025 4200 50  0001 C CNN "SPURL"
	1    4175 8700
	1    0    0    -1  
$EndComp
Wire Wire Line
	14025 5875 14400 5875
Wire Wire Line
	13100 5975 13525 5975
Wire Wire Line
	14375 6475 14025 6475
Wire Wire Line
	12075 9800 11450 9800
Wire Wire Line
	11450 9700 12075 9700
Wire Wire Line
	12075 9700 12075 9800
Wire Wire Line
	9400 9700 9450 9700
Wire Wire Line
	9400 9625 9400 9700
Wire Wire Line
	6025 6725 5775 6725
Connection ~ 4075 10350
Wire Wire Line
	9125 9475 9125 9800
Wire Wire Line
	9125 9175 9125 9000
$Comp
L SparkFun-Capacitors:2.2UF-1206-50V-10% C19
U 1 1 619B1CAD
P 9125 9275
F 0 "C19" H 9233 9420 45  0000 L CNN
F 1 "4.7UF" H 9233 9336 45  0000 L CNN
F 2 "1206" H 9125 9525 20  0001 C CNN
F 3 "https://product.tdk.com/info/en/catalog/datasheets/mlcc_commercial_general_en.pdf?ref_disty=digikey" H 9125 9275 50  0001 C CNN
F 4 "C3216X5R1H475K160AB" H 9233 9241 60  0001 L CNN "MPN"
F 5 "TDK Corporation" H 9125 9275 50  0001 C CNN "MFR"
	1    9125 9275
	-1   0    0    1   
$EndComp
Wire Wire Line
	2925 10000 7575 10000
Wire Wire Line
	3025 10100 7575 10100
Wire Wire Line
	7575 9800 6550 9800
Wire Wire Line
	14925 5575 15375 5575
Connection ~ 8225 1375
Wire Wire Line
	5600 2775 6575 2775
Wire Wire Line
	1450 7150 1450 9200
Wire Wire Line
	1275 9350 1275 7150
Wire Wire Line
	1275 6950 1275 6725
Wire Wire Line
	1275 6725 1450 6725
Wire Wire Line
	14400 6175 14025 6175
Wire Wire Line
	15875 5975 15925 5975
Wire Wire Line
	14425 7700 14050 7700
Wire Wire Line
	15950 7675 15900 7675
Wire Wire Line
	15900 7775 15950 7775
Wire Wire Line
	15950 7875 15900 7875
Wire Wire Line
	15950 7975 15900 7975
Wire Wire Line
	15900 7075 15925 7075
Wire Wire Line
	15925 7175 15900 7175
Wire Wire Line
	15925 6475 15900 6475
Wire Wire Line
	15925 6575 15900 6575
Wire Wire Line
	15400 8575 14950 8575
Wire Wire Line
	15400 7975 14950 7975
Wire Wire Line
	15400 7375 14925 7375
Wire Wire Line
	15400 6775 14925 6775
Wire Wire Line
	15400 6475 14875 6475
Wire Wire Line
	13125 8600 13550 8600
Wire Wire Line
	14925 7175 15400 7175
Wire Wire Line
	15400 7275 14925 7275
Wire Wire Line
	14950 7875 15400 7875
Wire Wire Line
	15400 7775 14900 7775
Wire Wire Line
	15400 7675 14900 7675
Wire Wire Line
	15400 8375 14900 8375
Wire Wire Line
	9125 9800 9450 9800
Wire Wire Line
	13125 8500 13550 8500
Wire Wire Line
	14925 7075 15400 7075
Wire Wire Line
	14875 6575 15400 6575
Wire Wire Line
	14925 6675 15400 6675
Wire Wire Line
	15400 8275 14900 8275
Wire Wire Line
	14950 8475 15400 8475
Text Label 14075 7075 0    60   ~ 0
SHIELD
Text Label 13125 7375 0    60   ~ 0
#4_CHG-
Text Label 14075 7275 0    60   ~ 0
#4_SDA
Text Label 14075 7375 0    60   ~ 0
#4_CLK
Text Label 13125 6675 0    60   ~ 0
#3_CHG+
Text Label 13125 6775 0    60   ~ 0
#3_CHG-
Text Label 14025 6675 0    60   ~ 0
#3_SDA
Text Label 14025 6775 0    60   ~ 0
#3_CLK
Text Label 13150 6075 0    60   ~ 0
#2_CHG+
Text Label 13150 6175 0    60   ~ 0
#2_CHG-
Text Label 14050 6075 0    60   ~ 0
#2_SDA
Text Label 14050 6175 0    60   ~ 0
#2_CLK
Text Label 13175 5475 0    60   ~ 0
#1_CHG+
Text Label 13175 5575 0    60   ~ 0
#1_CHG-
Text Label 14075 5475 0    60   ~ 0
#1_SDA
Text Label 14075 5575 0    60   ~ 0
#1_CLK
Wire Wire Line
	9125 9000 9400 9000
Text Label 14075 7700 0    60   ~ 0
SHIELD
Text Label 13125 7800 0    60   ~ 0
BATT-
Text Label 14075 7900 0    60   ~ 0
#5_SDA
Text Label 14075 8000 0    60   ~ 0
#5_CLK
Wire Wire Line
	14450 8300 14050 8300
Wire Wire Line
	14450 8400 14050 8400
Wire Wire Line
	14450 8500 14050 8500
Text Label 14100 8300 0    60   ~ 0
SHIELD
Text Label 14100 8400 0    60   ~ 0
GND
Text Label 14100 8500 0    60   ~ 0
#6_SDA
Text Label 14100 8600 0    60   ~ 0
#6_CLK
Text Label 14950 6175 0    60   ~ 0
#8_CHG-
Text Label 14950 6075 0    60   ~ 0
#8_CHG+
Wire Wire Line
	15900 6675 15925 6675
Wire Wire Line
	15900 6775 15925 6775
Wire Wire Line
	5600 2675 6500 2675
Wire Wire Line
	6375 1900 6375 2475
Wire Wire Line
	5600 2475 6375 2475
Text Label 15925 6075 0    60   ~ 0
#8_SDA
Text Label 14925 5475 0    60   ~ 0
#7_CHG+
Text Label 14925 5575 0    60   ~ 0
#7_CHG-
Text Label 15925 5975 0    60   ~ 0
GND
Text Label 15875 5475 0    60   ~ 0
#7_SDA
Text Label 15875 5575 0    60   ~ 0
#7_CLK
Text Label 15000 6675 0    60   ~ 0
#9_CHG+
Text Label 15000 6775 0    60   ~ 0
#9_CHG-
Text Label 15925 6675 0    60   ~ 0
#9_SDA
Text Label 15925 6775 0    60   ~ 0
#9_CLK
Text Label 14950 7275 0    60   ~ 0
#10_CHG+
Text Label 14950 7375 0    60   ~ 0
#10_CHG-
Text Label 15925 7275 0    60   ~ 0
#10_SDA
Text Label 15925 7375 0    60   ~ 0
#10_CLK
Text Label 14950 7875 0    60   ~ 0
#11_CHG+
Text Label 14950 7975 0    60   ~ 0
#11_CHG-
Text Label 15950 7875 0    60   ~ 0
#11_SDA
Text Label 15950 7975 0    60   ~ 0
#11_CLK
Text Label 14950 8475 0    60   ~ 0
#12_CHG+
Text Label 14950 8575 0    60   ~ 0
#12_CHG-
Text Label 15950 8475 0    60   ~ 0
#12_SDA
Text Label 15950 8575 0    60   ~ 0
#12_CLK
Wire Wire Line
	12075 9900 11450 9900
Wire Wire Line
	8675 5675 10850 5675
Wire Wire Line
	2325 10350 4075 10350
Wire Wire Line
	6550 6725 6550 9800
Wire Wire Line
	7575 9700 6725 9700
Wire Wire Line
	6725 9700 6725 8800
Wire Wire Line
	6725 8800 6625 8800
Connection ~ 9125 9800
Wire Wire Line
	3175 6050 3175 6275
Wire Wire Line
	3175 4550 3175 5300
$Comp
L Connector_Generic:Conn_01x05 J3
U 1 1 615153BB
P 1250 6400
F 0 "J3" H 1250 5975 50  0000 C CNN
F 1 "Charge_Ctrl" H 1250 6075 50  0000 C CNN
F 2 "Connector_Molex:Molex_Micro-Fit_3.0_43650-0500_1x05_P3.00mm_Horizontal" H 1250 6400 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/molex/43650-0500/www.molex.com/webdocs/datasheets/pdf/en-us/0436500500_PCB_HEADERS.pdf" H 1250 6400 50  0001 C CNN
F 4 "Molex" H 1250 6400 50  0001 C CNN "MFR"
F 5 "43650-0500" H 1250 6400 50  0001 C CNN "MPN"
	1    1250 6400
	-1   0    0    1   
$EndComp
Wire Wire Line
	3400 6500 3400 5850
Wire Wire Line
	1450 6600 3175 6600
Wire Wire Line
	1450 6300 2950 6300
Wire Wire Line
	2950 6300 2950 4750
Wire Wire Line
	1450 6400 3025 6400
Wire Wire Line
	3025 6400 3025 4650
Text Notes 1000 8775 1    60   ~ 0
When charging:\nDisconnect Profiler endcap from J1A\nConnect J1B and join (J1B-1 and J1B-2)
Text Notes 1175 6600 2    60   ~ 0
GND\nSTC TX\nSTC RX\n5V to PCB\n5V from PCB
Wire Wire Line
	1450 6500 3400 6500
Text Label 15875 5275 0    60   ~ 0
SHIELD
Text Label 15875 5375 0    60   ~ 0
GND
Text Label 14925 5275 0    60   ~ 0
STC_BATT+
Text Label 14925 5375 0    60   ~ 0
BATT-
Wire Wire Line
	1975 1375 2850 1375
Text Label 14950 5875 0    60   ~ 0
STC_BATT+
Text Label 14950 5975 0    60   ~ 0
BATT-
Text Label 15925 5875 0    60   ~ 0
SHIELD
Text Label 15925 6175 0    60   ~ 0
#8_CLK
Text Label 13150 8500 0    60   ~ 0
#6_CHG+
Text Label 13150 8600 0    60   ~ 0
#6_CHG-
Text Label 13150 8400 0    60   ~ 0
BATT-
Text Label 13150 8300 0    60   ~ 0
RTE_BATT+
Wire Wire Line
	14950 5875 15375 5875
Wire Wire Line
	14950 5975 15375 5975
Text Label 13125 7900 0    60   ~ 0
#5_CHG+
Text Label 13125 8000 0    60   ~ 0
#5_CHG-
Text Label 13125 7700 0    60   ~ 0
RTE_BATT+
Text Label 14075 7800 0    60   ~ 0
GND
$Comp
L Connector_Generic:Conn_02x04_Top_Bottom J?
U 1 1 60F34798
P 13825 7175
AR Path="/5F745286/60F34798" Ref="J?"  Part="1" 
AR Path="/5E57930B/60F34798" Ref="J?"  Part="1" 
AR Path="/60F34798" Ref="BT4"  Part="1" 
F 0 "BT4" H 13875 7400 50  0000 C CNN
F 1 "Conn_02x04_Top_Bottom" H 13875 7401 50  0001 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5569-08A2_2x04_P4.20mm_Horizontal" H 13825 7175 50  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/039301082_sd.pdf" H 13825 5375 50  0001 C CNN
F 4 "Molex" H 13825 7175 50  0001 C CNN "MFR"
F 5 "0039301082" H 13825 7175 50  0001 C CNN "MPN"
	1    13825 7175
	-1   0    0    -1  
$EndComp
Text Label 13125 7075 0    60   ~ 0
STC_BATT+
Text Label 13125 7175 0    60   ~ 0
BATT-
Text Label 13125 7275 0    60   ~ 0
#4_CHG+
Text Label 14075 7175 0    60   ~ 0
GND
Text Label 13125 6475 0    60   ~ 0
STC_BATT+
Text Label 13125 6575 0    60   ~ 0
BATT-
Text Label 14025 6475 0    60   ~ 0
SHIELD
Text Label 14025 6575 0    60   ~ 0
GND
$Comp
L Connector_Generic:Conn_01x12 J1
U 1 1 61106EF5
P 1250 4650
F 0 "J1" H 1625 5325 50  0000 L CNN
F 1 "Profiler Interconnect" H 1200 5250 50  0000 L CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5569-12A2_2x06_P4.20mm_Horizontal" H 1250 4650 50  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/039301120_sd.pdf" H 1250 4650 50  0001 C CNN
F 4 "Molex" H 1250 4650 50  0001 C CNN "MFR"
F 5 "0039301120" H 1250 4650 50  0001 C CNN "MPN"
	1    1250 4650
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1450 6200 2875 6200
Wire Wire Line
	2875 4850 2875 6200
Wire Wire Line
	2575 4150 1450 4150
Wire Wire Line
	2650 4250 1450 4250
Wire Wire Line
	2725 4350 1450 4350
Wire Wire Line
	2800 4450 1450 4450
Wire Wire Line
	3175 4550 1450 4550
Wire Wire Line
	3025 4650 1450 4650
Wire Wire Line
	2950 4750 1450 4750
Wire Wire Line
	2875 4850 1450 4850
Text Label 15925 6475 0    60   ~ 0
SHIELD
Text Label 15925 6575 0    60   ~ 0
GND
Wire Wire Line
	5600 2075 6000 2075
Wire Wire Line
	5600 2175 6100 2175
Text Label 13100 5875 0    60   ~ 0
STC_BATT+
Text Label 13100 5975 0    60   ~ 0
BATT-
Text Label 14050 5875 0    60   ~ 0
SHIELD
Text Label 14050 5975 0    60   ~ 0
GND
Text Label 14075 5275 0    60   ~ 0
SHIELD
Text Label 14075 5375 0    60   ~ 0
GND
Text Label 13125 5275 0    60   ~ 0
STC_BATT+
Text Label 13125 5375 0    60   ~ 0
BATT-
Text Label 14975 6575 0    60   ~ 0
BATT-
Text Label 15925 7075 0    60   ~ 0
SHIELD
Text Label 15925 7175 0    60   ~ 0
GND
Text Label 14925 7075 0    60   ~ 0
STC_BATT+
Text Label 14925 7175 0    60   ~ 0
BATT-
Text Label 14900 7675 0    60   ~ 0
STC_BATT+
Text Label 14900 7775 0    60   ~ 0
BATT-
Text Label 15950 7675 0    60   ~ 0
SHIELD
Text Label 15950 7775 0    60   ~ 0
GND
Text Label 14900 8275 0    60   ~ 0
STC_BATT+
Text Label 14900 8375 0    60   ~ 0
BATT-
Text Label 15950 8275 0    60   ~ 0
SHIELD
Text Label 15950 8375 0    60   ~ 0
GND
Wire Wire Line
	13150 8300 13550 8300
Wire Wire Line
	13150 8400 13550 8400
Wire Wire Line
	13125 7700 13550 7700
Wire Wire Line
	13125 7800 13550 7800
$Comp
L Connector_Generic:Conn_01x02 J4
U 1 1 614DBEC7
P 14725 5475
F 0 "J4" H 14643 5608 60  0000 C CNN
F 1 "460150209" H 14643 5608 60  0001 C CNN
F 2 "Footprint:460150209" H 15125 5415 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/460150209_sd.pdf" H 14725 5475 60  0001 C CNN
F 4 "Molex" H 14725 5475 50  0001 C CNN "MFR"
F 5 "460150209" H 14725 5475 50  0001 C CNN "MPN"
	1    14725 5475
	-1   0    0    -1  
$EndComp
Wire Wire Line
	14925 6175 15375 6175
Wire Wire Line
	15375 6075 14925 6075
$Comp
L Connector_Generic:Conn_01x02 J6
U 1 1 61EE5E18
P 14725 6075
F 0 "J6" H 14643 6208 60  0000 C CNN
F 1 "460150209" H 14643 6208 60  0001 C CNN
F 2 "Footprint:460150209" H 15125 6015 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/460150209_sd.pdf" H 14725 6075 60  0001 C CNN
F 4 "Molex" H 12925 8500 50  0001 C CNN "MFR"
F 5 "460150209" H 12925 8500 50  0001 C CNN "MPN"
	1    14725 6075
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J8
U 1 1 61EE647D
P 12925 8500
F 0 "J8" H 12843 8633 60  0000 C CNN
F 1 "460150209" H 12843 8633 60  0001 C CNN
F 2 "Footprint:460150209" H 13325 8440 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/460150209_sd.pdf" H 12925 8500 60  0001 C CNN
F 4 "Molex" H 12925 8500 50  0001 C CNN "MFR"
F 5 "460150209" H 12925 8500 50  0001 C CNN "MPN"
	1    12925 8500
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J10
U 1 1 61EE6A60
P 12925 7900
F 0 "J10" H 12843 8033 60  0000 C CNN
F 1 "460150209" H 12843 8033 60  0001 C CNN
F 2 "Footprint:460150209" H 13325 7840 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/460150209_sd.pdf" H 12925 7900 60  0001 C CNN
F 4 "Molex" H 12925 8500 50  0001 C CNN "MFR"
F 5 "460150209" H 12925 8500 50  0001 C CNN "MPN"
	1    12925 7900
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J11
U 1 1 61EE768D
P 12925 7275
F 0 "J11" H 12843 7408 60  0000 C CNN
F 1 "460150209" H 12843 7408 60  0001 C CNN
F 2 "Footprint:460150209" H 13325 7215 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/460150209_sd.pdf" H 12925 7275 60  0001 C CNN
F 4 "Molex" H 12925 8500 50  0001 C CNN "MFR"
F 5 "460150209" H 12925 8500 50  0001 C CNN "MPN"
	1    12925 7275
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J13
U 1 1 61EE7C25
P 12925 6675
F 0 "J13" H 12843 6808 60  0000 C CNN
F 1 "460150209" H 12843 6808 60  0001 C CNN
F 2 "Footprint:460150209" H 13325 6615 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/460150209_sd.pdf" H 12925 6675 60  0001 C CNN
F 4 "Molex" H 12925 8500 50  0001 C CNN "MFR"
F 5 "460150209" H 12925 8500 50  0001 C CNN "MPN"
	1    12925 6675
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J14
U 1 1 61EE83B1
P 12925 6075
F 0 "J14" H 12843 6208 60  0000 C CNN
F 1 "460150209" H 12843 6208 60  0001 C CNN
F 2 "Footprint:460150209" H 13325 6015 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/460150209_sd.pdf" H 12925 6075 60  0001 C CNN
F 4 "Molex" H 12925 8500 50  0001 C CNN "MFR"
F 5 "460150209" H 12925 8500 50  0001 C CNN "MPN"
	1    12925 6075
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J9
U 1 1 61EE9206
P 14750 7875
F 0 "J9" H 14668 8008 60  0000 C CNN
F 1 "460150209" H 14668 8008 60  0001 C CNN
F 2 "Footprint:460150209" H 15150 7815 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/460150209_sd.pdf" H 14750 7875 60  0001 C CNN
F 4 "Molex" H 12925 8500 50  0001 C CNN "MFR"
F 5 "460150209" H 12925 8500 50  0001 C CNN "MPN"
	1    14750 7875
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J7
U 1 1 61EE9681
P 14725 7275
F 0 "J7" H 14643 7408 60  0000 C CNN
F 1 "460150209" H 14643 7408 60  0001 C CNN
F 2 "Footprint:460150209" H 15125 7215 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/460150209_sd.pdf" H 14725 7275 60  0001 C CNN
F 4 "Molex" H 12925 8500 50  0001 C CNN "MFR"
F 5 "460150209" H 12925 8500 50  0001 C CNN "MPN"
	1    14725 7275
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J5
U 1 1 61EE9CE6
P 14725 6675
F 0 "J5" H 14643 6808 60  0000 C CNN
F 1 "460150209" H 14643 6808 60  0001 C CNN
F 2 "Footprint:460150209" H 15125 6615 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/460150209_sd.pdf" H 14725 6675 60  0001 C CNN
F 4 "Molex" H 12925 8500 50  0001 C CNN "MFR"
F 5 "460150209" H 12925 8500 50  0001 C CNN "MPN"
	1    14725 6675
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J15
U 1 1 61EEA346
P 12925 5475
F 0 "J15" H 12843 5608 60  0000 C CNN
F 1 "460150209" H 12843 5608 60  0001 C CNN
F 2 "Footprint:460150209" H 13325 5415 60  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/460150209_sd.pdf" H 12925 5475 60  0001 C CNN
F 4 "Molex" H 12925 8500 50  0001 C CNN "MFR"
F 5 "460150209" H 12925 8500 50  0001 C CNN "MPN"
	1    12925 5475
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2075 4950 1450 4950
Wire Wire Line
	2075 4950 2075 5050
Wire Wire Line
	1450 5050 2075 5050
Wire Wire Line
	2725 5150 1450 5150
Wire Wire Line
	2725 5150 2725 5250
Wire Wire Line
	2725 5250 1450 5250
Connection ~ 2725 5250
Wire Wire Line
	13525 5575 13125 5575
Wire Wire Line
	13525 5475 13125 5475
Wire Wire Line
	14425 5575 14025 5575
Wire Wire Line
	14425 5475 14025 5475
Wire Wire Line
	14425 5375 14025 5375
Wire Wire Line
	14425 5275 14025 5275
$Comp
L Connector_Generic:Conn_02x04_Top_Bottom J?
U 1 1 60F347A9
P 13825 5375
AR Path="/5F745286/60F347A9" Ref="J?"  Part="1" 
AR Path="/5E57930B/60F347A9" Ref="J?"  Part="1" 
AR Path="/60F347A9" Ref="BT1"  Part="1" 
F 0 "BT1" H 13875 5600 50  0000 C CNN
F 1 "5569-08A2" H 13875 5601 50  0001 C CNN
F 2 "Connector_Molex:Molex_Mini-Fit_Jr_5569-08A2_2x04_P4.20mm_Horizontal" H 13825 5375 50  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/039301082_sd.pdf" H 13825 5375 50  0001 C CNN
F 4 "Molex" H 13825 5375 50  0001 C CNN "MFR"
F 5 "0039301082" H 13825 5375 50  0001 C CNN "MPN"
	1    13825 5375
	-1   0    0    -1  
$EndComp
Wire Wire Line
	15375 5275 14925 5275
Wire Wire Line
	15375 5375 14925 5375
Wire Wire Line
	13125 6575 13525 6575
Wire Wire Line
	13125 5375 13525 5375
Wire Wire Line
	2575 4150 2575 3650
Wire Wire Line
	2575 3650 1450 3650
Wire Wire Line
	1450 3550 2650 3550
Wire Wire Line
	2650 3550 2650 4250
Wire Wire Line
	1450 3750 2725 3750
Wire Wire Line
	2725 3750 2725 4350
Wire Wire Line
	1450 3850 2800 3850
Wire Wire Line
	2800 3850 2800 4450
Wire Wire Line
	13125 6475 13525 6475
Wire Wire Line
	13525 5275 13125 5275
Wire Wire Line
	12225 2850 13325 2850
Wire Wire Line
	12350 1375 11700 1375
Connection ~ 12350 1375
Connection ~ 11700 1375
Connection ~ 12350 4375
Wire Wire Line
	6250 2375 5600 2375
Wire Wire Line
	13400 2750 13400 825 
Wire Wire Line
	13400 825  6500 825 
Wire Wire Line
	6500 825  6500 2675
Connection ~ 13400 2750
Wire Wire Line
	13325 975  6575 975 
Wire Wire Line
	6575 975  6575 2775
Connection ~ 13325 2850
Connection ~ 5000 4375
Wire Wire Line
	5000 3375 5000 4375
Wire Wire Line
	4200 1375 4200 2025
Connection ~ 4200 1375
Wire Wire Line
	4200 1375 5000 1375
Connection ~ 3475 1375
Wire Wire Line
	3475 1375 4200 1375
Wire Wire Line
	3475 1375 3475 1750
Wire Wire Line
	3400 1375 3400 1750
Connection ~ 3400 1375
Wire Wire Line
	3400 1375 3475 1375
Wire Wire Line
	3250 1375 3250 1750
Connection ~ 3250 1375
Wire Wire Line
	3250 1375 3400 1375
Wire Wire Line
	3175 1375 3175 1750
Connection ~ 3175 1375
Wire Wire Line
	3175 1375 3250 1375
Connection ~ 5000 1375
Wire Wire Line
	15900 8375 15950 8375
Wire Wire Line
	15900 8275 15950 8275
Wire Wire Line
	15900 8475 15950 8475
Wire Wire Line
	15900 8575 15950 8575
Wire Wire Line
	8800 9800 9125 9800
Text Label 14975 6475 0    60   ~ 0
STC_BATT+
Wire Wire Line
	2725 5250 2725 5325
Wire Wire Line
	1450 5525 2075 5525
Wire Wire Line
	2075 5525 2075 5050
Connection ~ 2075 5050
Text Label 1725 4950 0    60   ~ 0
BATT-
Text Label 2175 5625 0    60   ~ 0
RTE_BATT+
Text Label 2175 5150 0    60   ~ 0
STC_BATT+
Wire Wire Line
	1450 5625 2725 5625
Text Label 1475 6600 0    60   ~ 0
ISO_5V_OUT
Text Label 1475 6500 0    60   ~ 0
5V
Text Label 5975 8400 0    60   ~ 0
TTL_TXD
Text Label 5975 8500 0    60   ~ 0
TTL_RXD
Wire Wire Line
	11350 7625 11350 8500
Wire Wire Line
	5175 8500 11350 8500
Connection ~ 11350 7625
Wire Wire Line
	11200 7525 11200 8400
Connection ~ 11200 7525
Wire Wire Line
	5175 8400 11200 8400
Text Label 1525 3550 0    60   ~ 0
LEAK-1
Text Label 1525 3650 0    60   ~ 0
LEAK-1_RTN
Text Label 1525 3750 0    60   ~ 0
LEAK-2
Text Label 1525 3850 0    60   ~ 0
LEAK-2_RTN
Text Label 1475 6300 0    60   ~ 0
BATT_TXD
Text Label 1475 6400 0    60   ~ 0
BATT_RXD
Text Label 3650 6725 0    60   ~ 0
5V
Text Label 6875 9700 0    60   ~ 0
MCLR\
Text Label 6875 9800 0    60   ~ 0
5V
Text Label 6875 9900 0    60   ~ 0
GND
Text Label 6875 10100 0    60   ~ 0
PGD
Text Label 6875 10000 0    60   ~ 0
PGC
$Comp
L SparkFun-Switches:MOMENTARY-SWITCH-SPST-2-SMD-4.6X2.8MM RESET1
U 1 1 62542CD1
P 6275 9150
F 0 "RESET1" V 6275 9353 45  0000 L CNN
F 1 "MOMENTARY-SWITCH-SPST-2-SMD-4.6X2.8MM" V 6264 9353 45  0001 L CNN
F 2 "Footprint:KSC401J 50SH LFS" H 6275 9400 20  0001 C CNN
F 3 "https://www.ckswitches.com/media/1970/ksc4.pdf" H 6275 9150 50  0001 C CNN
F 4 "SWCH-13065" V 6359 9353 60  0001 L CNN "Field4"
F 5 "C&K" V 6275 9150 50  0001 C CNN "MFR"
F 6 "KSC401J 50SH LFS" V 6275 9150 50  0001 C CNN "MPN"
	1    6275 9150
	0    -1   1    0   
$EndComp
Wire Wire Line
	6275 8950 6275 8800
Wire Wire Line
	6375 9350 6375 9900
Wire Wire Line
	6275 9350 6275 9900
Connection ~ 6275 8800
Text Label 9175 5675 0    60   ~ 0
5V
Wire Wire Line
	8725 7525 7750 7525
Wire Wire Line
	7750 7525 7750 4650
Wire Wire Line
	7750 4650 3025 4650
Connection ~ 3025 4650
Text Label 1475 6200 0    60   ~ 0
GND
Wire Wire Line
	8725 7625 7675 7625
Wire Wire Line
	7675 7625 7675 4750
Wire Wire Line
	7675 4750 2950 4750
Connection ~ 2950 4750
Text Label 3600 2225 0    60   ~ 0
#9_CLK
Text Label 3600 2325 0    60   ~ 0
#9_SDA
Text Label 3625 2925 0    60   ~ 0
#10_CLK
Text Label 3625 3025 0    60   ~ 0
#10_SDA
Connection ~ 6250 1375
Wire Wire Line
	6250 1375 6375 1375
Connection ~ 6375 1375
Wire Wire Line
	6375 1375 8100 1375
Text Label 5600 2075 0    60   ~ 0
#11_CLK
Connection ~ 6000 1375
Connection ~ 6100 1375
Wire Wire Line
	6100 1375 6250 1375
Wire Wire Line
	5000 1375 6000 1375
Wire Wire Line
	6000 1375 6100 1375
Text Label 5600 2175 0    60   ~ 0
#11_SDA
Text Label 5650 2375 0    60   ~ 0
#12_CLK
Text Label 5650 2475 0    60   ~ 0
#12_SDA
Wire Wire Line
	15925 6175 15875 6175
Wire Wire Line
	15875 5875 15925 5875
$Comp
L SparkFun-Jumpers:JUMPER-SMT_3_NO JP5
U 1 1 6353B331
P 2850 2475
F 0 "JP5" H 2650 2600 45  0000 L CNN
F 1 "JUMPER-SMT_3_NO" H 1875 2550 45  0001 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm_NumberLabels" H 2850 2725 20  0001 C CNN
F 3 "" H 2850 2475 60  0001 C CNN
F 4 "XXX-00000" H 2954 2559 60  0001 L CNN "PROD_ID"
	1    2850 2475
	-1   0    0    1   
$EndComp
$Comp
L SparkFun-Jumpers:JUMPER-SMT_3_NO JP4
U 1 1 6353C2E4
P 2650 2750
F 0 "JP4" H 2450 2900 45  0000 L CNN
F 1 "JUMPER-SMT_3_NO" H 1675 2825 45  0001 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm_NumberLabels" H 2650 3000 20  0001 C CNN
F 3 "" H 2650 2750 60  0001 C CNN
F 4 "XXX-00000" H 2754 2834 60  0001 L CNN "PROD_ID"
	1    2650 2750
	-1   0    0    1   
$EndComp
$Comp
L SparkFun-Jumpers:JUMPER-SMT_3_NO JP3
U 1 1 6353CAA9
P 2475 3050
F 0 "JP3" H 2300 3225 45  0000 L CNN
F 1 "JUMPER-SMT_3_NO" H 1500 3125 45  0001 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm_NumberLabels" H 2475 3300 20  0001 C CNN
F 3 "" H 2475 3050 60  0001 C CNN
F 4 "XXX-00000" H 2579 3134 60  0001 L CNN "PROD_ID"
	1    2475 3050
	-1   0    0    1   
$EndComp
Wire Wire Line
	2475 3250 3025 3250
Wire Wire Line
	3025 3250 3025 4375
Wire Wire Line
	2650 2950 3025 2950
Wire Wire Line
	3025 2950 3025 3250
Connection ~ 3025 3250
Wire Wire Line
	2850 2675 3025 2675
Wire Wire Line
	3025 2675 3025 2950
Connection ~ 3025 2950
Wire Wire Line
	2850 2275 2850 2125
Connection ~ 2850 1375
Wire Wire Line
	2850 1375 3175 1375
Wire Wire Line
	2475 2850 2475 2125
Wire Wire Line
	2475 2125 2650 2125
Connection ~ 2850 2125
Wire Wire Line
	2850 2125 2850 1375
Wire Wire Line
	2650 2550 2650 2125
Connection ~ 2650 2125
Wire Wire Line
	2650 2125 2850 2125
Wire Wire Line
	3025 4375 5000 4375
Wire Wire Line
	3350 2675 3350 3050
Wire Wire Line
	3350 3050 2625 3050
Wire Wire Line
	3350 2675 4300 2675
Wire Wire Line
	3275 2575 3275 2750
Wire Wire Line
	3275 2750 2800 2750
Wire Wire Line
	3275 2575 4300 2575
Text Label 11275 4375 0    60   ~ 0
GND
$Comp
L SparkFun-Jumpers:JUMPER-SMT_3_NO JP8
U 1 1 6380BD47
P 8300 2825
F 0 "JP8" H 8100 2950 45  0000 L CNN
F 1 "JUMPER-SMT_3_NO" H 7325 2900 45  0001 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm_NumberLabels" H 8300 3075 20  0001 C CNN
F 3 "" H 8300 2825 60  0001 C CNN
F 4 "XXX-00000" H 8404 2909 60  0001 L CNN "PROD_ID"
	1    8300 2825
	-1   0    0    1   
$EndComp
$Comp
L SparkFun-Jumpers:JUMPER-SMT_3_NO JP7
U 1 1 6380CA26
P 8100 3150
F 0 "JP7" H 7900 3275 45  0000 L CNN
F 1 "JUMPER-SMT_3_NO" H 7125 3225 45  0001 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm_NumberLabels" H 8100 3400 20  0001 C CNN
F 3 "" H 8100 3150 60  0001 C CNN
F 4 "XXX-00000" H 8204 3234 60  0001 L CNN "PROD_ID"
	1    8100 3150
	-1   0    0    1   
$EndComp
$Comp
L SparkFun-Jumpers:JUMPER-SMT_3_NO JP6
U 1 1 6380CF27
P 7900 3475
F 0 "JP6" H 7700 3600 45  0000 L CNN
F 1 "JUMPER-SMT_3_NO" H 6925 3550 45  0001 L CNN
F 2 "Jumper:SolderJumper-3_P1.3mm_Open_RoundedPad1.0x1.5mm_NumberLabels" H 7900 3725 20  0001 C CNN
F 3 "" H 7900 3475 60  0001 C CNN
F 4 "XXX-00000" H 8004 3559 60  0001 L CNN "PROD_ID"
	1    7900 3475
	-1   0    0    1   
$EndComp
Wire Wire Line
	7900 3675 7900 4375
Connection ~ 7900 4375
Wire Wire Line
	7900 4375 5000 4375
Wire Wire Line
	8100 3350 8100 4375
Connection ~ 8100 4375
Wire Wire Line
	8100 4375 7900 4375
Wire Wire Line
	8300 3025 8300 4375
Connection ~ 8300 4375
Wire Wire Line
	8300 4375 8100 4375
Wire Wire Line
	8300 2625 8100 2625
Wire Wire Line
	7900 2625 7900 3275
Wire Wire Line
	8100 2950 8100 2625
Connection ~ 8100 2625
Wire Wire Line
	8100 2625 7900 2625
Wire Wire Line
	8100 2625 8100 1375
Connection ~ 8100 1375
Wire Wire Line
	8100 1375 8225 1375
Wire Wire Line
	8300 4375 9900 4375
Wire Wire Line
	9350 2875 8450 2875
Wire Wire Line
	9350 2975 8600 2975
Wire Wire Line
	8600 2975 8600 3150
Wire Wire Line
	8600 3150 8250 3150
Wire Wire Line
	9350 3075 8700 3075
Wire Wire Line
	8700 3075 8700 3475
Wire Wire Line
	8700 3475 8050 3475
Wire Wire Line
	1775 1375 1300 1375
Text Label 1300 1375 0    60   ~ 0
5V
Text Label 11500 9700 0    60   ~ 0
STC_BATT+
Text Label 11500 9900 0    60   ~ 0
BATT-
Text Label 8800 9800 0    60   ~ 0
GND
Text Label 9175 9000 0    60   ~ 0
ISO_5V_OUT
Wire Wire Line
	8575 6825 8575 6625
Wire Wire Line
	8575 6625 8050 6625
Wire Wire Line
	8575 6825 8725 6825
Wire Wire Line
	8050 6625 8050 6750
Wire Wire Line
	8600 7325 8600 7150
Wire Wire Line
	8600 7150 8050 7150
Wire Wire Line
	8600 7325 8725 7325
Wire Wire Line
	8050 7150 8050 7275
Wire Wire Line
	1450 6950 1450 6725
Connection ~ 1450 6725
Wire Wire Line
	1450 6725 1975 6725
Connection ~ 6025 6725
Connection ~ 5775 6725
Wire Wire Line
	5775 6725 5425 6725
Wire Wire Line
	6025 6725 6175 6725
Wire Wire Line
	6025 7100 6025 8200
$Comp
L 109633-rescue:Rsmall-dgi2 R32
U 1 1 61A3E11E
P 6375 7000
F 0 "R32" V 5900 7000 50  0000 C CNN
F 1 "10K" V 6000 7000 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 6375 7000 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 6375 7000 60  0001 C CNN
F 4 "Susumu" H -3250 4425 50  0001 C CNN "MFR"
F 5 "RR0816P-103-D" H -3250 4425 50  0001 C CNN "MPN"
F 6 "-" H -3250 4425 50  0001 C CNN "SPR"
F 7 "-" H -3250 4425 50  0001 C CNN "SPN"
F 8 "-" H -3250 4425 50  0001 C CNN "SPURL"
	1    6375 7000
	0    -1   1    0   
$EndComp
Wire Wire Line
	6375 6900 6375 6725
Connection ~ 6375 6725
Wire Wire Line
	6375 6725 6550 6725
Wire Wire Line
	6375 7100 6375 8800
Connection ~ 6375 8800
Wire Wire Line
	6375 8800 6275 8800
Wire Wire Line
	5175 8800 6275 8800
Wire Wire Line
	5775 9900 6275 9900
Connection ~ 6375 9900
Wire Wire Line
	6375 9900 7575 9900
Connection ~ 6275 9900
Wire Wire Line
	6275 9900 6375 9900
Wire Wire Line
	6375 8800 6375 8950
Wire Wire Line
	5425 7250 5425 10350
Connection ~ 5425 10350
Wire Wire Line
	5425 10350 5500 10350
$Comp
L Connector_Generic:Conn_01x03 J17
U 1 1 61FE1E0E
P 1775 7375
F 0 "J17" H 1693 7600 50  0000 C CNN
F 1 "43650-0300" H 1693 7601 50  0001 C CNN
F 2 "Connector_Molex:Molex_Micro-Fit_3.0_43650-0300_1x03_P3.00mm_Horizontal" H 1775 7375 50  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/436500300_sd.pdf" H 1775 7375 50  0001 C CNN
F 4 "Molex" H 1775 7375 50  0001 C CNN "MFR"
F 5 "43650-0300" H 1775 7375 50  0001 C CNN "MPN"
	1    1775 7375
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J18
U 1 1 620297E3
P 2125 7375
F 0 "J18" H 2043 7600 50  0000 C CNN
F 1 "43650-0300" H 2043 7601 50  0001 C CNN
F 2 "Connector_Molex:Molex_Micro-Fit_3.0_43650-0300_1x03_P3.00mm_Horizontal" H 2125 7375 50  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/436500300_sd.pdf" H 2125 7375 50  0001 C CNN
F 4 "Molex" H 2125 7375 50  0001 C CNN "MFR"
F 5 "43650-0300" H 2125 7375 50  0001 C CNN "MPN"
	1    2125 7375
	-1   0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J20
U 1 1 62029BB0
P 2500 7375
F 0 "J20" H 2418 7600 50  0000 C CNN
F 1 "43650-0300" H 2418 7601 50  0001 C CNN
F 2 "Connector_Molex:Molex_Micro-Fit_3.0_43650-0300_1x03_P3.00mm_Horizontal" H 2500 7375 50  0001 C CNN
F 3 "https://www.molex.com/pdm_docs/sd/436500300_sd.pdf" H 2500 7375 50  0001 C CNN
F 4 "Molex" H 2500 7375 50  0001 C CNN "MFR"
F 5 "43650-0300" H 2500 7375 50  0001 C CNN "MPN"
	1    2500 7375
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2700 7275 2700 6725
Connection ~ 2700 6725
Wire Wire Line
	2700 6725 4175 6725
Wire Wire Line
	2325 7275 2325 6725
Connection ~ 2325 6725
Wire Wire Line
	2325 6725 2700 6725
Wire Wire Line
	1975 7275 1975 6725
Connection ~ 1975 6725
Wire Wire Line
	1975 6725 2325 6725
Wire Wire Line
	2700 7475 2700 7775
Wire Wire Line
	2700 7775 2325 7775
Wire Wire Line
	2325 7775 2325 8200
Wire Wire Line
	2325 7475 2325 7775
Connection ~ 2325 7775
Wire Wire Line
	1975 7475 1975 7775
Wire Wire Line
	1975 7775 2325 7775
Wire Wire Line
	3175 8000 2025 8000
Wire Wire Line
	2025 8000 2025 7375
Wire Wire Line
	2025 7375 1975 7375
Wire Wire Line
	2325 7375 2375 7375
Wire Wire Line
	2375 7375 2375 7900
Wire Wire Line
	2375 7900 3175 7900
Wire Wire Line
	3175 7800 2925 7800
Wire Wire Line
	2925 7800 2925 7375
Wire Wire Line
	2925 7375 2700 7375
$Comp
L Connector_Generic:Conn_01x04 J22
U 1 1 626818B9
P 7250 7800
F 0 "J22" V 7350 7825 50  0000 R CNN
F 1 "43650-0400" V 7123 7512 50  0001 R CNN
F 2 "Connector_Molex:Molex_Micro-Fit_3.0_43650-0400_1x04_P3.00mm_Horizontal" H 7250 7800 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/molex/43650-0400/www.molex.com/webdocs/datasheets/pdf/en-us/0436500400_PCB_HEADERS.pdf" H 7250 7800 50  0001 C CNN
F 4 "Molex" V 7250 7800 50  0001 C CNN "MFR"
F 5 "43650-0400" V 7250 7800 50  0001 C CNN "MPN"
	1    7250 7800
	0    -1   -1   0   
$EndComp
$Comp
L Connector_Generic:Conn_01x04 J23
U 1 1 6271B445
P 7750 7800
F 0 "J23" V 7875 7800 50  0000 R CNN
F 1 "43650-0400" V 7623 7512 50  0001 R CNN
F 2 "Connector_Molex:Molex_Micro-Fit_3.0_43650-0400_1x04_P3.00mm_Horizontal" H 7750 7800 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/molex/43650-0400/www.molex.com/webdocs/datasheets/pdf/en-us/0436500400_PCB_HEADERS.pdf" H 7750 7800 50  0001 C CNN
F 4 "Molex" V 7750 7800 50  0001 C CNN "MFR"
F 5 "43650-0400" V 7750 7800 50  0001 C CNN "MPN"
	1    7750 7800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6175 8025 6175 6725
Connection ~ 6175 6725
Wire Wire Line
	6175 6725 6375 6725
Wire Wire Line
	6775 8000 6775 8100
Wire Wire Line
	6775 8100 5775 8100
Connection ~ 5775 8100
Wire Wire Line
	6875 8000 6875 8200
Wire Wire Line
	6875 8200 6025 8200
Connection ~ 6025 8200
Wire Wire Line
	6175 8025 6675 8025
Wire Wire Line
	6675 8025 6675 8000
$Comp
L Connector_Generic:Conn_01x04 J21
U 1 1 6246B41A
P 6775 7800
F 0 "J21" V 6875 7825 50  0000 R CNN
F 1 "43650-0400" V 6648 7512 50  0001 R CNN
F 2 "Connector_Molex:Molex_Micro-Fit_3.0_43650-0400_1x04_P3.00mm_Horizontal" H 6775 7800 50  0001 C CNN
F 3 "https://www.digikey.com/en/products/detail/molex/43650-0400/www.molex.com/webdocs/datasheets/pdf/en-us/0436500400_PCB_HEADERS.pdf" H 6775 7800 50  0001 C CNN
F 4 "Molex" V 6775 7800 50  0001 C CNN "MFR"
F 5 "43650-0400" V 6775 7800 50  0001 C CNN "MPN"
	1    6775 7800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7150 8000 7150 8025
Wire Wire Line
	7150 8025 6675 8025
Connection ~ 6675 8025
Wire Wire Line
	7650 8000 7650 8025
Wire Wire Line
	7650 8025 7150 8025
Connection ~ 7150 8025
Wire Wire Line
	7250 8000 7250 8100
Wire Wire Line
	7250 8100 6775 8100
Connection ~ 6775 8100
Wire Wire Line
	7750 8000 7750 8100
Wire Wire Line
	7750 8100 7250 8100
Connection ~ 7250 8100
Wire Wire Line
	7350 8000 7350 8200
Wire Wire Line
	7350 8200 6875 8200
Connection ~ 6875 8200
Wire Wire Line
	7850 8000 7850 8200
Wire Wire Line
	7850 8200 7350 8200
Connection ~ 7350 8200
Wire Wire Line
	7950 8000 7950 8275
Wire Wire Line
	7950 8275 7450 8275
Wire Wire Line
	7450 8275 7450 8000
Wire Wire Line
	7450 8275 6975 8275
Wire Wire Line
	6975 8275 6975 8000
Connection ~ 7450 8275
Wire Wire Line
	6975 8275 5500 8275
Wire Wire Line
	5500 8275 5500 10350
Connection ~ 6975 8275
Connection ~ 5500 10350
Wire Wire Line
	5500 10350 5775 10350
Text Notes 6575 7625 0    60   ~ 0
SPARE I2C CONNECTIONS
Text Notes 1750 7050 0    60   ~ 0
SPARE I/O CONNECTIONS
Wire Wire Line
	14450 1375 14700 1375
Wire Wire Line
	14700 1375 14700 2550
Connection ~ 14450 1375
Wire Wire Line
	14700 2750 13400 2750
Wire Wire Line
	14700 2850 13325 2850
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 62FC974F
P 16175 1575
AR Path="/5B11D17D/62FC974F" Ref="R?"  Part="1" 
AR Path="/5B13E060/62FC974F" Ref="R?"  Part="1" 
AR Path="/5E57930B/62FC974F" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/62FC974F" Ref="R?"  Part="1" 
AR Path="/5F6C1276/62FC974F" Ref="R?"  Part="1" 
AR Path="/5F745286/62FC974F" Ref="R?"  Part="1" 
AR Path="/62FC974F" Ref="R33"  Part="1" 
F 0 "R33" V 15700 1675 50  0000 C CNN
F 1 "10K" V 15775 1675 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 16175 1575 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 16175 1575 60  0001 C CNN
F 4 "Susumu" H 8550 75  50  0001 C CNN "MFR"
F 5 "RR0816P-103-D" H 8550 75  50  0001 C CNN "MPN"
F 6 "-" H 8550 75  50  0001 C CNN "SPR"
F 7 "-" H 8550 75  50  0001 C CNN "SPN"
F 8 "-" H 8550 75  50  0001 C CNN "SPURL"
	1    16175 1575
	0    -1   1    0   
$EndComp
$Comp
L 109633-rescue:Rsmall-dgi2 R?
U 1 1 62FCAF52
P 16275 1575
AR Path="/5B11D17D/62FCAF52" Ref="R?"  Part="1" 
AR Path="/5B13E060/62FCAF52" Ref="R?"  Part="1" 
AR Path="/5E57930B/62FCAF52" Ref="R?"  Part="1" 
AR Path="/5EFBE18D/62FCAF52" Ref="R?"  Part="1" 
AR Path="/5F6C1276/62FCAF52" Ref="R?"  Part="1" 
AR Path="/5F745286/62FCAF52" Ref="R?"  Part="1" 
AR Path="/62FCAF52" Ref="R34"  Part="1" 
F 0 "R34" V 15800 1575 50  0000 C CNN
F 1 "10K" V 15875 1575 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 16275 1575 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 16275 1575 60  0001 C CNN
F 4 "Susumu" H 8650 75  50  0001 C CNN "MFR"
F 5 "RR0816P-103-D" H 8650 75  50  0001 C CNN "MPN"
F 6 "-" H 8650 75  50  0001 C CNN "SPR"
F 7 "-" H 8650 75  50  0001 C CNN "SPN"
F 8 "-" H 8650 75  50  0001 C CNN "SPURL"
	1    16275 1575
	0    -1   1    0   
$EndComp
Wire Wire Line
	16275 1675 16275 2850
Wire Wire Line
	16275 2850 16100 2850
Wire Wire Line
	14700 1375 16175 1375
Wire Wire Line
	16275 1375 16275 1475
Connection ~ 14700 1375
Wire Wire Line
	16175 1475 16175 1375
Connection ~ 16175 1375
Wire Wire Line
	16175 1375 16275 1375
Wire Wire Line
	13325 975  13325 2850
$Comp
L SensirionCO2_SensorsSCD4x:SCD41-D-R1 U7B1
U 1 1 611221DC
P 14725 3625
F 0 "U7B1" H 15025 3925 60  0000 C CNN
F 1 "SCD41-D-R1" H 15475 3925 60  0000 C CNN
F 2 "Footprint:SCD41-D-R1" H 15525 3865 60  0001 C CNN
F 3 "https://www.sensirion.com/fileadmin/user_upload/customers/sensirion/Dokumente/9.5_CO2/Sensirion_CO2_Sensors_SCD4x_Datasheet.pdf" H 14725 3625 60  0001 C CNN
F 4 "Sensirion" H 14725 3625 50  0001 C CNN "MFR"
F 5 "SCD41-D-R1" H 14725 3625 50  0001 C CNN "MPN"
	1    14725 3625
	1    0    0    -1  
$EndComp
Wire Wire Line
	16325 3825 16475 3825
Wire Wire Line
	16475 3825 16475 1375
Wire Wire Line
	16475 1375 16275 1375
Connection ~ 16275 1375
Wire Wire Line
	14725 4125 14450 4125
Connection ~ 14450 4125
Wire Wire Line
	14450 4125 14450 4375
Wire Wire Line
	14725 4225 13900 4225
Wire Wire Line
	14725 4525 13325 4525
$Comp
L Sensor_Gas:MQ-6 U1
U 1 1 61190FA0
P 5675 5525
F 0 "U1" H 5675 6106 50  0000 C CNN
F 1 "MQ-8" H 5675 6015 50  0000 C CNN
F 2 "Sensor:MQ-6" H 5725 5075 50  0001 C CNN
F 3 "https://www.winsen-sensor.com/d/files/semiconductor/mq-6.pdf" H 5675 5775 50  0001 C CNN
F 4 "Winsen" H 5675 5525 50  0001 C CNN "MFR"
F 5 "MQ-8" H 5675 5525 50  0001 C CNN "MPN"
	1    5675 5525
	1    0    0    -1  
$EndComp
Wire Wire Line
	5125 5125 5125 5425
Wire Wire Line
	5125 5125 5675 5125
Wire Wire Line
	5275 5425 5125 5425
Connection ~ 5125 5425
Wire Wire Line
	5275 5625 5125 5625
Wire Wire Line
	5125 5425 5125 5625
Connection ~ 5125 5625
Wire Wire Line
	5125 5625 5125 6725
Wire Wire Line
	6075 5425 7000 5425
Wire Wire Line
	7000 5425 7000 7525
Wire Wire Line
	7000 7525 6675 7525
Wire Wire Line
	5600 7525 5600 8000
Wire Wire Line
	5600 8000 5175 8000
$Comp
L 109633-rescue:Rsmall-dgi2 R35
U 1 1 614D9366
P 5675 6175
F 0 "R35" V 5650 6000 50  0000 C CNN
F 1 "10K" V 5750 5975 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5675 6175 60  0001 C CNN
F 3 "https://www.susumu.co.jp/common/pdf/n_catalog_partition05_en.pdf" H 5675 6175 60  0001 C CNN
F 4 "Susumu" H -3950 3600 50  0001 C CNN "MFR"
F 5 "RR0816P-103-D" H -3950 3600 50  0001 C CNN "MPN"
F 6 "-" H -3950 3600 50  0001 C CNN "SPR"
F 7 "-" H -3950 3600 50  0001 C CNN "SPN"
F 8 "-" H -3950 3600 50  0001 C CNN "SPURL"
	1    5675 6175
	0    -1   1    0   
$EndComp
Wire Wire Line
	5675 5925 5675 6075
Wire Wire Line
	6075 5625 6325 5625
Wire Wire Line
	6325 5625 6325 6375
Wire Wire Line
	6325 6375 5675 6375
Wire Wire Line
	5675 6375 5675 6275
Wire Wire Line
	6325 6375 8400 6375
Connection ~ 6325 6375
Connection ~ 8400 6375
Wire Wire Line
	8400 6375 8400 6125
$Comp
L dk_Test-Points:5001 TP4
U 1 1 611A222C
P 4175 6625
F 0 "TP4" H 4125 6672 50  0000 R CNN
F 1 "5001" H 4175 6525 50  0001 C CNN
F 2 "Footprint:Test_Point_D102mm" H 4375 6825 60  0001 L CNN
F 3 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 4375 6925 60  0001 L CNN
F 4 "36-5001-ND" H 4375 7025 60  0001 L CNN "Digi-Key_PN"
F 5 "5001" H 4375 7125 60  0001 L CNN "MPN"
F 6 "Test and Measurement" H 4375 7225 60  0001 L CNN "Category"
F 7 "Test Points" H 4375 7325 60  0001 L CNN "Family"
F 8 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 4375 7425 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/keystone-electronics/5001/36-5001-ND/255327" H 4375 7525 60  0001 L CNN "DK_Detail_Page"
F 10 "PC TEST POINT MINIATURE BLACK" H 4375 7625 60  0001 L CNN "Description"
F 11 "Keystone Electronics" H 4375 7725 60  0001 L CNN "MFR"
F 12 "Active" H 4375 7825 60  0001 L CNN "Status"
	1    4175 6625
	-1   0    0    1   
$EndComp
Wire Wire Line
	4175 6725 5125 6725
$Comp
L dk_Test-Points:5001 TP3
U 1 1 61206BF1
P 3300 6175
F 0 "TP3" H 3250 6222 50  0000 R CNN
F 1 "5001" H 3300 6075 50  0001 C CNN
F 2 "Footprint:Test_Point_D102mm" H 3500 6375 60  0001 L CNN
F 3 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 3500 6475 60  0001 L CNN
F 4 "36-5001-ND" H 3500 6575 60  0001 L CNN "Digi-Key_PN"
F 5 "5001" H 3500 6675 60  0001 L CNN "MPN"
F 6 "Test and Measurement" H 3500 6775 60  0001 L CNN "Category"
F 7 "Test Points" H 3500 6875 60  0001 L CNN "Family"
F 8 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 3500 6975 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/keystone-electronics/5001/36-5001-ND/255327" H 3500 7075 60  0001 L CNN "DK_Detail_Page"
F 10 "PC TEST POINT MINIATURE BLACK" H 3500 7175 60  0001 L CNN "Description"
F 11 "Keystone Electronics" H 3500 7275 60  0001 L CNN "MFR"
F 12 "Active" H 3500 7375 60  0001 L CNN "Status"
	1    3300 6175
	-1   0    0    1   
$EndComp
$Comp
L dk_Test-Points:5001 TP2
U 1 1 6120897C
P 3300 5200
F 0 "TP2" H 3250 5247 50  0000 R CNN
F 1 "5001" H 3300 5100 50  0001 C CNN
F 2 "Footprint:Test_Point_D102mm" H 3500 5400 60  0001 L CNN
F 3 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 3500 5500 60  0001 L CNN
F 4 "36-5001-ND" H 3500 5600 60  0001 L CNN "Digi-Key_PN"
F 5 "5001" H 3500 5700 60  0001 L CNN "MPN"
F 6 "Test and Measurement" H 3500 5800 60  0001 L CNN "Category"
F 7 "Test Points" H 3500 5900 60  0001 L CNN "Family"
F 8 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 3500 6000 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/keystone-electronics/5001/36-5001-ND/255327" H 3500 6100 60  0001 L CNN "DK_Detail_Page"
F 10 "PC TEST POINT MINIATURE BLACK" H 3500 6200 60  0001 L CNN "Description"
F 11 "Keystone Electronics" H 3500 6300 60  0001 L CNN "MFR"
F 12 "Active" H 3500 6400 60  0001 L CNN "Status"
	1    3300 5200
	-1   0    0    1   
$EndComp
Wire Wire Line
	3300 5300 3175 5300
Connection ~ 3175 5300
Wire Wire Line
	3175 5300 3175 5650
Wire Wire Line
	3300 6275 3175 6275
Connection ~ 3175 6275
Wire Wire Line
	3175 6275 3175 6600
$Comp
L dk_Test-Points:5001 TP6
U 1 1 612CE661
P 6675 7425
F 0 "TP6" H 6625 7472 50  0000 R CNN
F 1 "5001" H 6675 7325 50  0001 C CNN
F 2 "Footprint:Test_Point_D102mm" H 6875 7625 60  0001 L CNN
F 3 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 6875 7725 60  0001 L CNN
F 4 "36-5001-ND" H 6875 7825 60  0001 L CNN "Digi-Key_PN"
F 5 "5001" H 6875 7925 60  0001 L CNN "MPN"
F 6 "Test and Measurement" H 6875 8025 60  0001 L CNN "Category"
F 7 "Test Points" H 6875 8125 60  0001 L CNN "Family"
F 8 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 6875 8225 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/keystone-electronics/5001/36-5001-ND/255327" H 6875 8325 60  0001 L CNN "DK_Detail_Page"
F 10 "PC TEST POINT MINIATURE BLACK" H 6875 8425 60  0001 L CNN "Description"
F 11 "Keystone Electronics" H 6875 8525 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6875 8625 60  0001 L CNN "Status"
	1    6675 7425
	-1   0    0    1   
$EndComp
Connection ~ 6675 7525
Wire Wire Line
	6675 7525 5600 7525
$Comp
L dk_Test-Points:5001 TP8
U 1 1 612D0296
P 11275 7525
F 0 "TP8" H 11225 7572 50  0000 R CNN
F 1 "5001" H 11275 7425 50  0001 C CNN
F 2 "Footprint:Test_Point_D102mm" H 11475 7725 60  0001 L CNN
F 3 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 11475 7825 60  0001 L CNN
F 4 "36-5001-ND" H 11475 7925 60  0001 L CNN "Digi-Key_PN"
F 5 "5001" H 11475 8025 60  0001 L CNN "MPN"
F 6 "Test and Measurement" H 11475 8125 60  0001 L CNN "Category"
F 7 "Test Points" H 11475 8225 60  0001 L CNN "Family"
F 8 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 11475 8325 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/keystone-electronics/5001/36-5001-ND/255327" H 11475 8425 60  0001 L CNN "DK_Detail_Page"
F 10 "PC TEST POINT MINIATURE BLACK" H 11475 8525 60  0001 L CNN "Description"
F 11 "Keystone Electronics" H 11475 8625 60  0001 L CNN "MFR"
F 12 "Active" H 11475 8725 60  0001 L CNN "Status"
	1    11275 7525
	-1   0    0    1   
$EndComp
Connection ~ 11275 7625
Wire Wire Line
	11275 7625 11350 7625
$Comp
L dk_Test-Points:5001 TP7
U 1 1 612D0FB5
P 10950 7425
F 0 "TP7" H 10900 7472 50  0000 R CNN
F 1 "5001" H 10950 7325 50  0001 C CNN
F 2 "Footprint:Test_Point_D102mm" H 11150 7625 60  0001 L CNN
F 3 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 11150 7725 60  0001 L CNN
F 4 "36-5001-ND" H 11150 7825 60  0001 L CNN "Digi-Key_PN"
F 5 "5001" H 11150 7925 60  0001 L CNN "MPN"
F 6 "Test and Measurement" H 11150 8025 60  0001 L CNN "Category"
F 7 "Test Points" H 11150 8125 60  0001 L CNN "Family"
F 8 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 11150 8225 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/keystone-electronics/5001/36-5001-ND/255327" H 11150 8325 60  0001 L CNN "DK_Detail_Page"
F 10 "PC TEST POINT MINIATURE BLACK" H 11150 8425 60  0001 L CNN "Description"
F 11 "Keystone Electronics" H 11150 8525 60  0001 L CNN "MFR"
F 12 "Active" H 11150 8625 60  0001 L CNN "Status"
	1    10950 7425
	-1   0    0    1   
$EndComp
Connection ~ 10950 7525
Wire Wire Line
	10950 7525 11200 7525
$Comp
L dk_Test-Points:5001 TP5
U 1 1 612D14F4
P 6625 8700
F 0 "TP5" H 6575 8747 50  0000 R CNN
F 1 "5001" H 6625 8600 50  0001 C CNN
F 2 "Footprint:Test_Point_D102mm" H 6825 8900 60  0001 L CNN
F 3 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 6825 9000 60  0001 L CNN
F 4 "36-5001-ND" H 6825 9100 60  0001 L CNN "Digi-Key_PN"
F 5 "5001" H 6825 9200 60  0001 L CNN "MPN"
F 6 "Test and Measurement" H 6825 9300 60  0001 L CNN "Category"
F 7 "Test Points" H 6825 9400 60  0001 L CNN "Family"
F 8 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 6825 9500 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/keystone-electronics/5001/36-5001-ND/255327" H 6825 9600 60  0001 L CNN "DK_Detail_Page"
F 10 "PC TEST POINT MINIATURE BLACK" H 6825 9700 60  0001 L CNN "Description"
F 11 "Keystone Electronics" H 6825 9800 60  0001 L CNN "Manufacturer"
F 12 "Active" H 6825 9900 60  0001 L CNN "Status"
	1    6625 8700
	-1   0    0    1   
$EndComp
Connection ~ 6625 8800
Wire Wire Line
	6625 8800 6375 8800
$Comp
L dk_Test-Points:5001 TP1
U 1 1 612D3C33
P 2850 1275
F 0 "TP1" H 2800 1322 50  0000 R CNN
F 1 "5001" H 2850 1175 50  0001 C CNN
F 2 "Footprint:Test_Point_D102mm" H 3050 1475 60  0001 L CNN
F 3 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 3050 1575 60  0001 L CNN
F 4 "36-5001-ND" H 3050 1675 60  0001 L CNN "Digi-Key_PN"
F 5 "5001" H 3050 1775 60  0001 L CNN "MPN"
F 6 "Test and Measurement" H 3050 1875 60  0001 L CNN "Category"
F 7 "Test Points" H 3050 1975 60  0001 L CNN "Family"
F 8 "http://www.keyelco.com/product-pdf.cfm?p=1310" H 3050 2075 60  0001 L CNN "DK_Datasheet_Link"
F 9 "/product-detail/en/keystone-electronics/5001/36-5001-ND/255327" H 3050 2175 60  0001 L CNN "DK_Detail_Page"
F 10 "PC TEST POINT MINIATURE BLACK" H 3050 2275 60  0001 L CNN "Description"
F 11 "Keystone Electronics" H 3050 2375 60  0001 L CNN "MFR"
F 12 "Active" H 3050 2475 60  0001 L CNN "Status"
	1    2850 1275
	-1   0    0    1   
$EndComp
Wire Wire Line
	13900 1375 14450 1375
Connection ~ 13900 1375
Wire Wire Line
	12850 4375 14450 4375
Wire Wire Line
	13400 2750 13400 4425
Wire Wire Line
	13325 2850 13325 4525
Wire Wire Line
	14450 2325 14450 3050
Wire Wire Line
	12850 1375 13900 1375
Wire Wire Line
	13900 1375 13900 4225
Wire Wire Line
	13400 4425 14725 4425
Connection ~ 14450 3375
Wire Wire Line
	14450 3375 14450 4125
Wire Wire Line
	14700 3150 14450 3150
Connection ~ 14450 3150
Wire Wire Line
	14450 3150 14450 3375
Wire Wire Line
	14700 3050 14450 3050
Connection ~ 14450 3050
Wire Wire Line
	14450 3050 14450 3150
Wire Wire Line
	14450 3375 16425 3375
Wire Wire Line
	16325 3725 16425 3725
Wire Wire Line
	16425 3725 16425 3625
Wire Wire Line
	16325 3625 16425 3625
Connection ~ 16425 3625
Wire Wire Line
	16425 3625 16425 3375
$Comp
L 10142048:10142048-02 U7A1
U 1 1 62CC573B
P 15400 2850
F 0 "U7A1" H 15400 3500 60  0000 C CNN
F 1 "10142048-02" H 15400 3400 50  0000 C CNN
F 2 "Footprint:10142048-02" H 15400 2850 50  0001 L BNN
F 3 "https://www.te.com/commerce/DocumentDelivery/DDEController?Action=showdoc&DocId=Data+Sheet%7FHTU31_RHT_SENSOR_IC%7F3%7Fpdf%7FEnglish%7FENG_DS_HTU31_RHT_SENSOR_IC_3.pdf%7F10142048-01" H 15400 2850 50  0001 L BNN
F 4 "6" H 15400 2850 50  0001 L BNN "PARTREV"
F 5 "1.0 mm" H 15400 2850 50  0001 L BNN "MAXIMUM_PACKAGE_HEIGHT"
F 6 "IPC 7351B" H 15400 2850 50  0001 L BNN "STANDARD"
F 7 "TE Connectivity" H 15400 2850 50  0001 L BNN "MFR"
F 8 "10142048-02" H 15400 2850 50  0001 C CNN "MPN"
	1    15400 2850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	16175 1675 16175 2750
Wire Wire Line
	16175 2750 16100 2750
Wire Wire Line
	10450 1825 10775 1825
$Comp
L PCA9518AD:PCA9548AD,118 U4
U 1 1 61C4B051
P 10050 2825
F 0 "U4" H 9900 4306 50  0000 C CNN
F 1 "PCA9548AD,118" H 9900 4215 50  0000 C CNN
F 2 "Footprint:PCA9548AD_118" H 10050 2825 50  0001 L BNN
F 3 "https://www.nxp.com/docs/en/data-sheet/PCA9548A.pdf" H 10050 2825 50  0001 L BNN
F 4 "0.1" H 10050 2825 50  0001 L BNN "A1_MIN"
F 5 "15.4" H 10050 2825 50  0001 L BNN "D_NOM"
F 6 "NXP" H 10050 2825 50  0001 L BNN "MFR"
F 7 "15.2" H 10050 2825 50  0001 L BNN "D_MIN"
F 8 "15.6" H 10050 2825 50  0001 L BNN "D_MAX"
F 9 "24.0" H 10050 2825 50  0001 L BNN "PIN_COUNT"
F 10 "5.1" H 10050 2825 50  0001 L BNN "PARTREV"
F 11 "" H 10050 2825 50  0001 L BNN "SNAPEDA_PACKAGE_ID"
F 12 "0.36" H 10050 2825 50  0001 L BNN "B_MIN"
F 13 "" H 10050 2825 50  0001 L BNN "DMAX"
F 14 "0.75" H 10050 2825 50  0001 L BNN "L_NOM"
F 15 "" H 10050 2825 50  0001 L BNN "D1_MAX"
F 16 "0.4" H 10050 2825 50  0001 L BNN "L_MIN"
F 17 "10.325" H 10050 2825 50  0001 L BNN "E_NOM"
F 18 "" H 10050 2825 50  0001 L BNN "DMIN"
F 19 "10.0" H 10050 2825 50  0001 L BNN "E_MIN"
F 20 "IPC 7351B" H 10050 2825 50  0001 L BNN "STANDARD"
F 21 "" H 10050 2825 50  0001 L BNN "L1_MAX"
F 22 "7.6" H 10050 2825 50  0001 L BNN "E1_MAX"
F 23 "" H 10050 2825 50  0001 L BNN "DNOM"
F 24 "2.65" H 10050 2825 50  0001 L BNN "A_NOM"
F 25 "" H 10050 2825 50  0001 L BNN "EMIN"
F 26 "2.65" H 10050 2825 50  0001 L BNN "A_MAX"
F 27 "10.65" H 10050 2825 50  0001 L BNN "E_MAX"
F 28 "" H 10050 2825 50  0001 L BNN "PACKAGE_TYPE"
F 29 "" H 10050 2825 50  0001 L BNN "L1_MIN"
F 30 "" H 10050 2825 50  0001 L BNN "D1_NOM"
F 31 "7.5" H 10050 2825 50  0001 L BNN "E1_NOM"
F 32 "7.4" H 10050 2825 50  0001 L BNN "E1_MIN"
F 33 "" H 10050 2825 50  0001 L BNN "PINS"
F 34 "0.0" H 10050 2825 50  0001 L BNN "E2_MAX"
F 35 "" H 10050 2825 50  0001 L BNN "D1_MIN"
F 36 "" H 10050 2825 50  0001 L BNN "EMAX"
F 37 "1.1" H 10050 2825 50  0001 L BNN "L_MAX"
F 38 "" H 10050 2825 50  0001 L BNN "L1_NOM"
F 39 "1.27" H 10050 2825 50  0001 L BNN "ENOM"
F 40 "" H 10050 2825 50  0001 L BNN "VACANCIES"
F 41 "0.0" H 10050 2825 50  0001 L BNN "D2_MAX"
F 42 "0.425" H 10050 2825 50  0001 L BNN "B_NOM"
F 43 "2.65" H 10050 2825 50  0001 L BNN "A_MIN"
F 44 "0.49" H 10050 2825 50  0001 L BNN "B_MAX"
F 45 "PCA9548AD,118" H 10050 2825 50  0001 C CNN "MPN"
	1    10050 2825
	1    0    0    -1  
$EndComp
Wire Wire Line
	9350 2625 8575 2625
Wire Wire Line
	8575 1825 8575 2625
Wire Wire Line
	10450 1925 10875 1925
Wire Wire Line
	10450 2225 11050 2225
Wire Wire Line
	10450 2325 11150 2325
Wire Wire Line
	10450 2875 12225 2875
Wire Wire Line
	12225 2875 12225 2850
Wire Wire Line
	10450 2775 10450 2750
Wire Wire Line
	10450 2750 13400 2750
Wire Wire Line
	10450 3325 11325 3325
Wire Wire Line
	10450 3775 11700 3775
Wire Wire Line
	10450 3675 11600 3675
Wire Wire Line
	10450 3425 11425 3425
Wire Wire Line
	9250 1375 9950 1375
Wire Wire Line
	9950 1525 9950 1375
Connection ~ 9950 1375
Wire Wire Line
	9950 1375 10775 1375
Wire Wire Line
	9900 4025 9900 4375
Connection ~ 9900 4375
Wire Wire Line
	9900 4375 12350 4375
Wire Wire Line
	8450 2825 8450 2875
Connection ~ 2325 8200
Connection ~ 2325 8600
Wire Wire Line
	2325 8600 2325 10350
Wire Wire Line
	2325 8600 2425 8600
Wire Wire Line
	2325 8200 2425 8200
Wire Wire Line
	2325 8200 2325 8600
Wire Wire Line
	3000 2475 4300 2475
$EndSCHEMATC
