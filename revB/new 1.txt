import tkinter as tk
import tkinter.ttk as ttk
#from tkinter import Entry
from tkinter.ttk import *
import serial.tools.list_ports
from tkinter import *
from tkinter import filedialog
ProfilerEndcapConfiguration = []  # Declared.

root = tk.Tk()
root.title("Automation Configuration")

exitButton = Button(root, text="Quit", bg="white", fg="red", command=lambda: root.destroy()).place(x=500, y=10)

w, h = root.winfo_screenwidth(), root.winfo_screenheight()
root.geometry("%dx%d+0+0" % (w, h))

ConfigBox = tk.Frame(root, bg='white')
ConfigBox.place(x=110, y=300, height=400, width=1300)

##############################################################################
# Name          : Declare base file name
# Purpose       : Base file name for all Factory Acceptance Test (FAT) names
# Author	    : bkelly@whoi.edu
##############################################################################
nameFile = StringVar()
#nameFile = ""
saveMAT = Entry(root, bg="white", textvariable=nameFile, width=68)
saveMAT.place(x=800, y=80)
saveMAT.set = ""
saveMATlabel = Label(root, text="Please insert the BASE file name\nThe base file name must only\ncontain letters and/or numbers", fg="green").place(x=600, y=50)

ExperimentDir = StringVar()
mystring = tk.StringVar(ConfigBox)

def exeCmd():
    ccmd = listbox2.get(ANCHOR)
    exec(ccmd)

##############################################################################
# Name          : on_enter
# Purpose       : Commit User question(s)
# Author	    : bkelly@whoi.edu
##############################################################################
def on_enter(event):
    widget = event.widget
    print(widget.get())
    listbox3.insert(ANCHOR, (widget.get()))
    widget.delete(0, END)  # Clear field


##############################################################################
# Name          : Generic commit Commands
# Purpose       : Commit Command
# Author	    : bkelly@whoi.edu
##############################################################################
def genCommit(data):
    if data != "":
        listbox2.insert(END, data)


##############################################################################
# Name          : Serial Config Port Command
# Purpose       : Commit serial config string(s)
# Author	    : bkelly@whoi.edu
##############################################################################
def ParseConfigCommit(Port, ptype, ploc, pdel, pdelnum):
    if Port.startswith("COM"):
        start = "("
        end = ")"
        Port = ((Port.split(start))[1].split(end)[0])
        print(Port)
        ##syntax ser=serial.Serial('COM7', baudrate=19200, bytesize=8, parity='N', stopbits=1, timeout=None, xonxoff=0, rtscts=0, dsrdtr=False)
        PortConfig = ("ser=serial.Serial("+Port+",Parse Type="+ptype+",Parse Location="+ploc+",Parse Delimeter="+pdel+",Parse Delimeter Number="+pdelnum+")")
        print(PortConfig)
        listbox2.insert(END, PortConfig)
        ConfigBox.place(x=110, y=300, height=400, width=1300)


##############################################################################
# Name          : Serial Config Port Command
# Purpose       : Commit serial config string(s)
# Author	    : bkelly@whoi.edu
##############################################################################
def SerialConfigCommit(Port, Baud, Bits, Parity, Stop, xonxoff, rtscts, dtr):
if Port.startswith("COM"):
        start = "("
        end = ")"
        Port = ((Port.split(start))[1].split(end)[0])
        print(Port)
        ##syntax ser=serial.Serial('COM7', baudrate=19200, bytesize=8, parity='N', stopbits=1, timeout=None, xonxoff=0, rtscts=0, dsrdtr=False)
        PortConfig = ("ser=serial.Serial('"+Port+"',baudrate="+Baud+",bytesize="+Bits+",parity='"+Parity+"',stopbits="+Stop+",timeout=None,xonxoff="+xonxoff+",rtscts="+rtscts+",dsrdtr="+dtr+")")
        print(PortConfig)
        listbox2.insert(END, PortConfig)
        ConfigBox.place(x=110, y=300, height=400, width=1300)


##############################################################################
# Name          : GetExperimentDir
# Purpose       : Select the experiment directory for saving log files
# Author	    : bkelly@whoi.edu
##############################################################################
def GetExperimentDir():
    global ExperimentDir
    ExperimentDir = filedialog.askdirectory(parent=root, initialdir="C:/Users/Brian/Documents/bitbucket/mooringautomationterminal", title='Please select project directory')
    Button(root, text=ExperimentDir, width=60, command=lambda: GetExperimentDir())
    ExpDir = Label(root, text=ExperimentDir, relief=FLAT, anchor="w", width=60)
    ExpDir.place(x=800, y=40)
    return ExperimentDir


def removeItem():
    listbox2.delete(ANCHOR)


def moveDown():
    listbox2.get(listbox2.curselection())
    x1 = listbox2.curselection()[0]
    listbox2.selection_clear(x1)
    if x1 + 1 == listbox2.size():
        listbox2.selection_set(0)
    else:
        listbox2.selection_set(x1 + 1)


def moveUp():
    listbox2.get(listbox2.curselection())
    x1 = listbox2.curselection()[0]
    listbox2.selection_clear(x1)
    if x1 - 1 == listbox2.size():
        listbox2.selection_set(0)
    else:
        listbox2.selection_set(x1 - 1)


def saveFile():
    newfile = saveMAT.get()
    try:
        if newfile == "":
            newfile = "Default.map"
            f = open(newfile, mode='w+', encoding='utf-8')
            for lines in (listbox2.get(0, END)):
                if not lines.isspace():
                    f.write(lines)
                    saveMAT.delete(0, END)  # Clear field
                    saveMAT.insert(tk.END, newfile)  # add this
            #f.close
        else:
            f = open(newfile, mode='w+', encoding='utf-8')
            for lines in (listbox2.get(0, END)):
                if not lines.isspace():
                    f.write(lines)
                    saveMAT.delete(0, END)  # Clear field
                    saveMAT.insert(tk.END, newfile)  # add this
            #f.close
    except IndexError:
        pass


def loadFile():
    global newfile
    listbox2.delete(0, END)
    newfile = 'BJK.txt'
    saveMAT.delete(0, END)  # Clear field
    saveMAT.insert(tk.END, newfile)  # add this
    f = open(newfile, "r")
    for x in f:
        listbox2.insert(END, x)
    f.close()


def copyItem():
    value = listbox1.get(ANCHOR)
    filenameConfig = saveMAT.get()
    if filenameConfig.isalnum() is False:
        badfilename = Label(root, text="**********************************\nA file directory and file name\n"
                                       "must be chosen before continuing\nThe base file name can be\n letters and/or "
                                       "numbers only", fg="RED", width=30)
        badfilename.place(x=1225, y=20)
        print("Blank", filenameConfig)
        mainloop()

    if filenameConfig.isalnum() is True:
        badFileName = Label(root, text="\n\n\n\n", fg="white", width=30)
        badFileName.place(x=1225, y=20)
        #print("Text entered", filenameConfig)

    if value == "Configure Serial Port":
        ConfigBox = tk.Frame(root, bg='white')
        ConfigBox.place(x=110, y=300, height=400, width=1300)

        ConfigLbl = Label(ConfigBox, text="Serial Port Configuration", bg="white", fg="blue")
        ConfigLbl.place(x=0, y=0)

        ports = serial.tools.list_ports.comports()
        selectedport = ttk.Combobox(ConfigBox, values=ports)
        selectedport.pack()
        selectedport.place(x=0, y=20)
        selectedport.bind('<<ComboboxSelected>>', on_select)

        Label(ConfigBox, text="Select Baud Rate", bg="white").place(x=175, y=0)

        portbaud = StringVar(ConfigBox, "1")
        # Baud Rate Selections
        values = {"110 Baud": "110", "300 Baud": "300", "600 Baud": "600", "1200 Baud": "1200", "2400 Baud": "2400",
                  "4800 Baud": "4800", "9600 Baud": "9600", "19200 Baud": "19200", "38400 Baud": "38400", "57600 Baud": "19200",
                  "115200 Baud": "115200"}

        yy = 30  # Line by line increment
        for (text, value) in values.items():
            Radiobutton(ConfigBox, text=text, variable=portbaud, value=value, bg="white").place(x=175, y=yy)
            yy = (yy + 20)

        Label(ConfigBox, text="Select Data Bits", bg="white").place(x=270, y=0)

        portbits = StringVar(ConfigBox, "1")
        # Data bits Selections
        values = {"8 Bits": "8", "7 Bits": "7"}

        yy = 30  # Line by line increment
        for (text, value) in values.items():
            Radiobutton(ConfigBox, text=text, variable=portbits, value=value, bg="white").place(x=270, y=yy)
            yy = (yy + 20)

        Label(ConfigBox, text="Select Parity", bg="white").place(x=335, y=0)
        portparity = StringVar(ConfigBox, "1")
        # Data parity Selections
        values = {"No Parity": "N", "odd Parity": "O", "even Parity": "E", "mark Parity": "M", "space Parity": "S"}

        yy = 30  # Line by line increment
        for (text, value) in values.items():
            Radiobutton(ConfigBox, text=text, variable=portparity, value=value, bg="white").place(x=335, y=yy)
            yy = (yy + 20)

        Label(ConfigBox, text="Select # of Stop Bits", bg="white").place(x=450, y=0)
        portstop = StringVar(ConfigBox, "1")
        # Data stop Selections
        values = {"1 Stop Bit": "1", "1.5 Stop Bit": "1.5", "2 Stop Bit": "2"}

        yy = 30  # Line by line increment
        for (text, value) in values.items():
            Radiobutton(ConfigBox, text=text, variable=portstop, value=value, bg="white").place(x=450, y=yy)
            yy = (yy + 20)

        Label(ConfigBox, text="Select Flow Control", bg="white").place(x=575, y=0)
        portxonxoff = StringVar(ConfigBox, "1")
        # Data flow Selections
        values = {"none": "0", "xon/xoff": "1"}

        yy = 30  # Line by line increment
        for (text, value) in values.items():
            Radiobutton(ConfigBox, text=text, variable=portxonxoff, value=value, bg="white").place(x=575, y=yy)
            yy = (yy + 20)

        Label(ConfigBox, text="Select RTS/CTS", bg="white").place(x=700, y=0)
        portrtscts = StringVar(ConfigBox, "1")
        # Data flow Selections
        values = {"none": "0", "RTSCTS": "1"}

        yy = 30  # Line by line increment
        for (text, value) in values.items():
            Radiobutton(ConfigBox, text=text, variable=portrtscts, value=value, bg="white").place(x=700, y=yy)
            yy = (yy + 20)

        Label(ConfigBox, text="Select DTR/DSR", bg="white").place(x=825, y=0)
        portdtrdsr = StringVar(ConfigBox, "1")
        # Data bits Selections
        values = {"none": "False", "DTR/DSR": "True"}

        yy = 30  # Line by line increment
        for (text, value) in values.items():
            Radiobutton(ConfigBox, text=text, variable=portdtrdsr, value=value, bg="white").place(x=825, y=yy)
            yy = (yy + 20)
        ##syntax ser=serial.Serial('COM7', baudrate=19200, bytesize=8, parity='N', stopbits=1, timeout=None, xonxoff=0, rtscts=0)
        AddSerialPort = Button(ConfigBox, text="Add Port",
                               command=lambda: SerialConfigCommit(selectedport.get(), portbaud.get(), portbits.get(),
                                                                  portparity.get(), portstop.get(), portxonxoff.get(),
                                                                  portrtscts.get(), portdtrdsr.get()))
        AddSerialPort.place(x=0, y=50)

    if value == 'Generate User Questions':
        ConfigBox = tk.Frame(root, bg='white')
        ConfigBox.place(x=110, y=300, height=400, width=1300)

        l3 = Label(ConfigBox, text="Build user question list")
        l3.place(x=10, y=10)
        global listbox3
        listbox3 = Listbox(ConfigBox)
        listbox3.place(x=10, y=90, width=500)

        ProfilerEndcapConfiguration = Entry(ConfigBox, width=60)
        ProfilerEndcapConfiguration.place(x=10, y=50)
        ProfilerEndcapConfiguration.focus()
        ProfilerEndcapConfiguration.bind('<Return>', on_enter)

    if value == 'Generate custom command':
        ConfigBox = tk.Frame(root, bg='white')
        ConfigBox.place(x=110, y=300, height=400, width=1300)
        Label(ConfigBox, text="Generate custom command").place(x=10, y=10)
        userCommand = StringVar()
        Entry(ConfigBox, bg="white", textvariable=userCommand, width=75).place(x=10, y=50)
        Button(ConfigBox, bg="white", fg="green", text="Add command to script",
               command=lambda: genCommit(userCommand.get())).place(x=10, y=80)

    if value == "Parse Data Stream":
        ConfigBox = tk.Frame(root, bg='white')
        ConfigBox.place(x=110, y=300, height=400, width=1300)

        #testParse = StringVar
        #testParse = "2014/08/11 00:00:30.910 superv cpm: 24.4 540.0 0.0 0.0 00000000 t 36.0 33.6 h 3.3 p 14.7 gf f 22.2 -35.1 116.9 66.5 ld 3 1216 1194"

        ConfigLbl = Label(ConfigBox, text="Serial Port Configuration", bg="white", fg="blue")
        ConfigLbl.place(x=0, y=0)

        ports = serial.tools.list_ports.comports()
        selectedport = ttk.Combobox(ConfigBox, values=ports)
        selectedport.pack()
        selectedport.place(x=0, y=20)
        selectedport.bind('<<ComboboxSelected>>', on_select)

        Label(ConfigBox, text="Select parse type", bg="white").place(x=155, y=0)

        ptype = StringVar(ConfigBox, "1")
        # Parse type selections
        values = {"float": "f", "Integer": "Int", "Character": "char", "String": "str"}

        yy = 30  # Line by line increment
        for (text, value) in values.items():
            Radiobutton(ConfigBox, text=text, variable=ptype, value=value, bg="white").place(x=155, y=yy)
            yy = (yy + 20)

        Label(ConfigBox, text="Select Parse Location", bg="white").place(x=275, y=0)

        ploc = StringVar(ConfigBox, "1")
        # Data bits Selections
        values = {"Beginning": "begin", "End": "end", "Between": "middle", "After specified characters": "after",
                  "Before specified characters": "before", "Delimeter": "delimeter"}

        yy = 30  # Line by line increment
        for (text, value) in values.items():
            Radiobutton(ConfigBox, text=text, variable=ploc, value=value, bg="white").place(x=275, y=yy)
            yy = (yy + 20)

        ##Parse Delimeter
        Label(ConfigBox, text="Select Parse Delimiter", bg="white").place(x=450, y=0)

        pdel = StringVar(ConfigBox, "1")
        values = {"Space": " ", "Comma ,": ",", "Back Slash \\": "\\", "Forward slash /": "/",
                  "Semi colin ;": ";"}

        yy = 30  # Line by line increment
        for (text, value) in values.items():
            Radiobutton(ConfigBox, text=text, variable=pdel, value=value, bg="white").place(x=450, y=yy)
            yy = (yy + 20)

        #parse delimited item number
        Label(ConfigBox, text="Which segment", bg="white").place(x=0, y=50)
        pdelnum = StringVar()
        pdelnum.set = ""
        Entry(ConfigBox, bg="white", textvariable=pdelnum, width=5).place(x=0, y=70)

        #Test Source
        Label(ConfigBox, text="Test Source", bg="white").place(x=650, y=0)
        pTestSource = StringVar()
        pTestSource.set = ""
        Text(ConfigBox, bg="white", width=50, height=3).place(x=650, y=20)

        #Test Source
        Label(ConfigBox, text="Command String", bg="white").place(x=650, y=70)
        pTestSource = StringVar()
        pTestSource.set = ""
        Text(ConfigBox, bg="white", width=50, height=1).place(x=650, y=90)

        #Test Results
        Label(ConfigBox, text="Test Result", bg="white").place(x=650, y=300)
        pTestResult = StringVar()
        pTestResult.set = ""
        Text(ConfigBox, bg="white", width=50, height=3).place(x=650, y=320)

        beforeParse = StringVar()
        beforeParse.set = ""
        Entry(ConfigBox, bg="white", textvariable=beforeParse, width=68).place(x=10, y=200)
        Label(ConfigBox, text="Please enter the **Exact** text expected\nbefore the parsed item",
              fg="green").place(x=10, y=150)

        afterParse = StringVar()
        afterParse.set = ""
        Entry(ConfigBox, bg="white", textvariable=afterParse, width=68).place(x=500, y=200)
        Label(ConfigBox, text="Please enter the **Exact** text expected\nafter the parsed item",
              fg="green").place(x=500, y=150)

        textBeforeParse = StringVar()
        textBeforeParse.set = ""
        Entry(ConfigBox, bg="white", textvariable=textBeforeParse, width=68).place(x=10, y=280)
        Label(ConfigBox, text="Please enter the text to be added\nin the report before the parsed item",
              fg="blue").place(x=10, y=240)

        textAfterParse = StringVar()
        textAfterParse.set = ""
        Entry(ConfigBox, bg="white", textvariable=textAfterParse, width=68).place(x=500, y=280)
        Label(ConfigBox, text="Please enter the text to be added\nin the report after the parsed item",
              fg="blue").place(x=500, y=240)

        ##syntax ser=serial.Serial('COM7', baudrate=19200, bytesize=8, parity='N', stopbits=1, timeout=None, xonxoff=0, rtscts=0)
        AddSerialPort = Button(ConfigBox, text="Add Port",
                               command=lambda: ParseConfigCommit(selectedport.get(), ptype.get(), ploc.get(), pdel.get(), pdelnum.get()))
        AddSerialPort.place(x=0, y=100)


label = Label(ConfigBox)
label.pack()


def serial_ports():
    return serial.tools.list_ports.comports()


def on_select(event=None):

    # get selection from event
    TestPort = (event.widget.get())
    if TestPort.startswith("COM"):
        start = "("
        end = ")"
        TestPort = ((TestPort.split(start))[1].split(end)[0])
    # print(event.widget.get())
    print(TestPort)


label1 = Label(root, text="COMMANDS")
label1.place(x=110, y=80)
listbox1 = Listbox(root)
listbox1.place(x=110, y=100, width=200)
listbox1.insert(0, "Configure Serial Port")
listbox1.insert(1, "Configure ADC Input")
listbox1.insert(2, "Send Command")
listbox1.insert(3, "Parse Data Stream")
listbox1.insert(4, "Log Serial Response")
listbox1.insert(5, "Log Analog Value")
listbox1.insert(6, "Insert Text")
listbox1.insert(7, "Request User Input")
listbox1.insert(8, "Request Yes or No")
listbox1.insert(9, "Wait for User")
listbox1.insert(10, "Get Serial Number")
listbox1.insert(11, "System Version")
listbox1.insert(12, "Start Test Log")
listbox1.insert(13, "Request Pre-Test Details")
listbox1.insert(14, "Generate User Questions")
listbox1.insert(15, "Generate custom command")

l2 = Label(root, text="SCRIPT BUILD")
l2.place(x=450, y=80)
listbox2 = Listbox(root)
listbox2.place(x=515, y=100, width=900)

GetExpDir = Button(root, text="Get Project Directory", width=50, command=lambda: GetExperimentDir())
GetExpDir.config(font=("Courier", 10))
GetExpDir.place(x=800, y=10)

sendCommand = Button(root, text="Execute Command", width=50, command=lambda: exeCmd())
sendCommand.config(font=("Courier", 10))
sendCommand.place(x=800, y=100)

#Control Configuration program
b2 = Button(root, wraplength=60, width=10, text="Remove From Script", command=removeItem).place(x=320, y=160)
b3 = Button(root, wraplength=70, width=10, text="Add To Script", command=copyItem).place(x=320, y=220)
b4 = Button(root, width=10, text="Save File", command=saveFile).place(x=320, y=100)
b5 = Button(root, width=10, text="Load File", command=loadFile).place(x=320, y=130)
b6 = Button(root, wraplength=60, width=10, text="Move Script Command Down", command=moveDown).place(x=425, y=100)
b7 = Button(root, wraplength=60, width=10, text="Move Script Command Up", command=moveUp).place(x=425, y=190)

tk.mainloop()
