import tkinter as tk
import tkinter.ttk as ttk
#from tkinter import Entry
from tkinter.ttk import *
import serial
import serial.tools.list_ports
from tkinter import *
from tkinter import filedialog
import re
import numpy as np
import ast
import csv
import pandas as pd
import os
from datetime import datetime
import math
import Pmw
import time, threading
from tkinter import messagebox
import subprocess
import shlex

refreshTime = 30

setChgVoltsOne = "16"


root = tk.Tk()
root.title("Lithium Battery User Interface (LBUI)")

exitButton = Button(root, text="Quit", bg="white", fg="red", command=lambda: root.destroy()).place(x=1300, y=10)

w, h = root.winfo_screenwidth(), root.winfo_screenheight()
root.geometry("%dx%d+0+0" % (w, h))

rows, cols = (12, 30)
battMatrix = [[0] * cols] * rows

sleepTime = 60
port = "NONE"

# Keep track of the on/off button state for the power supply
global is_on
is_on = True

def dateTimeSet(dtString):
    #subprocess.call(shlex.split("timedatectl set-ntp false"))  # May be necessary
    subprocess.call(shlex.split("sudo date -s dtString"))
    #subprocess.call(shlex.split("sudo hwclock -w"))

def openNewWindow():
    newWindow = Toplevel(root)
    newWindow.title("Charger (Power Supply) Settings")
    newWindow.geometry("600x600")

    voltageScale = Scale(newWindow, variable=setChgVoltsOne, from_=0, to=16.5, resolution=0.5, orient=HORIZONTAL)
    voltageScale.place(x=50,y=50)
    voltageScaleLabel = Label(newWindow,text="Select charging voltage")
    voltageScaleLabel.place(x=50, y=100)

    currentScale = Scale(newWindow, from_=0, to=10, orient=HORIZONTAL)
    currentScale.place(x=200,y=50)
    currentScaleLabel = Label(newWindow,text="Select charging current")
    currentScaleLabel.place(x=200, y=100)

    def switch():
        global is_on

        # Determine is on or off
        if is_on:
            on_button.config(image=off)
            my_label.config(text="The Switch is Off",
                            fg="grey")
            is_on = False
        else:
            on_button.config(image=on)
            my_label.config(text="The Switch is On", fg="green")
            is_on = True

    if is_on:
        on_button.config(image=off)
        my_label.config(text="The Switch is Off",
                        fg="grey")
        is_on = False
    else:
        on_button.config(image=on)
        my_label.config(text="The Switch is On", fg="green")
        is_on = True

# powerSupplyConfigButton = Button(root,text="Click to configure charger",command=openNewWindow)
# powerSupplyConfigButton.place(x=200, y=50)

def serial_ports():
    return serial.tools.list_ports.comports()

def on_select(event=port):
    global port
    port = event.widget.get()
    if port.startswith('COM'):
        port = port.split(' - ')[0]
    ##print (port)
    return port

def autoUpdate(): #Auto update from Battery
    threading.Timer(sleepTime, autoUpdate).start()
    ##getSerUpdate(port) #Get data from board to CSV file
    updateMatrix() # Interpet and display data from CSV file

##############################################################################
# Name          : GetExperimentDir
# Purpose       : Select the experiment directory for saving log files
# Author	    : bkelly@whoi.edu
##############################################################################
def GetExperimentDir():
    global ExperimentDir
    ExperimentDir = filedialog.askdirectory(parent=root, initialdir="C:/Users/Brian/Documents/Projects/OOI/LithiumBatteryInterface", title='Please select project directory')
    Button(root, text=ExperimentDir, width=60, command=lambda: GetExperimentDir())
    ExpDir = Label(root, text=ExperimentDir, relief=FLAT, anchor="w", width=60)
    ExpDir.place(x=150, y=12)
    return ExperimentDir

def prepend_line(file_name, line):
    """ Insert given string as a new line at the beginning of a file """
    # define name of temporary dummy file
    dummy_file = file_name + '.bak'
    # open original file in read mode and dummy file in write mode
    with open(file_name, 'r') as read_obj, open(dummy_file, 'w') as write_obj:
        # Write given line to the dummy file
        write_obj.write(line)
        # Read lines from original file one by one and append them to the dummy file
        for line in read_obj:
            write_obj.write(line)
    # remove original file
    os.remove(file_name)
    # Rename dummy file as the original file
    os.rename(dummy_file, file_name)

def getSerUpdate(port):
    print (port)
    if port == ("NONE"):
        messagebox.showinfo(title="Warning", message="Select battery data serial port")
    now = datetime.now()
    dateString = now.strftime("%d%m%Y")
    timeString = now.strftime("%H%M%S")
    ser = serial.Serial(port, 115200, timeout=1)
    count = 0
    while (count < 13):
        count = count + 1
        lbdata = ser.readline()
        lbdata = lbdata.decode("utf-8")
        start = lbdata.find("#")
        end = lbdata.find(";")
        lbdata = lbdata.replace(' ', ',')
        lbdata = lbdata.replace('#', '')
        lbdata = lbdata.replace(',;', "," + dateString + "," + timeString + '\r')
        print(lbdata)
        prepend_line('battData.csv', lbdata)

def updateMatrix(): ## Comment out for normal operation
    now = datetime.now()
    dateString = now.strftime("%d%m%Y")
    timeString = now.strftime("%H%M%S")
    battData = pd.read_csv("battData.csv", header = None) ##Battery data csv read into battData array
    for i in range(0, 12): ## Cycle through the battery data
        bat = battData.iloc[i, 0] ##battery number (Character)
        batInt = int(bat) ##Convert battery number to integer

        if (batInt == 1): ##Determine which battery of data and there for what is the y position to place that data on the screen
            yy = 140
        elif (batInt == 2):
            yy = 160
        elif (batInt == 3):
            yy = 180
        elif (batInt == 4):
            yy = 200
        elif (batInt == 5):
            yy = 220
        elif (batInt == 6):
            yy = 240
        elif (batInt == 7):
            yy = 260
        elif (batInt == 8):
            yy = 280
        elif (batInt == 9):
            yy = 300
        elif (batInt == 10):
            yy = 320
        elif (batInt == 11):
            yy = 340
        elif (batInt == 12):
            yy = 360
        else:
            print("Less than 12 batteries in the string")
        l = 5
        for zz in range(0, 14):  ## Cycle through the battery data
            bat = (battData.iloc[i, zz])

            if zz == 3 or zz == 7: ## Convert to binary string
                bat = int(bat)
                bat = "{0:b}".format(bat).strip("-" "")

            if zz == 8:##Format Temperature display in celcius
                bat = float(bat)
                bat = bat - 273.15
                bat = bat / 100
                bat = "{:.1f}".format(bat)

            if zz == 9:##Format Voltage display
                bat = float(bat)
                bat = bat / 1000
                bat = "{:.2f}".format(bat)

            if zz == 10: ##Format Current display
                bat = float(bat)
                bat = bat / 1000
                bat = "{:.3f}".format(bat)

            if zz == 11:  ##Format Average current display
                bat = float(bat)
                bat = bat / 1000
                bat = "{:.3f}".format(bat)

            if zz == 12 or zz == 13:
                bat = float(bat)
                bat = bat / 100
                bat = "{:.0f}".format(bat)

            batData2Array = Label(root, text=bat, width=15)
            if (i % 2) == 0:
                labelColor = "green"
            else:
                labelColor = "blue"
            batData2Array.config(font=("Courier", 10), foreground=labelColor)
            batData2Array.place(x=l, y=yy)

            if zz == 0:
                batData2Array = Label(root, text=bat, width=15)
                if (i % 2) == 0:
                    labelColor = "green"
                else:
                    labelColor = "blue"
                batData2Array.config(font=("Courier", 10), foreground=labelColor)
                batData2Array.place(x=5, y=(yy+340))
            l = l + 100

    battData = pd.read_csv("battData.csv", header = None) ##Battery data csv read into battData array
    for i in range(0, 12): ## Cycle through the battery data
        bat = battData.iloc[i, 0] ##battery number (Character)
        batInt = int(bat) ##Convert battery number to integer
        if (batInt == 1): ##Determine which battery of data and there for what is the y position to place that data on the screen
            yy = 480
        elif (batInt == 2):
            yy = 500
        elif (batInt == 3):
            yy = 520
        elif (batInt == 4):
            yy = 540
        elif (batInt == 5):
            yy = 560
        elif (batInt == 6):
            yy = 580
        elif (batInt == 7):
            yy = 600
        elif (batInt == 8):
            yy = 620
        elif (batInt == 9):
            yy = 640
        elif (batInt == 10):
            yy = 660
        elif (batInt == 11):
            yy = 680
        elif (batInt == 12):
            yy = 700
        else:
            print("Less than 12 batteries in the string")
        l = 110
        for zz in range(14, 27):  ## Cycle through the battery data
            bat = (battData.iloc[i, zz])

            if zz == 15:
                bat = float(bat)
                bat = bat / 100
                bat = "{:.0f}".format(bat)

            if zz == 16:
                bat = float(bat)
                bat = bat / 1000
                bat = "{:.3f}".format(bat)

            if zz == 17 or zz == 18 or zz == 19:
                bat = float(bat)
                bat = int(bat)

            if zz == 20: ##Format Charging current display
                bat = float(bat)
                bat = bat / 1000
                bat = "{:.3f}".format(bat)

            if zz == 21:##Format Charging voltage display
                bat = float(bat)
                bat = bat / 1000
                bat = "{:.2f}".format(bat)

            if zz == 22:##Format Battery status
                bat = float(bat)
                bat = int(bat)
                bat = hex(bat)

            if zz == 23 or zz == 24: ## Convert to binary string
                bat = int(bat)
                bat = "{0:b}".format(bat).strip("-" "")

            if zz == 25:
                bat = dateString
                bat = float(bat)
                bat = int(bat)

            if zz == 26:
                bat == timeString
                bat = float(bat)
                bat = int(bat)

            batData2Array = Label(root, text=bat, width=15)
            if (i % 2) == 0:
                labelColor = "green"
            else:
                labelColor = "blue"
            batData2Array.config(font=("Courier", 10),foreground=labelColor)
            batData2Array.place(x=l, y=yy)
            l = l + 100

batNum = Label(root, text="\nBattery\nNumber", width=10)
tooltip_batNum = Pmw.Balloon(root) #Calling the tooltip
tooltip_batNum.bind(batNum,'Battery number as defined by the connector on the interface PCB\r')
batNum.config(font=("Courier", 10))
batNum.place(x=10, y=80)

RCA = Label(root, text="Remaining\nCapacity\nAlarm", width=12)
tooltip_RCA = Pmw.Balloon(root) #Calling the tooltip
tooltip_RCA.bind(RCA,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r'
             'Low Capacity value, the Smart Battery sends AlarmWarning() messages to the SMBus Host with the\r'
             'REMAINING_CAPACITY_ALARM bit set. A Low Capacity value of 0 disables this alarm.\r'
             '(If the ALARM_MODE bit is set in BatteryMode() then the AlarmWarning() message is disabled for a set\r'
             'period of time. See the BatteryMode() function for further information.)\r'
             'The Low Capacity value is set to 10% of design capacity at time of manufacture. The Low Capacity value\r'
             'will remain unchanged until altered by the RemainingCapacityAlarm() function. The Low Capacity value\r'
             'may be expressed in either capacity (mAh) or power (10mWh) depending on the setting of the\r'
             'The RemainingCapacityAlarm() function can be used by systems to indicate a first level near end of\r'
             'discharge state. Since the alarm and the RemainingCapacity() value itself are expressed at C/5 or P/5\r'
             'discharge rates, the value may not directly correspond to the actual present discharge rate. Although this\r'
             'provides a finely controlled alarm set-point, the RemainingTimeAlarm() and related time functions are\r'
             'better suited to for indicating at which point a system should transition into a suspend or hibernate state.\r'
             'The Low Capacity value can be read to verify the value in use by the Smart Battery\'s Low Capacity alarm.\r)')
RCA.config(font=("Courier", 10))
RCA.place(x=110, y=80)

RTA = Label(root, text="Remaining\nTime\nAlarm", width=12)
tooltip_RTA = Pmw.Balloon(root) #Calling the tooltip
tooltip_RTA.bind(RTA,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
RTA.config(font=("Courier", 10))
RTA.place(x=210, y=80)

BM = Label(root, text="\nBattery\nMode", width=12)
tooltip_BM = Pmw.Balloon(root) #Calling the tooltip
BM.bind(BM,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
BM.config(font=("Courier", 10))
BM.place(x=310, y=80)

AR = Label(root, text="\nAt\nRate", width=12) #04
tooltip_AR = Pmw.Balloon(root) #Calling the tooltip
tooltip_AR.bind(AR,'Since the AtRate() function is the first half of a two-function call-set, it is followed by the second function\r'
                'of the call-set that calculates and returns a value based on the AtRate value and the battery\'s present state:\r'
                'When the AtRate value is positive, the AtRateTimeToFull() function returns the predicted time to fullcharge\r'
                'at the AtRate value of charge. (This does NOT include the present charge or discharge rate and\r'
                'so is calculated independently from the present charge or discharge rate of the battery.)\r'
                'When the AtRate value is negative, the AtRateTimeToEmpty() function returns the predicted operating\r'
                'time at the AtRate value of discharge. (This does NOT include the present charge or discharge rate and\r'
                'so is calculated independently from the present charge or discharge rate of the battery.)\r'
                'When the AtRate value is negative, the AtRateOK() function returns a Boolean value that predicts the\r'
                'battery\'s ability to supply the AtRate value of additional discharge energy (current or power) for a\r'
                'minimum of 10 seconds. (This DOES include the present discharge rate of the battery and so is\r'
                'calculated differently from the previous \‘Time\’ values listed.)\r')
AR.config(font=("Courier", 10))
AR.place(x=410, y=80)

ARTTF = Label(root, text="At Rate\nTime to\nFull", width=12)
tooltip_ARTTF = Pmw.Balloon(root) #Calling the tooltip
tooltip_ARTTF.bind(ARTTF,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
ARTTF.config(font=("Courier", 10))
ARTTF.place(x=510, y=80)

ARTTE = Label(root, text="At Rate\nTime to\nEmpty", width=12)
tooltip_ARTTE = Pmw.Balloon(root) #Calling the tooltip
tooltip_ARTTE.bind(ARTTE,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
ARTTE.config(font=("Courier", 10))
ARTTE.place(x=610, y=80)

AROK = Label(root, text="\nAt Rate\nOK", width=12)
tooltip_AROK = Pmw.Balloon(root) #Calling the tooltip
tooltip_AROK.bind(AROK,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
AROK.config(font=("Courier", 10))
AROK.place(x=710, y=80)

TempC = Label(root, text="\nTemperature\nC\u00b0", width=12)
tooltip_1 = Pmw.Balloon(root) #Calling the tooltip
tooltip_1.bind(TempC,'Returns the cell-pack\'s internal temperature (°K). Converted for display to °C. The actual operational\r'
                     'temperature range will be defined at a pack level by a particular manufacturer.\r'
                     'Purpose:\r'
                     'The Temperature() function provides accurate cell temperatures for use by battery chargers and thermal\r'
                     'management systems. A battery charger will be able to use the temperature as a safety check. Thermal\r'
                     'management systems may use the temperature because the battery is one of the largest thermal sources in a\r'
                     'system. (Kelvin units are used to facilitate simple unsigned handling of temperature information and to\r'
                     'permit easy conversion to other units.)\r')
TempC.config(font=("Courier", 10))
TempC.place(x=810, y=80)

Volts = Label(root, text="\n\nVolts\n", width=12)
tooltip_Volts = Pmw.Balloon(root) #Calling the tooltip
tooltip_Volts.bind(Volts,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
Volts.config(font=("Courier", 10))
Volts.place(x=910, y=80)

Amps = Label(root, text="\n\nAmps", width=12)
tooltip_Amps = Pmw.Balloon(root) #Calling the tooltip
tooltip_Amps.bind(Amps,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
Amps.config(font=("Courier", 10))
Amps.place(x=1010, y=80)

AAmps = Label(root, text="\nAverage\nAmps", width=12)
tooltip_AAmps = Pmw.Balloon(root) #Calling the tooltip
tooltip_AAmps.bind(AAmps,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
AAmps.config(font=("Courier", 10))
AAmps.place(x=1110, y=80)

MaxErr = Label(root, text="\nMax\n% of Error", width=12)
tooltip_MaxErr = Pmw.Balloon(root) #Calling the tooltip
tooltip_MaxErr.bind(MaxErr,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
MaxErr.config(font=("Courier", 10))
MaxErr.place(x=1210, y=80)

RSOC = Label(root, text="Relative\nState of\nCharge %", width=12)
tooltip_RSOC = Pmw.Balloon(root) #Calling the tooltip
tooltip_RSOC.bind(RSOC,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
RSOC.config(font=("Courier", 10))
RSOC.place(x=1310, y=80)

batNum = Label(root, text="\nBattery\nNumber", width=10)
batNum.config(font=("Courier", 10))
batNum.place(x=10, y=400)

ASOC = Label(root, text="Absolute\nState of\nCharge %", width=12)
tooltip_ASOC = Pmw.Balloon(root) #Calling the tooltip
tooltip_ASOC.bind(ASOC,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
ASOC.config(font=("Courier", 10))
ASOC.place(x=110, y=400)

RC = Label(root, text="Remaining\nCapacity\nAmps", width=12)
tooltip_RC = Pmw.Balloon(root) #Calling the tooltip
tooltip_RC.bind(RC,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
RC.config(font=("Courier", 10))
RC.place(x=210, y=400)

FCC = Label(root, text="Full\nCharge\nCapacity (A)", width=12)
tooltip_FCC = Pmw.Balloon(root) #Calling the tooltip
tooltip_FCC.bind(FCC,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
FCC.config(font=("Courier", 10))
FCC.place(x=310, y=400)

RTTE = Label(root, text="Remaining\nTime to\nEmpty", width=12)
tooltip_RTTE = Pmw.Balloon(root) #Calling the tooltip
tooltip_RTTE.bind(RTTE,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
RTTE.config(font=("Courier", 10))
RTTE.place(x=410, y=400)

ATTE = Label(root, text="Average\nTime to\nEmpty", width=12)
tooltip_ATTE = Pmw.Balloon(root) #Calling the tooltip
tooltip_ATTE.bind(ATTE,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
ATTE.config(font=("Courier", 10))
ATTE.place(x=510, y=400)

ATTF = Label(root, text="Average\nTime to\nFull", width=12)
tooltip_ATTF = Pmw.Balloon(root) #Calling the tooltip
tooltip_ATTF.bind(ATTF,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
ATTF.config(font=("Courier", 10))
ATTF.place(x=610, y=400)

CC = Label(root, text="\nCharging\nCurrent", width=12)
tooltip_CC = Pmw.Balloon(root) #Calling the tooltip
tooltip_CC.bind(CC,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
CC.config(font=("Courier", 10))
CC.place(x=710, y=400)

CV = Label(root, text="\nCharging\nVoltage", width=12)
tooltip_CV = Pmw.Balloon(root) #Calling the tooltip
tooltip_CV.bind(CV,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
CV.config(font=("Courier", 10))
CV.place(x=810, y=400)

BattStat = Label(root, text="\nBattery\nStatus", width=12)
tooltip_BattStat = Pmw.Balloon(root) #Calling the tooltip
tooltip_BattStat.bind(BattStat,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
BattStat.config(font=("Courier", 10))
BattStat.place(x=910, y=400)

Cycles = Label(root, text="\nCycle\nCount", width=12)
tooltip_Cycles = Pmw.Balloon(root) #Calling the tooltip
tooltip_Cycles.bind(Cycles,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
Cycles.config(font=("Courier", 10))
Cycles.place(x=1010, y=400)

SerNum = Label(root, text="\nSerial\nNumber", width=12)
tooltip_SerNum = Pmw.Balloon(root) #Calling the tooltip
tooltip_SerNum.bind(SerNum,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
SerNum.config(font=("Courier", 10))
SerNum.place(x=1110, y=400)

ManufName = Label(root, text="\nDate\n\n", width=12)
tooltip_ManufName = Pmw.Balloon(root) #Calling the tooltip
tooltip_ManufName.bind(ManufName,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
ManufName.config(font=("Courier", 10))
ManufName.place(x=1210, y=400)

timeOfDay = Label(root, text="\nTime\n\n", width=12)
tooltip_timeOfDay = Pmw.Balloon(root) #Calling the tooltip
tooltip_timeOfDay.bind(timeOfDay,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
timeOfDay.config(font=("Courier", 10))
timeOfDay.place(x=1310, y=400)

# cb = ttk.Combobox(root, values=serial_ports())
# tooltip_timeOfDay = Pmw.Balloon(root) #Calling the tooltip
# tooltip_timeOfDay.bind(timeOfDay,'Gets the Low Capacity alarm threshold value. Whenever the RemainingCapacity() falls below the\r')
# cb.place(x=10,y=50)
# cb.bind('<<ComboboxSelected>>', on_select)
#
# voltageScale="16.5"
# scale = Scale(root,orient=HORIZONTAL,from_=0,to=17,tickinterval=0.5, variable=voltageScale)
# scale.place(x=10, y=100)
# lambda: scale(voltageScale.get())

##getSerUpdate(port)
GetData_Button = Button(root, text="Get latest Data from Battery", command=lambda:[getSerUpdate(port), updateMatrix()])
GetData_Button.place(x=700, y=10)

Directory_Button = Button(root, text="Set working Directory", command=lambda:GetExperimentDir())
Directory_Button.place(x=10, y=10)

UpdateDisplay = Button(root, text="Update Display", command=lambda:updateMatrix())
UpdateDisplay.place(x=1000, y=10)

UpdateDisplay = Button(root, text="Get Data", command=lambda:getSerUpdate(port))
UpdateDisplay.place(x=1300, y=10)

quitButton = Button(root, text="Quit", command=lambda: root.destroy())
quitButton.place(x=1400, y=10)

hourSet = tk.StringVar(value=0)
Hour_spin_box = ttk.Spinbox(root,from_=0,to=24,textvariable=hourSet,wrap=True, width=2, text="Hour")
Hour_spin_box.place(x=200,y=30)
HourSpinLabel = Label(root, text="Hour").place(x=200,y=5)

minuteSet = tk.StringVar(value=0)
Minute_spin_box = ttk.Spinbox(root,from_=0,to=59,textvariable=minuteSet,wrap=True, width=2, text="Minute")
Minute_spin_box.place(x=250,y=30)
MinuteSpinLabel = Label(root, text="Minute").place(x=250,y=5)

yearSet = tk.StringVar(value=2022)
Year_spin_box = ttk.Spinbox(root,from_=2022,to=2050,textvariable=yearSet,wrap=True, width=4, text="Year")
Year_spin_box.place(x=300,y=30)
YearSpinLabel = Label(root, text="Year").place(x=300,y=5)

monthSet = tk.StringVar(value=1)
Month_spin_box = ttk.Spinbox(root,from_=1,to=12,textvariable=monthSet,wrap=True, width=2, text="Month")
Month_spin_box.place(x=350,y=30)
MonthSpinLabel = Label(root, text="Month").place(x=350,y=5)

daySet = tk.StringVar(value=1)
Day_spin_box = ttk.Spinbox(root,from_=1,to=31,textvariable=daySet,wrap=True, width=2, text="Day")
Day_spin_box.place(x=400,y=30)
DaySpinLabel = Label(root, text="Day").place(x=400,y=5)

dtString = tk.StringVar()
dtString=(daySet,monthSet,yearSet,hourSet,minuteSet)

timeSetInstructionLabel=Label(root,text="Adjust for Time and Day").place(x=200,y=50)
setTime = Button(root,text="Set Rasberry\nPI Clock", command=lambda: dateTimeSet(dtString)).place(x=450, y=20)

autoUpdate()

tk.mainloop()

