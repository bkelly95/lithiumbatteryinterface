import tkinter as tk
import tkinter.ttk as ttk
#from tkinter import Entry
from tkinter.ttk import *
import serial
import serial.tools.list_ports
from tkinter import *
from tkinter import filedialog
import re
import numpy as np
import ast
import csv
import pandas as pd
import os
from datetime import datetime

# datetime object containing current date and time
now = datetime.now()
dt_string = now.strftime("%d/%m/%Y %H:%M:%S")

root = tk.Tk()
root.title("Lithium Battery User Interface (LBUI)")

exitButton = Button(root, text="Quit", bg="white", fg="red", command=lambda: root.destroy()).place(x=1300, y=10)

w, h = root.winfo_screenwidth(), root.winfo_screenheight()
root.geometry("%dx%d+0+0" % (w, h))

volts8 = tk.StringVar()
volts8.set("----")

rows, cols = (12, 30)
battMatrix = [[0] * cols] * rows

##############################################################################
# Name          : Declare base file name
# Purpose       : Base file name for all Factory Acceptance Test (FAT) names
# Author	    : bkelly@whoi.edu
##############################################################################
# nameFile = StringVar()
# #nameFile = ""
# saveMAT = Entry(root, bg="white", textvariable=nameFile, width=68)
# saveMAT.place(x=800, y=10)
# saveMAT.set = ""
# saveMATlabel = Label(root, text="Please insert the BASE file name\nThe base file name must only\ncontain letters and/or numbers", fg="green").place(x=600, y=10)
#
# ExperimentDir = StringVar()

##############################################################################
# Name          : GetExperimentDir
# Purpose       : Select the experiment directory for saving log files
# Author	    : bkelly@whoi.edu
##############################################################################
def GetExperimentDir():
    global ExperimentDir
    ExperimentDir = filedialog.askdirectory(parent=root, initialdir="C:/Users/Brian/Documents/Projects/OOI/LithiumBatteryInterface", title='Please select project directory')
    Button(root, text=ExperimentDir, width=60, command=lambda: GetExperimentDir())
    ExpDir = Label(root, text=ExperimentDir, relief=FLAT, anchor="w", width=60)
    ExpDir.place(x=200, y=10)
    return ExperimentDir

def prepend_line(file_name, line):
    """ Insert given string as a new line at the beginning of a file """
    # define name of temporary dummy file
    dummy_file = file_name + '.bak'
    # open original file in read mode and dummy file in write mode
    with open(file_name, 'r') as read_obj, open(dummy_file, 'w') as write_obj:
        # Write given line to the dummy file
        write_obj.write(line)
        # Read lines from original file one by one and append them to the dummy file
        for line in read_obj:
            write_obj.write(line)
    # remove original file
    os.remove(file_name)
    # Rename dummy file as the original file
    os.rename(dummy_file, file_name)

def getSerUpdate():
    ser = serial.Serial('com11', 115200, timeout=1)
    count = 0
    while (count < 13):
        count = count + 1
        lbdata = ser.readline()
        lbdata = lbdata.decode("utf-8")
        start = lbdata.find("#")
        end = lbdata.find(";")
        lbdata = lbdata.replace(' ', ',')
        lbdata = lbdata.replace('#', '')
        lbdata = lbdata.replace(',;', '\r')
        print(lbdata)
        #with open('battData.csv', 'a') as fd:
        prepend_line('battData.csv', lbdata)
            #fd.write(lbdata)
            #fd.close()
        # with open('battData.csv', newline='') as csvfile:
        #     rdr = csv.reader(csvfile)
        #     l = sorted(rdr, key=lambda x: x[6], reverse=True)

##def updateMatrix():
    battData = pd.read_csv("battData.csv", header = None) ##Battery data csv read into battData array
    for i in range(0, 12): ## Cycle through the battery data
        bat = battData.iloc[i, 0] ##battery number (Character)
        batInt = int(bat) ##Convert battery number to integer
        if (batInt == 1): ##Determine which battery of data and there for what is the y position to place that data on the screen
            yy = 140
        elif (batInt == 2):
            yy = 160
        elif (batInt == 3):
            yy = 180
        elif (batInt == 4):
            yy = 200
        elif (batInt == 5):
            yy = 220
        elif (batInt == 6):
            yy = 240
        elif (batInt == 7):
            yy = 260
        elif (batInt == 8):
            yy = 280
        elif (batInt == 9):
            yy = 300
        elif (batInt == 10):
            yy = 320
        elif (batInt == 11):
            yy = 340
        elif (batInt == 12):
            yy = 360
        else:
            print("Less than 12 batteries in the string")
        l = 5
        for zz in range(0, 14):  ## Cycle through the battery data
            bat = (battData.iloc[i, zz])
            batData2Array = Label(root, text=bat, width=15)
            if (i % 2) == 0:
                labelColor = "green"
            else:
                labelColor = "blue"
            batData2Array.config(font=("Courier", 10), foreground=labelColor)
            batData2Array.place(x=l, y=yy)
            if zz == 0:
                batData2Array = Label(root, text=bat, width=15)
                if (i % 2) == 0:
                    labelColor = "green"
                else:
                    labelColor = "blue"
                batData2Array.config(font=("Courier", 10), foreground=labelColor)
                batData2Array.place(x=5, y=(yy+340))
            l = l + 100

    battData = pd.read_csv("battData.csv", header = None) ##Battery data csv read into battData array
    for i in range(0, 12): ## Cycle through the battery data
        bat = battData.iloc[i, 0] ##battery number (Character)
        batInt = int(bat) ##Convert battery number to integer
        if (batInt == 1): ##Determine which battery of data and there for what is the y position to place that data on the screen
            yy = 480
        elif (batInt == 2):
            yy = 500
        elif (batInt == 3):
            yy = 520
        elif (batInt == 4):
            yy = 540
        elif (batInt == 5):
            yy = 560
        elif (batInt == 6):
            yy = 580
        elif (batInt == 7):
            yy = 600
        elif (batInt == 8):
            yy = 620
        elif (batInt == 9):
            yy = 640
        elif (batInt == 10):
            yy = 660
        elif (batInt == 11):
            yy = 680
        elif (batInt == 12):
            yy = 700
        else:
            print("Less than 12 batteries in the string")
        l = 110
        for zz in range(15, 28):  ## Cycle through the battery data
            bat = (battData.iloc[i, zz])
            batData2Array = Label(root, text=bat, width=15)
            if (i % 2) == 0:
                labelColor = "green"
            else:
                labelColor = "blue"
            batData2Array.config(font=("Courier", 10),foreground=labelColor)
            batData2Array.place(x=l, y=yy)
            l = l + 100

batNum = Label(root, text="\nBattery\nNumber", width=10)
batNum.config(font=("Courier", 10))
batNum.place(x=10, y=60)

RCA = Label(root, text="Remaining\nCapacity\nAlarm", width=12)
RCA.config(font=("Courier", 10))
RCA.place(x=110, y=60)

RTA = Label(root, text="Remaining\nTime\nAlarm", width=12)
RTA.config(font=("Courier", 10))
RTA.place(x=210, y=60)

BM = Label(root, text="\nBattery\nMode", width=12)
BM.config(font=("Courier", 10))
BM.place(x=310, y=60)

AR = Label(root, text="\nAt\nRate", width=12)
AR.config(font=("Courier", 10))
AR.place(x=410, y=60)

ARTTF = Label(root, text="At Rate\nTime to\nFull", width=12)
ARTTF.config(font=("Courier", 10))
ARTTF.place(x=510, y=60)

ARTTE = Label(root, text="At Rate\nTime to\nEmpty", width=12)
ARTTE.config(font=("Courier", 10))
ARTTE.place(x=610, y=60)

AROK = Label(root, text="\nAt Rate\nOK", width=12)
AROK.config(font=("Courier", 10))
AROK.place(x=710, y=60)

TempC = Label(root, text="\nTemperature\nin C", width=12)
TempC.config(font=("Courier", 10))
TempC.place(x=810, y=60)

Volts = Label(root, text="\n\nmili Volts\n", width=12)
Volts.config(font=("Courier", 10))
Volts.place(x=910, y=60)

Amps = Label(root, text="\n\nmili Amps", width=12)
Amps.config(font=("Courier", 10))
Amps.place(x=1010, y=60)

AAmps = Label(root, text="\nAverage\nMilli Amps", width=12)
AAmps.config(font=("Courier", 10))
AAmps.place(x=1110, y=60)

MaxErr = Label(root, text="\nMax\nError", width=12)
MaxErr.config(font=("Courier", 10))
MaxErr.place(x=1210, y=60)

RSOC = Label(root, text="Relative\nState of\nCharge", width=12)
RSOC.config(font=("Courier", 10))
RSOC.place(x=1310, y=60)

batNum = Label(root, text="\nBattery\nNumber", width=10)
batNum.config(font=("Courier", 10))
batNum.place(x=10, y=400)

ASOC = Label(root, text="Absolute\nState of\nCharge", width=12)
ASOC.config(font=("Courier", 10))
ASOC.place(x=110, y=400)

RC = Label(root, text="\nRemaining\nCapacity", width=12)
RC.config(font=("Courier", 10))
RC.place(x=210, y=400)

FCC = Label(root, text="Full\nCharge\nCapacity", width=12)
FCC.config(font=("Courier", 10))
FCC.place(x=310, y=400)

RTTE = Label(root, text="Remaining\nTime to\nEmpty", width=12)
RTTE.config(font=("Courier", 10))
RTTE.place(x=410, y=400)

ATTE = Label(root, text="Average\nTime to\nEmpty", width=12)
ATTE.config(font=("Courier", 10))
ATTE.place(x=510, y=400)

ATTF = Label(root, text="Average\nTime to\nFull", width=12)
ATTF.config(font=("Courier", 10))
ATTF.place(x=610, y=400)

CC = Label(root, text="\nCharging\nCurrent", width=12)
CC.config(font=("Courier", 10))
CC.place(x=710, y=400)

CV = Label(root, text="\nCharging\nVoltage", width=12)
CV.config(font=("Courier", 10))
CV.place(x=810, y=400)

BattStat = Label(root, text="\nBattery\nStatus", width=12)
BattStat.config(font=("Courier", 10))
BattStat.place(x=910, y=400)

Cycles = Label(root, text="\nCycle\nCount", width=12)
Cycles.config(font=("Courier", 10))
Cycles.place(x=1010, y=400)

ManDate = Label(root, text="\nManufacturer\nDate", width=12)
ManDate.config(font=("Courier", 10))
ManDate.place(x=1110, y=400)

SerNum = Label(root, text="\nSerial\nNumber", width=12)
SerNum.config(font=("Courier", 10))
SerNum.place(x=1210, y=400)

ManufName = Label(root, text="\nManufacturer\nName", width=12)
ManufName.config(font=("Courier", 10))
ManufName.place(x=1310, y=400)
#
# Chem = Label(root, text="\nBattery\nChemistry", width=12)
# Chem.config(font=("Courier", 10))
# Chem.place(x=1410, y=400)

##getSerUpdate()
GetData_Button = Button(root, text="Get latest Data from Battery", command=lambda:getSerUpdate())
GetData_Button.place(x=700, y=10)

Directory_Button = Button(root, text="Set working Directory", command=lambda:GetExperimentDir())
Directory_Button.place(x=50, y=10)

##UpdateDisplay = Button(root, text="Update Display", command=lambda:updateMatrix())
##UpdateDisplay.place(x=1000, y=10)


tk.mainloop()
