extern double SCD41_humidity;
extern double SCD41_temp;

// Function prototypes
int SCD41_init_humidity();
int SCD41_init_temp();
int SCD41_get_humidity();
int SCD41_get_temp();
int SCD41_init();

#define SCD41_address 0x62
