/*********************************************************************
 * FileName: 		common.h
 * Dependencies: 	None
 * Overview:		Sets up global variables to be used in "main.c". 
 *
 *
 ********************************************************************/

#define ON_AL 0
#define OFF_AL 1

#define ON 1
#define OFF 0

// ***************** User-configurable parameters ********************

// Define board (CPM, DCL, STC)
#define setboard_DCL

// Allow CE wake from any DCL
//#define wake_from_DCL

// Allow CE wake from STC channels 1 and 2 (level-sensitive, polled; no wakemask bit for STC ch. 1, bit 10 for STC ch. 2)
#define wake_from_STC_chan

// Allow STC wakes from serial port taps (ports 3,5,7)
//#define wake_from_STC_serial_chan

// Power on iridium at boot (supervisor / STC only)
//#define iridium_on_boot

// Power on freewave / wifi at boot (supervisor / STC only)
#define fwwf_on_boot

// Maintains console channel regardless of pwron/off fwwf.
//#define console_port_override

// Enable default heartbeat values on any boot. CPM/STC only.
#define heartbeats_enabled_on_boot

// Sets correct CEcon polarity.
#define CE_soft_start

// Define board versions. "1" = intial version.
#define CPM_version 3
#define DCL_version 3
#define STC_version 2

// Enables / disables soft start of irid / fwwf.
#define soft_start_irid 1
#define soft_start_fwwf 0

// Sync board updates with streaming data (otherwise runs on independent timer)
#define sync_status

// Enables startup filter of CE UART.
#define CE_UART_startup_filter

// Preset max invalid characters to filter on startup / CE boot
#define CE_UART_filter_count	5

// Starts GPS on any STC CE boot
#define STC_GPS_on_boot
// *******************************************************************

#define _CPM 0
#define _DCL 1
#define _STC 2

// Defines polarity of FET enabling CE. (If CE soft-start is used polarity must be active-high).
#ifdef CE_soft_start
#define CE_ON		ON
#define CE_OFF	 	OFF
#else 
#define CE_ON		ON_AL
#define CE_OFF	 	OFF_AL
#endif

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "math.h"

extern long MP_errors;
extern long MP_DCL_errors;

const extern int BOARDTYPE;

extern int CPM_PRIMARY_SEL;

#define fw_version "2.09"

extern int debug;

extern int CPMTYPE;
extern int CPM2_PSC_ENABLED;
extern int CPM1_PSC_ENABLED;

// ******** Debug constants ********

// Debug CPM.
// #define cpm_debug

// Debug heartbeats.
// #define hbeat_debug

// Debug CE wake.
// #define CE_wake_debug

// Debug ground faults
// #define gflt_debug

// Debug SBD.
// #define sbd_debug

// Debug channel pics.
// #define chpic_debug

// Debug watchdog.
// #define watchdog_debug

// Debug external wake.
// #define external_wake_debug

// Debug SBD9602 fifo.
// #define sbd9602_fifo_debug

// Debug wake sources.
// #define wake_src_debug

// Debug LT4151
// #define LT4151_debug

// Debug SHT25
// #define SHT25_debug

// Debug CE wake timer
// #define ce_timer_debug

// Debug DPB
// #define dpb_debug

// *********************************


// Global supervisor error codes (sets up bitmask)

/*1*/ #define err_SBD_hardware_failure 0x1
/*2*/ #define err_SBD_antenna 0x2
/*3*/ #define err_SBD_comms 0x4
/*4*/ #define err_SBD_session_time 0x8
/*5*/ #define err_SBD_bad_cmd_rec 0x10

/*6*/ #define err_Vmain_range 0x20
/*7*/ #define err_Imain_range 0x40
/*8*/ #define err_Vbatt_range 0x80
/*9*/ #define err_Ibatt_range 0x100

/*10*/ #define err_PPS_Seascan 0x200
/*11*/ #define err_PPS_gps 0x400

/*12*/ #define err_wake 0x800

/*13*/ #define err_PSC_rec_string 0x1000
/*14*/ #define err_PSC_Vmain_track 0x2000
/*15*/ #define err_avail_1 0x4000

/*16*/ #define err_CPM_hbeat 0x8000
/*17*/ #define err_CPM_restarting 0x10000

/*18*/ #define err_gflt_SBD5V 0x20000
/*19*/ #define err_gflt_SBD_gnd 0x40000
/*20*/ #define err_gflt_GPS3V3 0x80000
/*21*/ #define err_gflt_GPS_gnd 0x100000
/*22*/ #define err_gflt_Vmain 0x200000
/*23*/ #define err_gflt_Vmain_gnd 0x400000
/*24*/ #define err_gflt_inst_12_24 0x800000
/*25*/ #define err_gflt_inst_gnd 0x1000000

/*26*/ #define err_leak0 0x2000000
/*27*/ #define err_leak1 0x4000000
/*28*/ #define err_I2C 0x8000000
/*29*/ #define err_uart 0x10000000
/*30*/ #define err_CPM_fubar 0x20000000  
/*31*/ #define err_aux_cpic_overcurrent 0x40000000 
/*32*/ #define err_BOR_reset 0x80000000


// Global DCL error codes


/*1*/ #define dcl_err_Vmain_range 0x1
/*2*/ #define dcl_err_Imain_range 0x2

/*3*/ #define dcl_err_gflt_vsrc_1_h 0x4
/*4*/ #define dcl_err_gflt_vsrc_1_gnd 0x8
/*5*/ #define dcl_err_gflt_vsrc_2_h 0x10
/*6*/ #define dcl_err_gflt_vsrc_2_gnd 0x20
/*7*/ #define dcl_err_gflt_vsrc_3_h 0x40
/*8*/ #define dcl_err_gflt_vsrc_3_gnd 0x80
/*9*/ #define dcl_err_gflt_vsrc_4_h 0x100
/*10*/ #define dcl_err_gflt_vsrc_4_gnd 0x200


/*11*/ #define dcl_err_leak0  0x400
/*12*/ #define dcl_err_leak1 0x800

/*13*/ #define err_cpic_overcurrent 0x1000

/*14*/ #define err_cpic_1_rx 0x2000
/*15*/ #define err_cpic_2_rx 0x4000
/*16*/ #define err_cpic_3_rx 0x8000
/*17*/ #define err_cpic_4_rx 0x10000
/*18*/ #define err_cpic_5_rx 0x20000
/*19*/ #define err_cpic_6_rx 0x40000
/*20*/ #define err_cpic_7_rx 0x80000
/*21*/ #define err_cpic_8_rx 0x100000

/*22*/ #define dcl_err_I2C 0x200000
/*23*/ #define dcl_err_uart 0x400000
/*24*/ #define dcl_err_DPB_I2C_init 0x800000
/*25*/ #define dcl_err_DPB_I2C_brdon 0x1000000

/*32*/ #define dcl_err_BOR_reset 0x80000000


void set_error(long err);
void clear_error(long err);

void set_dcl_error(long err);
void clear_dcl_error(long err);

void clear_all_errors ();









