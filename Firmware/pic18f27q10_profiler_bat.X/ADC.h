#include <xc.h>
#include <pic18.h>


// ***************** User-configurable parameters ********************


// *******************************************************************

void set_ADC();
void start_ADC();

extern int hydrogenAlarm;

extern double hydrogenVoltage;

extern int hydrogen_enabled;

extern int hydrogen_limit;

void check_hydrogen_voltage();
