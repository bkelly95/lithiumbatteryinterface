/*********************************************************************
 * FileName: 		I2C.h
 * Dependencies: 	None
 * Overview:		Defines function prototypes and global variables. 
 *
 ********************************************************************/

// Function prototypes.

void setI2C ();
void i2cstart();
void i2cstop();
void i2cgetack();
int i2cgetack_initial();
void i2csendack();
void i2csendnack();
void i2cwrite(unsigned char c);
unsigned char i2cread();
extern int I2C_error;
extern int I2C_error_latch;
extern int I2C_bus_init;

void I2C_init();
void I2C_deinit();
