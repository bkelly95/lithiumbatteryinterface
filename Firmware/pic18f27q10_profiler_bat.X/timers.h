/*********************************************************************
 * FileName: 		timers.h
 * Dependencies: 	None
 * Overview:		Sets up global variables to be used in "main.c". 
 *
 *
 ********************************************************************/


// ***************** User-configurable parameters ********************

 // max seconds between PSC strings before error flag is set
#define PSC_max_string_delt 30

// seconds between SBD status transmissions (60 min)
#define SBD_default_status_msg_trig 3600

// seconds between checking for incoming SBD messages (20 min)
#define SBD_default_rec_msg_trig 1200

// max restart attempts of CE following no heartbeat
#define CPM_max_no_hbeat_restarts 5

// default seconds between testing seawater ground-faults (5 min)
#define default_gflt_trig_T 300

// default seconds between board sensor measurements
#define default_board_sensor_T 2

// *******************************************************************

// Function prototypes

extern int I2C_timeout;
void I2C_watchdog();
void blocking_delay (unsigned int milliseconds);
void set_timers();
void non_blocking_delay_A(int delay_sec);
void reset_timer_A();
void reset_CE_wake_timer();


void one_shot_non_blocking_delay_B(int milliseconds);

void disable_heartbeat_timer();
void enable_heartbeat_timer();

// Non-blocking timer values

extern int timer_A_idle;
extern int timer_A_alarm;
extern int timer_A_val;

extern int timer_B_alarm;

// Run time

extern unsigned long elapsed_time;

// CPM heartbeat variables

extern int CPM_heartbeats_enabled;
extern int CPM_heartbeat_del_t;
extern int CPM_heartbeat_threshold;
extern int CPM_missed_heartbeat_count;
extern int CPM_heartbeat_alarm;
extern int CPM_heartbeat_sec_count;
extern int CPM_switch;
extern int CPM_no_hbeat_force_reboot;
extern int CPM_force_reboot_count;

// CPM wake variables

extern int CPM_off_count;
extern unsigned long CPM_on_count;
extern int CPM_on_alarm;
extern int CPM_off_alarm;

// CPM stream data variables

extern int CE_stream_data;
extern int CE_stream_rate;
extern int CE_send_data;

// PSC string timer variables

extern int PSC_string_rec_alarm;
extern int PSC_sec_count;

// PPS timer variables

extern int pps_count_enabled;
extern int pps_count;

// SBD variables

extern int send_SBD_status;
extern int get_SBD_msg;
extern long SBD_status_alarm_trig;
extern long SBD_receive_trig;
extern long SBD_sec_count;

// GFLT variables
extern int get_gflt;
extern int gflt_trig_T;
extern int gflt_sec_count;

// board sensor variables
extern int poll_board_sensors;
extern int board_sensor_count;
extern int board_sensor_T;





