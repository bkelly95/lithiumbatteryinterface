  /*********************************************************************
 * FileName: 		GPIO_pin_assignments.h
 * Dependencies: 	None
 * Overview:		Assigns netnames defined in port/pin assignment
 *					spreadsheet to proper registers, and defines function
 *					prototype. 
 *
 ********************************************************************/

void set_GPIO_pins (); // function prototype


