/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.81.7
        Device            :  PIC18F26Q10
        Driver Version    :  2.00
 */

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
 */

#include "../pic18.h"
#include "mcc_generated_files/mcc.h"
#include <stdio.h>
#include "I2C.h"
#include "mcc_generated_files/examples/i2c1_master_example.h"
#include "SmartBattery.h"
#include "ADC.h"
#include "mcc_generated_files/i2c1_master.h"
#include "scd4x_i2c.h"
#include "sensirion_common.h"

//void I2C1_WriteByteRegister(i2c1_address_t address, uint8_t reg);
//void I2C1_ReadNBytes(i2c1_address_t address, uint8_t *data, size_t len);

uint16_t SCDco2;
int32_t SCDtemperature;
int32_t SCDhumidity;
int32_t SCDpress;
int I2C_error = 0; // I2C error flag


/*
    Main application
 */

/*********************************************************************
 *		Special function for verifying ACK from slave. A timer is 
 *		started prior to waiting for the ACK; if recieved normally
 *		the device is assumed to be operating properly.
 *
 *		If the timer exceeds 5ms delay the "I2C_timeout" flag is set which
 *		sets an error flag, and all I2C tasks are halted.
 ********************************************************************/


void EUSART1_sendString(const char *str)
{
    while(*str)
    {
        while (!(EUSART1_is_tx_ready()));
        EUSART1_Write(*str++);

    }
}

static void EUSART1_write(uint8_t txData)
{
while(0 == PIR3bits.TX1IF)
{
;
}
TX1REG = txData;
}			    

void main(void) {
    volatile uint8_t rxData; // Initialize the device
    SYSTEM_Initialize();
    TRISBbits.TRISB4 = 0; /* Configure the TRISA4 pin as output */
    TRISBbits.TRISB5 = 0; /* Configure the TRISA4 pin as output */
    LATBbits.LATB4 = 0; /* Drive the output on */
    LATBbits.LATB5 = 0; /* Drive the output off */
    
    // Enable the Global Interrupts
    INTERRUPT_GlobalInterruptEnable();
    
    // Enable the Peripheral Interrupts
    INTERRUPT_PeripheralInterruptEnable();

    _delay(400000);
    if (EUSART1_is_tx_ready()) {
        LATBbits.LATB4 = 1; /* Drive the output off */
        LATBbits.LATB5 = 0; /* Drive the output on */
        
    }

    while (1) {
        getBattData ();
    }
}
    /**
     End of File
     */

