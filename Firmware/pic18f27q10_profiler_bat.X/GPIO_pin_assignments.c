/*********************************************************************
 * FileName: 		GPIO_pin_assignments.c
 * Dependencies: 	GPIO_pin_assignments.h (include here and in main)
 *					common.h (include here and in main)
 *
 ********************************************************************/


/*********************************************************************
 * Function:        	void set_GPIO_pins (void)
 * PreCondition:    	None.
 * Input:           	None.
 * Output:          	None.
 * Side Effects:    	None.
 * Overview:       		Sets up digital GPIO pins. TRISx register sets
 *						direction; LATx register used for port writes 
 *						and PORTx register used for port reads. External
 *						interrupts also assigned here.
 *
 ********************************************************************/

#include "GPIO_pin_assignments.h"
#include "common.h"

void set_GPIO_pins (void) {

// PIN 13 (RC2 - Input - Hydrogen)
TRISCbits.TRISC2 = 1; // Input

}


