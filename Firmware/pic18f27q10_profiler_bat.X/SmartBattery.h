#include "mcc_generated_files/mcc.h"
#include <stdio.h>
#include "I2C.h"
#include "mcc_generated_files/examples/i2c1_master_example.h"
#include "./mcc_generated_files/i2c1_master.h"

/*
      unsigned int RemainingCapacityAlarm = 0;                            // cmd 01   r/w   - mAh
      unsigned int RemainingTimeAlarm     = 0;                            // cmd 02   r/w   - min
      unsigned int BatteryMode            = 0;                            // cmd 03   r/w   - bitfield
        signed int AtRate                 = 0;                            // cmd 04   r/w   - mA
      unsigned int AtRateTimeToFull       = 0xFFFF;                       // cmd 05   r     - min
      unsigned int AtRateTimeToEmpty      = 0xFFFF;                       // cmd 06   r     - min
      unsigned int AtRateOK               = 0x0001;                       // cmd 07   r     - boolean
const unsigned int Temperature            = 0;                            // cmd 08   r     - 10*�K
      unsigned int Voltage                = 0;                            // cmd 09   r     - mV 5000mV
        signed int Current                = 0;                            // cmd 0A   r     - mA 1234mA
        signed int AverageCurrent         = 0;                            // cmd 0B   r     - 1 minute average current
const unsigned int MaxError               = 10;                           // cmd 0C   r     - %
      unsigned int RelativeStateOfCharge  = 0;                            // cmd 0D   r     - %
      unsigned int AbsoluteStateOfCharge  = 0;                            // cmd 0E   r     - %
      unsigned int RemainingCapacity      = 0;                            // cmd 0F   r     - mAh
      unsigned int FullChargeCapacity     = 0;                            // cmd 10   r     - mAh
      unsigned int RunTimeToEmpty         = 0;                            // cmd 11   r     - min
      unsigned int AverageTimeToEmpty     = 0;                            // cmd 12   r     - min
      unsigned int AverageTimeToFull      = 0;                            // cmd 13   r     - min
const unsigned int ChargingCurrent        = 0;                            // cmd 14   r     - mA
const unsigned int ChargingVoltage        = 0;                            // cmd 15   r     - nCell * 4200mV
      unsigned int BatteryStatus          = 0;                            // cmd 16   r     - bitfielf
      unsigned int CycleCount             = 0;                            // cmd 17   r     - #
const unsigned int DesignCapacity         = 0;                            // cmd 18   r     - mAh
const unsigned int DesignVoltage          = 0;                            // cmd 19   r     - nCell * 3700mV
const unsigned int SpecificationInfo      = 0;                            // cmd 1A   r     - bitfield
const Date_t       ManufactureDate        = { 29,3,2019-1980 };           // cmd 1B   r     - bitfield
const unsigned int SerialNumber           = 0;                            // cmd 1C   r     - #
const char         ManufacturerName[]     = 0;                            // cmd 20   r     - first byte is the text length
const char         DeviceName[]           = 0;                            // cmd 21   r     - first byte is the text length
const char         DeviceChemistry[]      = 0;                            // cmd 22   r     - first byte is the text length
const char         ManufacturerData[]     = 0;                            // cmd 23   r     - first byte is the data length
 */

unsigned int RemainingCapacityAlarm = 0;
unsigned int RemainingTimeAlarm = 0;
unsigned int BatteryMode = 0;
signed int AtRate = 0;
unsigned int AtRateTimeToFull = 0;
unsigned int AtRateTimeToEmpty = 0;
unsigned int AtRateOK = 0;
float Temperature = 0;
unsigned int Current = 0;
unsigned int Voltage = 0;
signed int AverageCurrent = 0;
unsigned int MaxError = 0;
unsigned int RelativeStateOfCharge  = 0;//%
unsigned int AbsoluteStateOfCharge  = 0;//%
unsigned int RemainingCapacity      = 0;//mAh
unsigned int FullChargeCapacity     = 0;//mAh
unsigned int RunTimeToEmpty         = 0;//min
unsigned int AverageTimeToEmpty     = 0;//min
unsigned int AverageTimeToFull      = 0;//min
unsigned int ChargingCurrent  = 0;//mA
unsigned int ChargingVoltage  = 0;//mV
unsigned int BatteryStatus          = 0;//bitfield
unsigned int CycleCount             = 10;//#
unsigned int DesignCapacity         = 4400;//mAh
unsigned int DesignVoltage          = 11100;//nCell * 3700mV
unsigned int SpecificationInfo      = 0x0021;//bitfield
//const Date_t       ManufactureDate        = { 29,3,2019-1980 };//bitfield
unsigned int SerialNumber           = 0;          //#
char ManufacturerName[30]; //first byte is the text length
char         DeviceName[30]           = {};//first byte is the text length
char         DeviceChemistry[10]      = {};//first byte is the text length
char         ManufacturerData[10]     = {};//first byte is the data length

char muxChipID[2] = {0x77, 0x70}; //SMB Device Address 0x70 = PCA9546, 0x77 = PCA9548
char muxChanID[8] = {0x01,0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80}; //Smart Battery SMB Device Channel 0-7


void setChannel(char dev, char ch); //Select PCA9546 register 0x01 = 1, 0x02 = 2, 0x04 = 3, 0x08 = 4
void getBattData (void);
