/*********************************************************************
 * FileName: 		I2C.c
 * Dependencies: 	I2C.h (include here and in main)
 *					common.h (include here and in main)
 *					timers.h (include here and in main)
 *
 ********************************************************************/


/*********************************************************************
 * Function:        	void i2c(task) (void)
 * PreCondition:    	None.
 * Input:           	None.
 * Output:          	None.
 * Side Effects:    	None.
 * Overview:       		Polled functions for common I2C bus operations.
 *
 ********************************************************************/

#include "common.h"
#include "timers.h"


int I2C_error = 0; // I2C error flag
int I2C_error_latch = 0; // latching I2C error flag - must be cleared in main
int I2C_bus_init = 0;

void setI2C (void) {


// Set SCL / SDA low
TRISGbits.TRISG2 = 0;
TRISGbits.TRISG3 = 0;
LATGbits.LATG2 = 0;
LATGbits.LATG3 = 0;


/*
I2C1CONbits.I2CEN = 0; // temporarily disable
I2C1BRG = 0x13; // set baud for 400KHz FSCL (currently set by LTC4151)
I2C1CONbits.A10M = 0; // 7-bit address;
I2C1CONbits.I2CEN = 1; // enable
*/
}

void I2C_deinit (void){
I2C1CONbits.I2CEN = 0; // disable

// set SCL / SDA low

TRISGbits.TRISG2 = 0;
TRISGbits.TRISG3 = 0;
LATGbits.LATG2 = 0;
LATGbits.LATG3 = 0;
I2C_bus_init = 0;
}

void I2C_init (void){
I2C1CONbits.I2CEN = 0; // temporarily disable
I2C1BRG = 0x13; // set baud for 400KHz FSCL (currently set by LTC4151)
I2C1CONbits.A10M = 0; // 7-bit address;
I2C1CONbits.I2CEN = 1; // enable
blocking_delay(1);
I2C_bus_init =1;
}


void i2cstart(void){

	I2C_watchdog(); // start I2C watchdog timer

	I2C1CONbits.SEN = 1;

	while((I2C1CONbits.SEN==1) && (I2C_timeout == 0));

	if (I2C_timeout == 1) {
	I2C1CONbits.I2CEN = 0; // enable
	I2C1CONbits.I2CEN = 1; // enable
			I2C_error=1;
			I2C_error_latch = 1;
	}
 
}

void i2cstop(void){

	I2C_watchdog(); // start I2C watchdog timer

	I2C1CONbits.PEN = 1;

	while((I2C1CONbits.PEN==1)&& (I2C_timeout == 0));

	if (I2C_timeout == 1) {
	I2C1CONbits.I2CEN = 0; // enable
	I2C1CONbits.I2CEN = 1; // enable
			I2C_error=1;
			I2C_error_latch = 1;
	}
}




/*********************************************************************
 *		Special function for verifying ACK from slave. A timer is 
 *		started prior to waiting for the ACK; if recieved normally
 *		the device is assumed to be operating properly.
 *
 *		If the timer exceeds 5ms delay the "I2C_timeout" flag is set which
 *		sets an error flag, and all I2C tasks are halted.
 ********************************************************************/

void i2cgetack(void){

	I2C_error = 0; // clear the flag
	I2C_watchdog(); // start I2C watchdog timer

// block while both are true
while ((I2C1STATbits.ACKSTAT==1) && (I2C_timeout == 0)); 
	if (I2C_timeout == 1) {
	I2C1CONbits.I2CEN = 0; // enable
	I2C1CONbits.I2CEN = 1; // enable
			I2C_error=1;
			I2C_error_latch = 1;
	}
}

int i2cgetack_initial(void){

	I2C_error = 0; // clear the flag
	I2C_watchdog(); // start I2C watchdog timer

// block while both are true
while ((I2C1STATbits.ACKSTAT==1) && (I2C_timeout == 0)); 
	if (I2C_timeout == 1) {
	I2C1CONbits.I2CEN = 0; // enable
	I2C1CONbits.I2CEN = 1; // enable
return 0;
	}
return 1;
}




void i2csendack(void){

	I2C_watchdog(); // start I2C watchdog timer

	I2C1CONbits.ACKDT=0;
	I2C1CONbits.ACKEN=1;
	while((I2C1CONbits.ACKEN==1)&& (I2C_timeout == 0));

	if (I2C_timeout == 1) {
	I2C1CONbits.I2CEN = 0; // enable
	I2C1CONbits.I2CEN = 1; // enable
			I2C_error=1;
			I2C_error_latch = 1;
	}





}

void i2csendnack(void){

I2C_watchdog(); // start I2C watchdog timer

	I2C1CONbits.ACKDT=1;
	I2C1CONbits.ACKEN=1;
	while((I2C1CONbits.ACKEN==1)&& (I2C_timeout == 0));

	if (I2C_timeout == 1) {
	I2C1CONbits.I2CEN = 0; // enable
	I2C1CONbits.I2CEN = 1; // enable
			I2C_error=1;
			I2C_error_latch = 1;
	}
}

void i2cwrite(unsigned char c){

I2C_watchdog(); // start I2C watchdog timer

	I2C1TRN=c;
	while((I2C1STATbits.TRSTAT==1)&& (I2C_timeout == 0));

	if (I2C_timeout == 1) {
	I2C1CONbits.I2CEN = 0; // enable
	I2C1CONbits.I2CEN = 1; // enable
			I2C_error=1;
			I2C_error_latch = 1;
	}
}

unsigned char i2cread(void){

I2C_watchdog(); // start I2C watchdog timer

	unsigned char c;
	I2C1CONbits.RCEN=1;

	while((I2C1CONbits.RCEN==1)&& (I2C_timeout == 0));
	if (I2C_timeout == 1) {
	I2C1CONbits.I2CEN = 0; // enable
	I2C1CONbits.I2CEN = 1; // enable
			I2C_error=1;
			I2C_error_latch = 1;
			return 0;
	}

	c=I2C1RCV;
	return c;

}





