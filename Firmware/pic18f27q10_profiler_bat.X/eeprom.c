#include "eeprom.h"

unsigned int reset_count;
unsigned int eeprom_bt;
/*

Program memory EEPROM addresses: 7FFE00h to 7FFFFFh (512 addresses).

Apparently restricted to even adddresses only (as is prog memory, word aligned), so
usable addresses are even addresses from 0xFFE0 to FFFF; or 256 addresses of 
16 bits.

An erase must occur before data is written.

Manually introducing the base offset also works:
_erase_eedata(0xFE00 + address, _EE_WORD); 
while(NVMCONbits.WR); 

Additionally dsPIC / PIC30 "built in" routines ARE supported for PIC24 with
EEPROM. Need to include "libpic30.h" in "common.h".


*/

// Reserve all possible addresses.
unsigned int __attribute__((space(eedata), aligned(_EE_WORD*256))) ee_data[_EE_WORD*128];


void write_eeprom (unsigned int address, unsigned int data){
    

    _prog_addressT ee_addr;

_init_prog_address(ee_addr, ee_data);

_erase_eedata(ee_addr + address, _EE_WORD); 
while(NVMCONbits.WR); 

_write_eedata_word(ee_addr + address, data); 
while(NVMCONbits.WR); 


}




unsigned int read_eeprom (unsigned int address){

_prog_addressT ee_addr;
_init_prog_address(ee_addr, ee_data);

// location in RAM
unsigned int ee_data;

_memcpy_p2d16(&ee_data, ee_addr + address, _EE_WORD );

return ee_data;
}




void sync_reset_count (void){

unsigned int eeprom_val;

eeprom_bt = read_eeprom(0);

// if not set
if (eeprom_bt!=1){
write_eeprom(0,1);

// initial boot not reset
write_eeprom(10,0);
reset_count=0;

}

else if (eeprom_bt==1){

eeprom_val = read_eeprom(10);
eeprom_val++;
write_eeprom(10, eeprom_val);
reset_count=eeprom_val;
}


}


void clr_reset_count (void){

write_eeprom(10, 0);
reset_count=0;
}
