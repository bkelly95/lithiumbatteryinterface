#include "SmartBattery.h"

uint8_t Data[2]; //Declare Data array
char Text[2]; //Declare Text array
//Data[0]=0; //Clear array
//Data[1]=0; //Clear array
const char charData[30];
char binarynum[100];

void setChannel(char dev, char ch){ //Select PCA9546 register 0x01 = 1, 0x02 = 2, 0x04 = 3, 0x08 = 4
    char address = dev;
    char reg = ch;
    I2C1_WriteRegister(address, reg);
}
void getBattData (void){
    for(unsigned int muxChip =0; muxChip<2; muxChip++){
        for(unsigned int muxChan =0; muxChan<8; muxChan++){
            if (muxChip == 1 && muxChan == 4){
                break;
            }
        I2C1_WriteRegister(0x0B,0x09); //Address of smart battery
        setChannel(muxChipID[muxChip],muxChanID[muxChan]);
        __delay_ms(1000);

        I2C1_WriteRegister(0x0B,0x01); //Select Battery register (0x01 = RemainingCapacityAlarm)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 1 bytes from the battery
        RemainingCapacityAlarm = ((Data[1] << 8) + Data[0]); //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x02); //Select Battery register (0x02 = RemainingTimeAlarm)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery
        RemainingTimeAlarm = ((Data[1] >> 8) + Data[0]); //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x03); //Select Battery register (0x03 = BatteryMode)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery
        BatteryMode = (Data[1] + Data[0]); //Convert to four byte hex

        I2C1_WriteRegister(0x0B,0x04); //Select Battery register (0x04 = AtRate)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery in previous step
        AtRate = ((Data[1])+ Data[0]); //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x05); //Select Battery register (0x05 = AtRateTimeToFull)
        I2C1_ReadNBytes(0x0B, &Data[0], 4); //Read 2 bytes from the battery in previous step
        AtRateTimeToFull = (Data[3]+ Data[2] + Data[1]+ Data[0]); //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x06); //Select Battery register (0x06 = AtRateTimeToEmpty)
        I2C1_ReadNBytes(0x0B, &Data[0], 4); //Read 2 bytes from the battery in previous step
        AtRateTimeToEmpty = (Data[3]+ Data[2] + Data[1]+ Data[0]);; //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x07); //Select Battery register (0x07 = AtRateOK)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery in previous step
        AtRateOK = (Data[1]+ Data[0]); //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x08); //Select Battery register (0x08 = Temperature)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 1 byte from the battery in previous step
        Temperature = ((Data[1] << 8)+ Data[0]); //Convert to two byte hex
        Temperature = ((Temperature - 273.15)/100);

        I2C1_WriteRegister(0x0B,0x09); //Select Battery register (0x09 = Voltage)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery in previous step
        Voltage = ((Data[1] << 8)+ Data[0]); //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x0a); //Select Battery register (0x10 = Current)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery
        Current = ((Data[1] << 8) + Data[0]); //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x0b); //Select Battery register (0x0B = Average Current)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery
        AverageCurrent = ((Data[1] << 8) + Data[0]); //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x0c); //Select Battery register (0x0C = Max Error %)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery
        MaxError = ((Data[1]) + Data[0]); //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x0d); //Select Battery register (0x0D = Relative State of Charge %)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery
        RelativeStateOfCharge = ((Data[1] << 8) + Data[0]); //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x0e); //Select Battery register (0x0E = Absolute State of Charge %)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery
        AbsoluteStateOfCharge = ((Data[1] << 8) + Data[0]); //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x0f); //Select Battery register (0x0F = Remaining Capacity mA)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery
        RemainingCapacity = ((Data[1] << 8) + Data[0]); //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x10); //Select Battery register (0x10 = Full Charge Capacity mA)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery
        FullChargeCapacity = ((Data[1] << 8) + Data[0]); //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x11); //Select Battery register (0x11 = Run Time To Empty minutes)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery
        RunTimeToEmpty = ((Data[1] << 8) + Data[0]); //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x12); //Select Battery register (0x12 = Average Time to Empty minutes)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery
        AverageTimeToEmpty = ((Data[1] << 8) + Data[0]); //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x13); //Select Battery register (0x13 = Average Time to Full minutes)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery
        AverageTimeToFull = ((Data[1] << 8) + Data[0]); //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x14); //Select Battery register (0x14 = Charging Current mA)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery
        ChargingCurrent = ((Data[1] << 8) + Data[0]); //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x15); //Select Battery register (0x15 = Charging Voltage mA)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery
        ChargingVoltage = ((Data[1] << 8) + Data[0]); //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x16); //Select Battery register (0x16 = Battery Status Bits)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery
        BatteryStatus = ((Data[1] << 8) + Data[0]); //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x17); //Select Battery register (0x17 = CycleCount #)
        I2C1_ReadNBytes(0x0B, &Text[0], 2); //Read 2 bytes from the battery
        CycleCount = ((Text[1]) + Data[0]); //Convert to two byte hex/*

        I2C1_WriteRegister(0x0B,0x18); //Select Battery register (0x18 = Design Capacity mAh)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery
        DesignCapacity = ((Data[1] << 8) + Data[0]); //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x19); //Select Battery register (0x19 = Design Voltage mV)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery
        DesignVoltage = ((Data[1] << 8) + Data[0]); //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x1a); //Select Battery register (0x1A = Specification Info bits)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery
        SpecificationInfo = ((Data[1]) + Data[0]); //Convert to two byte hex/*

        //I2C1_WriteRegister(0x0B,0x1b); //Select Battery register (0x1B = Manufacture Date bits)
        //I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery
        //ManufactureDate = ((Data[1]) + Data[0]); //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x1c); //Select Battery register (0x1C = SerialNumber #)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery
        SerialNumber = (Data[1] + Data[0]); //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x20); //Select Battery register (0x20 = Manufacturer Name text)
        I2C1_ReadNBytes(0x0B, &Text[0], 2); //Read 2 bytes from the battery first byte is the text length
        ManufacturerName[30] = Text[1]; //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x21); //Select Battery register (0x21 = Device Name text)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery first byte is the text length
        Data[1] = DeviceName[Data[30]]; //Convert to two byte hex/*

        I2C1_WriteRegister(0x0B,0x22); //Select Battery register (0x22 = Device Chemistry text)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery first byte is the text length
        DeviceChemistry[Data[10] = Data[1]]; //Convert to two byte hex

        I2C1_WriteRegister(0x0B,0x23); //Select Battery register (0x23 = Manufacturer Data text)
        I2C1_ReadNBytes(0x0B, &Data[0], 2); //Read 2 bytes from the battery first byte is the text length
        ManufacturerData[Data[10] = Data[1]]; //Convert to two byte hex

        I2C1_WriteRegister(0x77, 0x00);
        I2C1_WriteRegister(0x70, 0x00);


            if (muxChip == 0 && muxChan == 0){
                printf("#1 ");
            }
            else if (muxChip == 0 && muxChan == 1){
                printf("#2 ");
            }
            else if (muxChip == 0 && muxChan == 2){
                printf("#3 ");
            }
            else if (muxChip == 0 && muxChan == 3){
                printf("#4 ");
            }
            else if (muxChip == 0 && muxChan == 4){
                printf("#5 ");
            }
            else if (muxChip == 0 && muxChan == 5){
                printf("#6 ");
            }
            else if (muxChip == 0 && muxChan == 6){
                printf("#7 ");
            }
            else if (muxChip == 0 && muxChan == 7){
                printf("#8 ");
            }
            else if (muxChip == 1 && muxChan == 0){
                printf("#9 ");
            }
            else if (muxChip == 1 && muxChan == 1){
                printf("#10 ");
            }
            else if (muxChip == 1 && muxChan == 2){
                printf("#11 ");
            }
            else if (muxChip == 1 && muxChan == 3){
                printf("#12 ");
            }

        printf("%d ",RemainingCapacityAlarm);
        printf("%d ",RemainingTimeAlarm);
        printf("%x ",BatteryMode);
        printf("%d ",AtRate);
        printf("%d ",AtRateTimeToFull);
        printf("%d ",AtRateTimeToEmpty);
        printf("%d ",AtRateOK);
        printf("%d ",Temperature);
        printf("%d ",Voltage);
        printf("%d ",Current);
        printf("%d ",AverageCurrent);
        printf("%d ",MaxError);
        printf("%d ",RelativeStateOfCharge);
        printf("%d ",AbsoluteStateOfCharge);
        printf("%d ",RemainingCapacity);
        printf("%d ",FullChargeCapacity);
        printf("%d ",RunTimeToEmpty);
        printf("%d ",AverageTimeToEmpty);
        printf("%d ",AverageTimeToFull);
        printf("%d ",ChargingCurrent);
        printf("%d ",ChargingVoltage);
        printf("%d ",BatteryStatus);
        printf("%d ",CycleCount);
        printf("%d ",DesignCapacity);
        printf("%d ",DesignVoltage);
        printf("%d ",SpecificationInfo);
        printf("%d ",SerialNumber);
        printf(";");
        _delay(40000);
        }
    }
}

